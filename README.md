# **PHASE 1:

Translating from SQL queries to a relational algebra query** #

Provided DBHW2.jar can be run by providing input and output directory as argument

For Example:

java -jar DBHW2.jar inputdir outputdir

Top Level Class :

SqlTranslator.java  present in cs4321.project2 package

Strategy Used :

While building the query execution plan, 
	
	We initially check if its a join operation or not
	If it is a Join Operation , then
	
		The conditions that are part of the where clause are parsed using WhereClauseVisitor to 
		separate the Select Conditions,Join conditions and neutral expressions(expressions like 1<2, 3<10)
		Neutral Expressions like 1< 3 , 2>4 would be stored as  1<3 AND 2>4
		
		We start from left to right of the tables getting joined
		
			if a select operation is found on the table , select is performed before joining
				
			if multiple select conditions are matched with the table, the two conditions are joined using ANDExpression and 
			it is passed as argurment to Select operation
			
			For Example: if the query is 
			Select * from Sailors, Reserves where Sailors.A = 1 and Sailors.B =2 ,then The input expression to the Select Operator on Sailors 
			would be (Sailors.A = 1 and Sailors.B =2). This saves multiple select operations
		
			If there are neutral expressions in the query , all the neutral expressions would be joined AND used as a part of 
			Select Condition of the first table being joined. This ensures that the neutral expression is correctly evaluated as part of Expression Visitor
			
			if there is no select condition, then scanoperator is created pointing to the file

			Also, while joining two tables, if multiple join conditions are matched with the list of tables, 
			the two conditions are joined using ANDExpression and it is passed as argurment to Join operation			
			
			For Example: if the query is 
			Select * from Sailors, Reserves where Sailors.A = Reserves.A and Sailors.B =Reserves.B ,then the input expression to the Select Operator on 
			Sailors would be (Sailors.A = Reserves.A and Sailors.B =Reserves.B ). This saves multiple join operations
			
				
			if there is are multiple join conditions matching the already joined tables in the list , all the conditions would be Anded and the result 
			would be used as Join Expression. This helps eliminate the dependecy on the order of joining tables. The tables in any order would work as every join expression 
			is compared to all the tables already joined

			Once the join expression is ready , joinoperator is created using the expression
			
		
		If * is present as the condition of projection operator, 
			then all the columns of the table are selected and 
			Join operator is made the child of Projection Operator
		Else 
			Columns part of Projection conditions are extracted and join operator is made the child of Projection Opertor 

		Further based on whether Order By or Distinct is present, the operator tree is constructed


	The Flow for expressions without the join condition is the same , except the condition in the where clause would indidcate whether the tuple should be slected or not.

	Once the tuple is selected, 
		If * is present as the condition of projection operator, 
			then all the columns of the table are selected and 
			Select Operator is made the child of Projection Operator
		Else 
			Columns part of Projection conditions are extracted and Select operator is made the child of Projection Opertor 
			
			
# **PHASE 2:

IMPLEMENTATION OF DIFFERENT TYPES OF Join and other operators including DISTINCT** #

Top-level class : SqlTranslator.java

**Logic of SMJ:**

For the Left/Outer relation, we are allocating all the buffer pages provided as an input to the program. 
The buffer contains the maximum number of tuples that can be present in the buffer at a time.
For Right/Inner relation, we are requesting the tuples one by one from the child(The ultimate child is obviously reading in pages so only 1 I/O)
Then a loop runs as long as the buffer contains tuples in it. When the buffer becomes empty, the left relation requests another set of tuples to load into its buffer and the process is continued.
For the right relation, tuple by tuple comparison is made with the left tuple and three conditions are checked
1) When the right tuple is equal to the left tuple 
2) When the right tuple is greater than the left tuple
3) When the right tuple is smaller than the left tuple



**Logic of partition reset in SMJ: (This logic makes sure that the right relation is not buffering tuples in memory)**

We are maintaining an index(named 'restoreIndex' in the code) which stores the index of the tuple to which the index must be restored.
Usage:It starts with a value of 1 and contains the tuple number of the right relation. 
so, if the right relation has 10000 tuples which fit in 2 pages, and if the restoreIndex is having a value of 6000, then it means it contains the 1000th tuple of page 2 of the right relation.

For handling 1)(as mentioned above), we read the next tuple from the right relation and the left tuple remains intact.
We are also storing an instance of the last tuple of the right relation in 'lastRightTuple'. 
This is done so because whenever the right relation reaches the end of its tuples, we read the next tuple of the left relation and if it matches with the lastRightTuple, then we need to restore the index by calling reset(). 
If the next relation's tuple doesn't match with the lastRightTuple, we break the SMJ operation indicating its termination.
Example(continuation of Usage: above): When 2nd page of right relation is being read and reset(int restoreIndex) is performed, as we are reading files in the human readable format, we would start from the index 0 and calls to getNextTuple would be made restoreIndex number of times.

For handling 2) We are reading the next tuple from the left relation. Similar to the way in 1) is handled.

For handling 3) We read the next tuple from the right relation. No special handling needed in this case. just the restore index will be incremented as we don't need to visit the last tuple of this relation again ever.


**
Logic of handling unbounded state in DISTINCT:**

In the previous project, we were using in-memory data structure 'List<Tuple>' which used to store all the tuples in it and then dump the distinct tuples whenever requested using getNextTuple()
This was causing the unbounded state issue and resulting in out of memory Exception.
So, we modified the logic. Now, before distinct is called, External Sort / In-Memory Sort is called to sort all the tuples and then DuplicaticeEliminationOperator reads these tuples one by one using getNextTuple().
We store the value of the last tuple read before reading another tuple.
So whenever a new tuple is read, it is compared with the last tuple and if it is distinct, then only it is passed as a next tuple to its parent/dump. Otherwise, it is ignored.


The files generated for performance benchmarking are included in the jar under the folder Benchmarking at the top level

# 
**PHASE 3:

Index Building and Modfications** #

Top Level Class of the code: SqlTranslator

**
Index Scan operator Implementation**

**1. How are LowKey and HighKey Set**

	LowKey and HighKey are set in the class IndexExpressionVisitor.java which outputs
		1. the list of select expressions and list of indexExpressions  
		2. Sets the values of highKey and lowKey


       Logic:

	  
	  if expression is R.A < 6 then highKey is 5. if the expression is R.A > 5 then lowKey is set to 6. 
          Meaning highKey and LowKey are set to the values such that even high and low key are included in the values being fetched

	  If Multiple highKey and LowKey Ranges are found. Then the highest lowKey in the range and the lowest HighKey in the range is set	

	  Implementation Specifics:	  

	  	if the expression is of the form  - Column expression Value 
	  
			if the expression is GreaterThan
	  			lowKey = Value +1  
	  
			if the expression is GreaterThanEquals
	  			lowKey = Value  
	  
	  		if the expression is of the form minor than then
	  
	  			lowKey = value +1
	  
	  		if the expression is of the form minor than equals then
	  		 
	  		        lowKey = value
	  
	  
	  	else if the expression is of the form Value expression Column
	  		
			if the expression is GreaterThan
	  			highKey = Value -1 
	  
			if the expression is GreaterThanEquals	
	  			highKey = Value  
	  
	  		if the expression is of the form minor than then
	  	
	  			highKey = value -1
	  
	  		if the expression is of the form minor than equals then
	  		
	  			highKey = value
	  
	  
	 	  else if expression is of the form R. A = 6

	 		set lowKey and highKey to the same value 
	  			

**2. IndexScanOperator init() method** 
	
	Purpose : To descend from root to leaf node
		
	Logic:

		Open a file pointer to the indexFile
		
		Read the root
		
		if lowKey has been set and not null
			locate the key from the node , such that, the value of the key is <= lowKey
		
		if the new node found is index node, then rerun with actual Keys of index node
		
		if new node found is leaf node
			set it as the foundLeafnode

		Now, inside the leaf node identify the corresponding key
		Once Mathcing Key is found, 
			deserialize the leaf node
			set the rid and key pointers correctly
	

**3. IndexScanOperator getNextTuple Method**

	Purpose: To get the next tuple matching the range on the indexed attribute

	if after the init() method call, no leaf node found 
		return null
	
	if clustered index 
		read the tuple from the filereader, already pointing to the correct page and offset
		
		if tuple is found 
			check if the tuple is in the range of <= highKey
			return the tuple to the operator above
	
	else if unclustered index 

		get the Key set by init() method
		
		check if key <= highKey
		
		get the rid specific to the key
		
		point the filereader to the page corrsponding to the page id and tuple id from the tuple
		
		read the next tuple and return it to the higher order operators

**4. Logic to separate out conditions that can be handled by Select and IndexScanOperators**

	Input : Expression part of Select condition
	Output: Expressions that are part of the Index condition and the Select condition separated
	Method : visit(SelectOperatorLogical selectOperatorLogical)

	Logic:
		Here, if Indexes have to be used to selection and the child of the select operator is a scan operator
		
		IndexExpressionVisitor is called, which returns the list of
			1. the list of select expressions and list of indexExpressions  
			2. Sets the values of highKey and lowKey
		
		Here, a check would be made on each selection expression of the form Column Expression Value and
		Value expression Column to check if there is an index present on the attribute

		If it is not present, then the expression is part of the select condition

		If not, the expression is part of the index conditions . HighKey and lowKey are set accordingly

		
		Once we have the separated expressions, we check if there were any expressions in the select condition which
		can be part of index scan operator,

		If present, we would make the index scan operator the child of select operator
		
		If all the expressions in the select condition can be part of index expressions, 
			then only index scan operator is created
			
		If none of the expressions in the select condition can be part of index expressions,
			 then only select operator is created

		rest of the flow reamins as it is
		
**# **		
PHASE 4:**

Separating the Evaluation into Logical Query Plan building and Physical Query Plan Building** #
Top Level Class of the code: SqlTranslator

The Logic has been clearly explained in the classes mentioned below:

**1.  Selection Pushing Logic**

	Found in Class: UnionFindWhereClauseVisitor.java
	Method Name: buildExpressions(BinaryExpression expression)

**2.  Choice of implementation for each logical selection operator**

	Found in Class: LogicalQueryPlanBuilder.java
	Method Name:  public LogicalOperator buildQuery(Statement statement)
**
3   Choice of the join order**

	Found in Class:  JoinPhysicalPlanBuilder.java
	Method Name:   public List<Table> generateJoinOrder(UnionFind   
                    unionFind)
**
4.  Choice of implementation for each join operator**
	
	Found in Class:  PhysicalQueryPlanBuilder.java
	Method Name:   private int getJoinType(List<String> tables, List<Column> unusedColumns, List<Column> equalsColumns) {
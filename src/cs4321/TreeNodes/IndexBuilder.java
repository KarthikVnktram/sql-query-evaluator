package cs4321.TreeNodes;


import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import net.sf.jsqlparser.schema.Table;
import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.PhysicalOperators.ScanOperator;
import cs4321.practicum.PhysicalOperators.SortOperator;
import cs4321.practicum.io.BinaryTreeReader;
import cs4321.practicum.io.NodeWriterBinaryFormat;
import cs4321.practicum.io.TupleWriterBinaryFormat;
import cs4321.practicum.utilities.DBUtils;


/**
 * For index creation using B+ tree bulk loading. Algorithm used for creation of this tree is the same as specified in the instructions
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 *
 */

public class IndexBuilder {
	ScanOperator scanOperator =  null;
	SortOperator sortOperator = null;
	String columnName = null;


	List<LeafNode> leafNodes = new ArrayList<LeafNode>();
	//This is the data structure which stores all the (address, Lowest_Value) pairs for each leaf node and index node. So that the parent's key can be decided using lowest_value of the child node(leaf/index doesnt matter...works for everything)
	TreeMap<Integer, Integer> leafNodesCreated = new TreeMap<Integer,Integer>();
	Table table ;
	boolean isClustered = false;
	String indexFileName = "";
	
	int numberOfLeafNodes = 0;
	int addressOfRootNode = 0;
	
	
	NodeWriterBinaryFormat nodeWriterBinaryFormat = null;
	int order = 0;
	public IndexBuilder(Table table,String columnName,int order,boolean isClustered) {
		// TODO Auto-generated constructor stub
		this.columnName = columnName;
		this.order = order;
		this.table = table;
		this.isClustered = isClustered;
		if(table.getAlias() != null && false){ //Modifiy in next project
			indexFileName = table.getAlias() +"." + columnName;
		} else{
			indexFileName = table.getName() + "." + columnName;
		}
	}
	
	public boolean isClustered() {
		return isClustered;
	}

	public void buildIndexes(){
		scanOperator = new ScanOperator(table);
		sortOperator = new SortOperator(table, scanOperator, DBUtils.getSelectedColumnsFromTable(table,columnName));
		sortOperator.sorter();
		
		// handling for clustered Index
		if(this.isClustered == true){
			//write data back to the relation
			String pathToTable = DatabaseGlobalCatalog.getInstance().getPathToTable(table);
			TupleWriterBinaryFormat writer = new TupleWriterBinaryFormat(pathToTable);
			int currentTupleIndex = -1;
			int currentPageIdentifier = 0;
			
			
			for(int i = 0 ; i < sortOperator.getAllTuples().size(); i ++){
				
				currentTupleIndex++;
				Tuple sortedTupleToWrite= sortOperator.getAllTuples().get(i);
				Rid rid = new Rid(currentPageIdentifier, currentTupleIndex);
				sortedTupleToWrite.setRid(rid);
				writer.write(sortedTupleToWrite.tuple);
				if(writer.endOfPageReached() == true){
					currentTupleIndex = -1;
					currentPageIdentifier++;
				}
			}
		
			writer.flush();
			writer.close();
			
			
		}
		
		
		int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columnName);
		createLeafNodes (sortOperator.getAllTuples(), index);
		numberOfLeafNodes = leafNodesCreated.size();
		
		createIndexNodes (this.leafNodesCreated);
		addressOfRootNode = (int) leafNodesCreated.keySet().toArray()[leafNodesCreated.size()-1];
		
		nodeWriterBinaryFormat.writeHeaderFile(addressOfRootNode, numberOfLeafNodes, order);
//		System.out.println("");
		nodeWriterBinaryFormat.close();
		
		BinaryTreeReader binaryTreeReader = new BinaryTreeReader(order, DatabaseGlobalCatalog.getInstance().getIndexesPath()+File.separator+getIndexFileName());
		LeafNode leaffNode = (LeafNode) binaryTreeReader.readPageWithPageNumber(1);
//		System.out.println();
	}
	/**
	 * return void: leafNodesCreated contains additional values obtained using this method.
	 * @param : leafNodesCreated: a tree map of (Address,lowest_value) to use and update leafNodesCreated whenever a new index node is to be created
	 */
	private void createIndexNodes(TreeMap<Integer, Integer> leafNodesCreated) {
		// TODO Auto-generated method stub		
		int n=leafNodesCreated.size();
		int k=(int)Math.ceil((double)n/(2*this.order+1));
		int addressField=0;//Major importance inside the below loops
		int indexNodesCreated=n;
		while(true)//TODO: go till root
		{
			if(indexNodesCreated<=1)//check when only one leaf node is there, Prof. lucja says she'll ignore the case when you need to create an index node on top of only leaf node.
				break;
			k=(int)Math.ceil((double)indexNodesCreated/((2*this.order+1)));
			n=indexNodesCreated;
			indexNodesCreated=0;
			while(k>=1)
			{
				//last two node case
				if((2*this.order+1)<n && n<(3*this.order+2)){					
						
						int childListSize1=n/2;
						int keyListSize1=childListSize1-1;
						int childListSize2=n-(n/2);
						int keyListSize2=childListSize2-1;
						addressField=createIndexNode(childListSize1, keyListSize1, addressField);
						indexNodesCreated++;
						addressField=createIndexNode(childListSize2, keyListSize2, addressField);
						indexNodesCreated++;
						k=0;
					}
				else{
					//normal split
					int childListSize;
					int keyListSize;
					if(n>(2*this.order+1)){
						childListSize=(2*this.order+1);
					}else{
						childListSize=n;
					}
					keyListSize=childListSize-1;
					n=n-childListSize;
					k--;
					addressField=createIndexNode(childListSize,keyListSize,addressField);	
					indexNodesCreated++;
				}
			}
		}
//		System.out.println();
	}
	/**
	 * 
	 * @param childListSize: contains the list of address of each of the child of the index node in process.
	 * @param keyListSize: contains the list of keys for the index node in process-it uses leafNodesCreated.
	 * @param addressField: contains the address on which the index node is to be written
	 * @return
	 */
	private int createIndexNode(int childListSize, int keyListSize,int addressField) {
		// TODO Auto-generated method stub
		IndexNode indexNode=new IndexNode();
		indexNode.setNumberOfKeys(keyListSize);
		indexNode.setNumberofchildren(childListSize);
		List<Integer> keyList=new ArrayList<Integer>();
		List<Integer> childList=new ArrayList<Integer>();
		int lowestValue=addressField+2;
		for(int childSize=0;childSize<childListSize;childSize++){
			childList.add((Integer)leafNodesCreated.keySet().toArray()[addressField]);
			addressField++;
			if(childSize<keyListSize){
				keyList.add(leafNodesCreated.get(addressField+1));
			}
		}
		this.leafNodesCreated.put(leafNodesCreated.size()+1, leafNodesCreated.get(childList.get(0)));//this is the reason lowestValue is used.
		indexNode.setActualKeys(keyList);
		indexNode.setAddressOfChildren(childList);
		indexNode.order = order;
		nodeWriterBinaryFormat.write(indexNode);
		
		return addressField;
	}
	/**
	 * 
	 * @param allTuples: consists of the tuples on which leaf node creation is to be done using b+ tree bulk loading
	 * @param index: to obtain the key valus in the leaf node.
	 */
	private void createLeafNodes(ArrayList<Tuple> allTuples, int index) {
		// TODO Auto-generated method stub
		Tuple prevTuple = allTuples.get(0);
		int prevKey = allTuples.get(0).tuple.get(index) ;
		LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
		int maxLeafKeys= 2 * order;
		
		ArrayList<Rid> rids = new ArrayList<Rid>();
		rids.add(prevTuple.getRid());
//		leafDataEntry.put(prevKey, rids);
		int i = 1;
		//This variable is used to check the last two node case where splitting is different from normal split.
		int currentTuplesRemaining = sortOperator.getDistinctTuples().size();
		//This variables are used to ensure all the edge cases are working
		boolean isLeafKeyAdded = true;
		boolean isRidListAdded = true;
		boolean order1check=false;
	
		nodeWriterBinaryFormat = new NodeWriterBinaryFormat(DatabaseGlobalCatalog.getInstance().getIndexesPath()+File.separator+indexFileName);
		nodeWriterBinaryFormat.skipFirstNode();
		while(i<allTuples.size()){
			//Last two node case whose handling is done outside the loop
			if(currentTuplesRemaining > 2*order && currentTuplesRemaining < 3 * order && leafDataEntry.size() == 0 ){
				
				i--;
				break;
				
			} 
			//normal split
			Tuple currentTuple  = sortOperator.getAllTuples().get(i);
			int currentKey = currentTuple.tuple.get(index);
			//when there are multiple rids associated with a particular key of leaf node.
			if(currentKey == prevKey){
				rids.add(currentTuple.getRid());
				isRidListAdded = false;
			} else {//if the current key and prev key doesn't match, add the list of rids obtained so far as a key entry in the leaf node unless it is full.
				isRidListAdded= true;
				leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
				rids = new ArrayList<Rid>();
				rids.add(currentTuple.getRid());
				currentTuplesRemaining --;
				prevKey = currentKey;
				if(i+1 == allTuples.size() && leafDataEntry.size() < maxLeafKeys){
					leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
					rids = new ArrayList<Rid>();
					
				}else if(i+1==allTuples.size() && this.order==1){
					//If this is the edge case, it is handled explicitly outside using order1check flag
					order1check=true;
				}
				isLeafKeyAdded = false;
				//add the leafnode when it reaches maximum capacity
				if(leafDataEntry.size() == maxLeafKeys){
					LeafNode leafNode = new LeafNode(maxLeafKeys);
					leafNode.setLeafDataEntry(leafDataEntry);
					int pageId = nodeWriterBinaryFormat.write(leafNode);
					addToLeafNodesCreated(pageId,leafDataEntry );
					leafNodes.add(leafNode);
					isLeafKeyAdded = true;
					leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
				}
			}
			i++;
		}
		
		
		//All these subsequent checks are written to handle edge cases.
		//If the rid list wasnt added with the key as a leaf data entry, then add it!
		if(isRidListAdded == false){
			leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
			LeafNode leafNode = new LeafNode(maxLeafKeys);
			leafNode.setLeafDataEntry(leafDataEntry);
			int pageId = nodeWriterBinaryFormat.write(leafNode);
			addToLeafNodesCreated(pageId,leafDataEntry );
			leafNodes.add(leafNode);
			leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
			isLeafKeyAdded=true;
		} 
		//If leaf key wasnt added, then this is executed
		if(isLeafKeyAdded == false) {
			LeafNode leafNode = new LeafNode(maxLeafKeys);
			leafNode.setLeafDataEntry(leafDataEntry);
			int pageId = nodeWriterBinaryFormat.write(leafNode);
			addToLeafNodesCreated(pageId,leafDataEntry );
			leafNodes.add(leafNode);
			leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
			
		}
		//to check the edge case when d(order)=1. To prevent it from allocating more than allowed keys in a node.
		if(order1check == true){
			leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
			LeafNode leafNode = new LeafNode(maxLeafKeys);
			leafNode.setLeafDataEntry(leafDataEntry);
			int pageId = nodeWriterBinaryFormat.write(leafNode);
			addToLeafNodesCreated(pageId,leafDataEntry );
			leafNodes.add(leafNode);
			leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
			isLeafKeyAdded=true;
		}
		//When all the tuples are processed, end.
		if(i == allTuples.size()){
			return;
		}
		
		prevTuple = allTuples.get(i);
		prevKey =  prevTuple.tuple.get(index);
		leafDataEntry = new LinkedHashMap<Integer, ArrayList<Rid>>();
		rids = new ArrayList<Rid>();
		rids.add(prevTuple.getRid());
		i++;
		// logic to handle last 2 nodes
		if(i < allTuples.size()){
			int runningCounter = 0;
			//Add second to last node.
			while( runningCounter < currentTuplesRemaining/2 && i<allTuples.size() ){
				Tuple currentTuple  = sortOperator.getAllTuples().get(i);
				int currentKey = currentTuple.tuple.get(index);
				if(currentKey == prevKey){
					rids.add(currentTuple.getRid());
				} else {
					leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
					prevKey = currentKey;
					rids = new ArrayList<Rid>();
					rids.add(currentTuple.getRid());
					runningCounter++;
					
				}
				i++;
			}	
			LeafNode leafNode = new LeafNode(maxLeafKeys);
			leafNode.setLeafDataEntry(leafDataEntry);
			int pageId = nodeWriterBinaryFormat.write(leafNode);
			addToLeafNodesCreated(pageId,leafDataEntry );
			leafNodes.add(leafNode);
			leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
			i--;
			prevTuple = allTuples.get(i);
			prevKey =  prevTuple.tuple.get(index);
			leafDataEntry = new LinkedHashMap<Integer, ArrayList<Rid>>();
			rids = new ArrayList<Rid>();
			rids.add(prevTuple.getRid());
			i++;
			
			//Add last node
			while( i<allTuples.size() ){
				Tuple currentTuple  = sortOperator.getAllTuples().get(i);
				int currentKey = currentTuple.tuple.get(index);
				if(currentKey == prevKey){
					rids.add(currentTuple.getRid());
					if(i+1 == allTuples.size()){
						leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
						rids = new ArrayList<Rid>();
						
					}
				} else {
					leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
					prevKey = currentKey;
					rids = new ArrayList<Rid>();
					rids.add(currentTuple.getRid());
					if(i+1 == allTuples.size()){
						leafDataEntry.put(prevKey, (ArrayList<Rid>) rids.clone());
						rids = new ArrayList<Rid>();
						
					}
				}
				
				i++;
			}
			leafNode = new LeafNode(maxLeafKeys);
			leafNode.setLeafDataEntry(leafDataEntry);
			pageId = nodeWriterBinaryFormat.write(leafNode);
			addToLeafNodesCreated(pageId,leafDataEntry );
			leafNodes.add(leafNode);
			leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
		
		}
//		System.out.println("");
	}
	/**
	 * to get the index file name using which indexes is needed to be built.
	 * @return String 
	 */
	public String getIndexFileName() {
		return indexFileName;
	}
	/**
	 * for setting the index file name
	 * @param indexFileName
	 */
	public void setIndexFileName(String indexFileName) {
		this.indexFileName = indexFileName;
	}
	/**
	 * for adding the created index annd leaf nodes into the (address,lowest_value) data structure.
	 * @param pageId: contains the address on which the node was created
	 * @param leafDataEntry: contains the list of keys whose 1st value is used for storing in the leafNodesCreated as we need to store (address,lowest_value) in the map. and 1st key will be lowest in any case
	 */
	private void addToLeafNodesCreated(int pageId, LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry ) {
		this.leafNodesCreated.put(pageId,(Integer) leafDataEntry.keySet().toArray()[0] );		
	}
	/**
	 * not getting used 
	 * @return String
	 */
	public String getColumnName() {
		return columnName;
	}
}

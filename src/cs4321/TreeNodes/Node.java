package cs4321.TreeNodes;


/**
 * This Class creates a abstract class for the Node. Extended by LeafNode and IndexNode
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */

public abstract class Node {

}

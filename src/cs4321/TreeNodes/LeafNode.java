package cs4321.TreeNodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * This Class creates a Data Structure for the Leaf Node
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class LeafNode extends Node {

	int numberOfEntriesInLeaf = 0;
	LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
	
	public LeafNode(int numberOfEntries) {
		// TODO Auto-generated constructor stub
		this.numberOfEntriesInLeaf = numberOfEntries;
	}
	
	public LeafNode(int numberOfEntries,LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry ){
		this.numberOfEntriesInLeaf = numberOfEntries;
		this.leafDataEntry = leafDataEntry;
	}
	
	public int getNumberOfEntriesInLeaf() {
		return numberOfEntriesInLeaf;
	}

	public void setNumberOfEntriesInLeaf(int numberOfEntriesInLeaf) {
		this.numberOfEntriesInLeaf = numberOfEntriesInLeaf;
	}

	public HashMap<Integer, ArrayList<Rid>> getLeafDataEntry() {
		return leafDataEntry;
	}

	public void setLeafDataEntry(LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry) {
		this.leafDataEntry = leafDataEntry;
	}
	
}

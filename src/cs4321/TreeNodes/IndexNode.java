package cs4321.TreeNodes;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class creates a Data Structure for the Index Node
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class IndexNode extends Node {

	int numberOfKeys = 0;
	int numberofchildren=0;
	public int order = 0;
	List<Integer> actualKeys = new ArrayList<Integer>();
	List<Integer> addressOfChildren=  new ArrayList<Integer>();
	
	public IndexNode() {
		// TODO Auto-generated constructor stub
		numberOfKeys = 0;
	}

	public IndexNode(int order,List<Integer> actualKeys ,List<Integer> addressOfChildren ){
		this.order = order;
		this.actualKeys.addAll(actualKeys);
		this.addressOfChildren.addAll(addressOfChildren);
	}
	public int getNumberOfKeys() {
		return numberOfKeys;
	}

	public void setNumberOfKeys(int numberOfKeys) {
		this.numberOfKeys = numberOfKeys;
	}

	public List<Integer> getActualKeys() {
		return actualKeys;
	}
	
	public int getNumberofchildren() {
		return this.numberofchildren;
	}
	
	public void setNumberofchildren(int n) {
		this.numberofchildren=n;
	}

	public void setActualKeys(List<Integer> actualKeys) {
		this.actualKeys = actualKeys;
	}

	public List<Integer> getAddressOfChildren() {
		return addressOfChildren;
	}

	public void setAddressOfChildren(List<Integer> addressOfChildren) {
		this.addressOfChildren = addressOfChildren;
	}
	
	
	
	
	
}

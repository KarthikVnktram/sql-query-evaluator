package cs4321.TreeNodes;


/**
 * This Class creates a Data Structure for the Rid
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class Rid {

	int pageId = 0;
	int tupleId = 0;
	
	public Rid(int pageIdentifier, int tupleIdentifier){
		
		this.pageId= pageIdentifier;
		this.tupleId =tupleIdentifier;
		
	}
	
	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public int getTupleId() {
		return tupleId;
	}

	public void setTupleId(int tupleId) {
		this.tupleId = tupleId;
	}

	
}

package cs4321.practicum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 *  This class outputs 
 *           Map of Table to Select Condition
 *           Map of Tables Used in Join to Join Condition 
 *           Neutral Expressions like 1< 3 , 2>4 would be stored as  1<3 AND 2>4
 *           that are part of where condition
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class WhereClauseVisitor extends AbstractExpressionEvaluator {

	//Map of Table to Select Condition
	Map<Table,Expression> selectExpression = new HashMap<Table,Expression>();
	
	//Map of Tables Used in Join to Join Condition
	Map<List<Table>,Expression> joinExpression = new HashMap<List<Table>,Expression>();
	
//	Neutral Expressions like 1< 3 , 2>4 would be stored as  1<3 AND 2>4
	Expression neutralExpression = null;
	
	/**
	 * visits AndExpression and calls the left and right child
	 */
	@Override
	public void visit(AndExpression andExpression) {
		// TODO Auto-generated method stub
		andExpression.getLeftExpression().accept(this);
		andExpression.getRightExpression().accept(this);
		
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		buildExpressions(equalsTo);
	}
	

	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		buildExpressions(notEqualsTo);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThan greaterThan) {
		buildExpressions(greaterThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		buildExpressions(greaterThanEquals);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThan minorThan) {
		buildExpressions(minorThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		buildExpressions(minorThanEquals);
	}
	
	
	/**
	 * Returns the select expressions map 
	 * @return
	 */
	public Map<Table, Expression> getSelectExpressions(){
		return selectExpression;
	}
	
	/**
	 * Returns the join expressions map 
	 * @return
	 */
	public Map<List<Table>, Expression> getJoinExpressions(){
		return joinExpression;
	}
	
	/**
	 * Method to check if list of tables is already present as key for join expression Map
	 * @param tables
	 * @return
	 */
	private List<Table> isTablesAlreadyPresent(List<Table> tables){
		boolean isFirstTableFound= false;
		for(List<Table> joinedTables :joinExpression.keySet()){
			
			for(Table joinTable:  joinedTables){
					
					if(isTableMatching(joinTable, tables.get(0)) || isTableMatching(joinTable,tables.get(1))  ){
						if(isFirstTableFound == true) {
							return joinedTables;
						} else{
							isFirstTableFound= true;
						}
					}
				}
			}
		return null;
	}

	/**
	 * Method to check if list of tables is already present as key for select expression Map
	 * @param tables
	 * @return
	 */
	private Table isTableAlreadyPresentInSelect(Table table){
		for(Table selectedTable : selectExpression.keySet()){
			if(isTableMatching(selectedTable,table)){
				return selectedTable;
			}
		}
		return null;
	}
	
	/**
	 * Method to check two tables given as arguments match
	 * @param tables
	 * @return
	 */
	private boolean isTableMatching(Table leftTable,Table rightTable){
		if(leftTable.getAlias()!= null) {
			if(leftTable.getAlias().equals(rightTable.getAlias())){
				 return true;
			}
		}else {
			if(leftTable.getName().equals(rightTable.getName())){
				 return true;
			}
		}
		return false;
	}

	/**
	 * Method to combine conditions if they are condition on
	 * 						same table in case of Select  and
	 * 						same list of tables in case of Join
	 * This would be passed in as expression while creating the Select and Join Operartors
	 * 
	 * @param expression     
	 * @param previousExpression
	 * @return
	 */
	private Expression buildAndExpression(Expression expression, Expression previousExpression) {
		// TODO Auto-generated method stub
		AndExpression andExpression = new AndExpression(expression, previousExpression);
		return andExpression;
	}
	
	/**
	 * Returns the neutral expressions 
	 * @return
	 */
	public Expression getNeutralExpressions() {
		return neutralExpression;
	}
	
	/**
	 * Builds the select expressions map, join conditions map and neutral expressions
	 * if mutiple conditions are referring to the same table/tables that would get ANDed and get stored
	 * @return
	 */
	private void buildExpressions(BinaryExpression expression) {
		// TODO Auto-generated method stub
		List<Table> tables = new ArrayList<Table>();
		if(expression.getLeftExpression() instanceof Column && expression.getRightExpression() instanceof Column) {
			tables.add(((Column)expression.getLeftExpression()).getTable());
			tables.add(((Column)expression.getRightExpression()).getTable());
			List<Table> joinExpressionKey = isTablesAlreadyPresent(tables);
			if( joinExpressionKey != null){
				joinExpression.put(joinExpressionKey, buildAndExpression(expression,joinExpression.get(joinExpressionKey)));
			} else {
				joinExpression.put(tables,expression);
			}
		} else if(expression.getLeftExpression() instanceof LongValue && expression.getRightExpression() instanceof LongValue){
			if(neutralExpression == null) {
				neutralExpression = expression;
			} else {
				neutralExpression = buildAndExpression(neutralExpression, expression);
			}
		}else {
		
			Table tableToAdd;
			if(expression.getLeftExpression() instanceof Column){
				tableToAdd= ((Column)expression.getLeftExpression()).getTable();
				Table selectedTable = isTableAlreadyPresentInSelect(tableToAdd);
				if( selectedTable != null){
					selectExpression.put(selectedTable,buildAndExpression(expression, selectExpression.get(selectedTable)));
				} else {
					selectExpression.put(tableToAdd,expression);
				}
			} else {
				tableToAdd= ((Column)expression.getRightExpression()).getTable();
				Table selectedTable = isTableAlreadyPresentInSelect(tableToAdd);
				if(selectedTable != null){
					selectExpression.put(selectedTable,buildAndExpression(expression, selectExpression.get(selectedTable)));
				} else {
					selectExpression.put(tableToAdd,expression);
				}
			}
		}

	}
}

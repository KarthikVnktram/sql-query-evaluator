package cs4321.practicum;

import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 * This Class evaluates the Where Expressions in Select and Join Operations
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class ExpressionEvaluator extends AbstractExpressionEvaluator{
	
	//final output of the ExpressionVisitor indicating whether tuple is selected or not
	boolean tupleSelected= false;
	
	//Left and Right values of the expression
	int leftValue= Integer.MIN_VALUE;   
	int rightValue = Integer.MIN_VALUE;
	
	Tuple tuple = null;  //Tuple corresponding to select
	
	Tuple leftTuple = null;   //Left Tuple of Join
	Tuple rightTuple = null;  // Right Tuple of Join
	boolean isJoin = false; 
	
	public boolean getResult(){
		return tupleSelected;
	}

	public ExpressionEvaluator(Tuple tuple){
		this.tuple = tuple;
	}
	
	public ExpressionEvaluator(Tuple leftTuple, Tuple rightTuple) {
		// TODO Auto-generated constructor stub
		this.leftTuple = leftTuple;
		this.rightTuple=rightTuple;
		isJoin= true;
	}

	/**
	 * Call the accept on left expression and right expression
	 * AND the results to get final result
	 * 
	 */
	@Override
	public void visit(AndExpression andExpression) {
		// TODO Auto-generated method stub
		andExpression.getLeftExpression().accept(this);
		
		boolean tempResult = tupleSelected;
		if(andExpression.getRightExpression() != null){
			andExpression.getRightExpression().accept(this);
			tupleSelected =  tupleSelected && tempResult;
		}
		
		reset();
	}
	
	/**
	 * 
	 * visits the column and obtains the value corresponding to the column
	 * 
	 */
	@Override
	public void visit(Column column) {
		// TODO Auto-generated method stub
		int attributeValue = -1;
		if(isJoin == false){
			 attributeValue=DatabaseGlobalCatalog.getInstance().getAttributes(tuple,column);
		} else {
			if(leftTuple.table != null) {
				String tableName = null;
				if(leftTuple.table.getAlias() != null){
					tableName= leftTuple.table.getAlias();
				} else {
					 tableName = leftTuple.table.getName();
				}
				if(column.getTable().getName().equals(tableName)){
					attributeValue=DatabaseGlobalCatalog.getInstance().getAttributes(leftTuple,column);
				} else{
					attributeValue=DatabaseGlobalCatalog.getInstance().getAttributes(rightTuple,column);
				}
			
			} else {
				Table tableToCheck = null;
				boolean isFirst = false;
				for(int k=0 ; k < leftTuple.tables.size() ; k++){
					String tableName = null;
					Table table = leftTuple.tables.get(k);
					if(table.getAlias() != null){
						tableName = table.getAlias();
					} else {
						tableName =  table.getName();
					}
					if(tableName.equals(column.getTable().getName())){
						tableToCheck = table;
						if(k==0) {
							isFirst =true;
						}
					}
				}
				if(tableToCheck!=null){
					attributeValue=DatabaseGlobalCatalog.getInstance().getAttributesFromTable(leftTuple,tableToCheck,column,isFirst);
				} else{
					attributeValue=DatabaseGlobalCatalog.getInstance().getAttributes(rightTuple,column);
				}
			}
			
		}

		if(leftValue != Integer.MIN_VALUE){
			rightValue= attributeValue; //call the method to return the value corresponding to the column in the tuple
		} else{
			leftValue = attributeValue;
		}
	}
	
	/**
	 * 
	 * Visits the Expression of type LongValue and stores to compare later
	 * 
	 */
	@Override
	public void visit(LongValue longValue) {
		// TODO Auto-generated method stub
		if(leftValue != Integer.MIN_VALUE){
			rightValue= (int) longValue.getValue(); 
		} else{
			leftValue = (int) longValue.getValue();
		}
	}
	
	
	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on equalsTo to get final result
	 * 
	 */
	@Override
	public void visit(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		equalsTo.getLeftExpression().accept(this);
		equalsTo.getRightExpression().accept(this);
		if(leftValue == rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		
		reset();
	}
	

	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on notEqualsTo to get final result
	 * 
	 */
	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		// TODO Auto-generated method stub
		notEqualsTo.getLeftExpression().accept(this);
		notEqualsTo.getRightExpression().accept(this);
		if(leftValue != rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		
		reset();
	}
	
	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on greaterThan to get final result
	 * 
	 */
	@Override
	public void visit(GreaterThan greaterThan) {
		// TODO Auto-generated method stub
		greaterThan.getLeftExpression().accept(this);
		greaterThan.getRightExpression().accept(this);
		
		if(leftValue > rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		
		reset();
	}
	
	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on greaterThanEquals to get final result
	 * 
	 */
	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		// TODO Auto-generated method stub
		greaterThanEquals.getLeftExpression().accept(this);
		greaterThanEquals.getRightExpression().accept(this);
		
		if(leftValue >= rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		
		reset();
	}
	
	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on minorThan to get final result
	 * 
	 */
	@Override
	public void visit(MinorThan minorThan) {
		// TODO Auto-generated method stub
		minorThan.getLeftExpression().accept(this);
		minorThan.getRightExpression().accept(this);
		
		if(leftValue < rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		reset();
	}
	

	/**
	 * 
	 * Call the accept on left expression and right expression
	 * compares the two results on minorThanEquals to get final result
	 * 
	 */
	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		// TODO Auto-generated method stub
		minorThanEquals.getLeftExpression().accept(this);
		minorThanEquals.getRightExpression().accept(this);
		
		if(leftValue <= rightValue){
			tupleSelected = true;
		} else {
			tupleSelected = false;
		}
		
		reset();
	}
	
	private void reset(){
		leftValue= Integer.MIN_VALUE;
		rightValue = Integer.MIN_VALUE;
	}
	

}


package cs4321.practicum;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;


/**
 * Database Catalog containing information about schema path, datapath, input files, output files.
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 *
 */

public class DatabaseGlobalCatalog {
	/**
	 * static is used to have the same reference across all the objects and classes.
	 */
	private static DatabaseGlobalCatalog instance = new DatabaseGlobalCatalog();
	
	/**
	 * Initialization of variables storing all the RELATIVE paths to the required files
	 */
	String outputFileName="query";

	String inputFilePath = "."+File.separator+"."+File.separator+"."+File.separator+"input";
	
	String outputFilePath = "."+File.separator+"."+File.separator+"."+File.separator+"expected_output";
	String schemaPath = inputFilePath+File.separator+"db";
	String dataPath = schemaPath+File.separator+"data";
	String indexesPath =  schemaPath+File.separator+"indexes";
	String statsPath = schemaPath;


	String testDataPath = schemaPath+File.separator+"data"+File.separator + "TEST_JIGAR";
	String schemaName = "schema.txt";
	String queryFileName = "queries.sql";
	String configFileName = "plan_builder_config.txt";
	String statsName = "stats.txt";
	
	boolean buildIndexes = false;
	boolean evaluateQueries = false;
	String indexInfoFileName = "index_info.txt";
	
	String tempDirectory = "."+File.separator+"."+File.separator+"."+File.separator+"temp_dir";
	
//	public HashMap<String, String> indexTable =  new HashMap<String, String>();
//	public HashMap<String, Boolean> indexClusterTable =  new HashMap<String, Boolean>();
//	public HashMap<String, Integer> indexOrderTable = new HashMap<String, Integer>();
	
	public HashMap<String,List<IndexInfo>> indexTable = new HashMap<String, List<IndexInfo>>();
	

	/**
	 * Getters and Setters functions
	 */

	public String getStatsName() {
		return statsName;
	}

	public void setStatsName(String statsName) {
		this.statsName = statsName;
	}

	public String getStatsPath() {
		return statsPath;
	}

	public void setStatsPath(String statsPath) {
		this.statsPath = statsPath;
	}
	
	public String getSchemaName() {
		return schemaName;
	}

	public String getOutputFileName()
	{
		return outputFileName;
	}
	
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getQueryFileName() {
		return queryFileName;
	}

	public void setQueryFileName(String queryFileName) {
		this.queryFileName = queryFileName;
	}
	
	public String getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
	
	public String getSchemaPath() {
		return schemaPath;
	}

	public void setSchemaPath(String schemaPath) {
		this.schemaPath = schemaPath;
	}

	public String getDataPath() {
		return dataPath;
	}

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	public static DatabaseGlobalCatalog getInstance(){
	
		return instance;
		
	}
	/**
	 * To get path of the table using Table Object
	 * @return String
	 */
	public String getPathToTable(Table fromTable) {
		
		return getDataPath()+File.separator+fromTable.getName();
	}
	/**
	 * For Non-Joins:It uses Tuple to get the table name and then matches it with the table name in Schema.txt and returns the index of the Column(Sent as an argument) using the columns of the selected tables from schema.txt
	 * @param Tuple
	 * @param Column
	 * @return Integer
	 */
	public Integer getAttributes(Tuple tuple,Column column)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(tuple.table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(column.getColumnName().equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * if Column match fails, return -1
		 */
		int result=-1;
		if(index!=-1)
			result=tuple.tuple.get(index);
		return result;
	}
	/**
	 * For Joins:It uses Tuple to get the table name and then matches it with the table name in Schema.txt and returns the index of the Column(Sent as an argument) using the columns of the selected tables from schema.txt
	 * @param Tuple
	 * @param Table
	 * @param boolean
	 * @param Column
	 * @return Integer
	 */
	public Integer getAttributesFromTable(Tuple tuple,Table table,Column column,boolean isFirst)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(column.getColumnName().equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int result=-1;
		if(isFirst == false){
			for(int j=0; j < tuple.tables.size(); j++ ){
				if(tuple.tables.get(j) == table){
					break;
				}
				int lengthOfPreviousTable = getActualIndexFromJoin(tuple.tables.get(j));
				if(lengthOfPreviousTable != -1) {
					index = index+ lengthOfPreviousTable; //get the length of first table and add the index found to get new index
				}
			}
		}
		if(index!=-1)
			result=tuple.tuple.get(index);
		return result;
	}

	/**
	 * To get the total number of attributes of the table.
	 * @param Table
	 * @return int
	 */
	
	private int getActualIndexFromJoin(Table table) {
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					scanner.close();
					return schema_info.length -1;
					//Attributes=schema_info;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public String getTestDataPath() {
		return testDataPath;
	}
	
	public String getConfigFileName() {
		return configFileName;
	}

	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}
	
	
	public String getPathToTestTable(Table fromTable) {
		
		return getTestDataPath()+File.separator+fromTable.getName();
	}
	
	
	
	public Integer getAttributesWithColumnName(Tuple tuple,String columnName)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(tuple.table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(columnName.equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * if Column match fails, return -1
		 */
		int result=-1;
		if(index!=-1)
			result=tuple.tuple.get(index);
		return result;
	}
	/**
	 * For Joins:It uses Tuple to get the table name and then matches it with the table name in Schema.txt and returns the index of the Column(Sent as an argument) using the columns of the selected tables from schema.txt
	 * @param Tuple
	 * @param Table
	 * @param boolean
	 * @param Column
	 * @return Integer
	 */
	public Integer getAttributesFromTableWithColumnName(Tuple tuple,Table table,String columnName,boolean isFirst)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(columnName.equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int result=-1;
		if(isFirst == false){
			for(int j=0; j < tuple.tables.size(); j++ ){
				if(getTableStringToMatch(tuple.tables.get(j)).equals(getTableStringToMatch(table))){
					break;
				}
				int lengthOfPreviousTable = getActualIndexFromJoin(tuple.tables.get(j));
				if(lengthOfPreviousTable != -1) {
					index = index+ lengthOfPreviousTable; //get the length of first table and add the index found to get new index
				}
			}
		}
		if(index!=-1)
			result=tuple.tuple.get(index);
		return result;
	}
	
	public String getTableStringToMatch(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	public Integer getIndexFromTable(Table table,String columnName)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(columnName.equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * if Column match fails, return -1
		 */
		int result=-1;
		if(index!=-1)
			result= index;
		return result;
	}
	
	
	
	public Integer getIndexFromMultipleTables(Table table,String columnName,List<Table> tables, boolean isFirst)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		int index=-1;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(columnName.equals(schema_info[i]))
						{
							index=i-1;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int result=-1;
		if(isFirst == false){
			for(int j=0; j < tables.size(); j++ ){
				if(tables.get(j) == table){
					break;
				}
				int lengthOfPreviousTable = getActualIndexFromJoin(tables.get(j));
				if(lengthOfPreviousTable != -1) {
					index = index+ lengthOfPreviousTable; //get the length of first table and add the index found to get new index
				}
			}
		}
		return index;
	}
	
	public Integer getExtraIndicesFromMultipleTables(Table table,String columnName,List<Table> tables, boolean isFirst)
	{
		String schema_name=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
//		int index=-1;
		int counter = 0;
		try {
			Scanner scanner=new Scanner(new FileReader(schema_name));
			while(scanner.hasNextLine())
			{
				String schema_info[]=scanner.nextLine().split(" ");
				if(schema_info[0].equals(table.getName()))
				{
					for(int i=1;i<schema_info.length;i++)
					{
						if(columnName.equals(schema_info[i]))
						{
//							index=i-1;
							counter = i;
							break;
						}
					}
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int result=-1;
		
		for(int j=0; j < tables.size(); j++ ){
			if(tables.get(j) == table){
				result = 0;
				continue;
			}
			int lengthOfPreviousTable = getActualIndexFromJoin(tables.get(j));
			if(lengthOfPreviousTable != -1) {
				result = result+ lengthOfPreviousTable; //get the length of first table and add the index found to get new index
			}
		}
		result= result+ counter;
		return result;
	
		
	}
	
	public String getTempDirectory() {
		return tempDirectory;
	}
	
	
	public void setTempDirectory(String tempDirectory) {
		this.tempDirectory = tempDirectory;
	}
	
	public boolean isBuildIndexes() {
		return buildIndexes;
	}

	public void setBuildIndexes(boolean buildIndexes) {
		this.buildIndexes = buildIndexes;
	}

	public boolean isEvaluateQueries() {
		return evaluateQueries;
	}

	public void setEvaluateQueries(boolean evaluateQueries) {
		this.evaluateQueries = evaluateQueries;
	}
	
	
	public String getIndexInfoFileName() {
		return indexInfoFileName;
	}

	public void setIndexInfoFileName(String indexInfoFileName) {
		this.indexInfoFileName = indexInfoFileName;
	}
	
	public String getIndexesPath() {
		return indexesPath;
	}

	public void setIndexesPath(String indexesPath) {
		this.indexesPath = indexesPath;
	}
}

package cs4321.practicum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 *  This class outputs 
 *                  the list of select expressions and list of indexExpressions 
 *                  from the select expression and sets the lowKey and highKey
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class IndexExpressionVisitor extends AbstractExpressionEvaluator {

	List<Expression> selectExpressions = new ArrayList<Expression>();
	
	List<Expression> indexExpressions =  new ArrayList<Expression>();
	Table indexTable = null;
	String indexColumnName = null;
	long lowKey = Long.MIN_VALUE;
	long highKey = Long.MAX_VALUE;
	
	/**
	 * constructor
	 * 
	 * @param indexTable
	 * @param indexColumnName
	 */
	public IndexExpressionVisitor(Table indexTable, String indexColumnName) {
		// TODO Auto-generated constructor stub
		this.indexTable = indexTable;
		this.indexColumnName = indexColumnName;
	}
	
	/**
	 * visits AndExpression and calls the left and right child
	 */
	@Override
	public void visit(AndExpression andExpression) {
		// TODO Auto-generated method stub
		if(andExpression.getLeftExpression() != null){
			andExpression.getLeftExpression().accept(this);
		}
		
		if(andExpression.getRightExpression() != null){
			andExpression.getRightExpression().accept(this);
		}	
	}
	
	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		//Goes straight into selectExpression
		checkAndAddExpressions(equalsTo,true);

	}
	

	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		//Goes straight into selectExpression
		selectExpressions.add(notEqualsTo);
	}
	
	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(GreaterThan greaterThan) {
		//might go into any expression , this must set lowKey = number +1
		checkAndAddExpressions(greaterThan,true);
//		buildExpressions(greaterThan);
	}
	
	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		//might go into any expression , this must set lowKey = number
		checkAndAddExpressions(greaterThanEquals,true);
	}
	
	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(MinorThan minorThan) {
		//might go into any expression , this must set highkey = number -1
		checkAndAddExpressions(minorThan,false);
	}
	
	/**
	 * Calls checkAndAddExpressions to build the separate index and select expressions and sets the lowKey and highKey value
	 */
	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		//might go into any expression , this must set highkey = number
		checkAndAddExpressions(minorThanEquals,false);
	}
	
	
	/**
	 * 
	 * This function contains the main logic to add the expressions to indexExpressions list or the select expressions list
	 * Also, the lowKey and highKey setting logic
	 * 
	 * Here we set the highKey and lowKey value correctly. if expression is R.A < 6 then highKey is 5. 
	 * 
	 *                                                     if the expression is R.A > 5 then lowKey is set to 6
	 * 
	 * if the expression is of the form Column expression Value 
	 * 
	 * 		lowKey = Value +1  if the expression is GreaterThan
	 * 
	 * 		lowKey = Value  if the expression is GreaterThanEquals
	 * 
	 * 		if the expression is of the form minor than then
	 * 
	 * 		lowKey = value +1
	 * 
	 * 		if the expression is of the form minor than equals then
	 * 		 
	 *      lowKey = value
	 * 
	 * 
	 * 	else if the expression is of the form Value expression Column
	 * 		
	 * 		highKey = Value -1  if the expression is GreaterThan
	 * 
	 * 		highKey = Value  if the expression is GreaterThanEquals
	 * 
	 * 		if the expression is of the form minor than then
	 * 
	 * 		highKey = value -1
	 * 
	 * 		if the expression is of the form minor than equals then
	 * 		
	 * 		highKey = value
	 * 
	 * 
	 *  else if expression is of the form R. A = 6
	 *		set lowKey and highKey to the same value 
	 * 			
	 * @param expression
	 * @param addLowKey
	 */
	private void checkAndAddExpressions(BinaryExpression expression,boolean addLowKey){
		if(isAnIndexExpression(expression) == true){
			indexExpressions.add(expression);
				if(expression instanceof GreaterThan){
					if(expression.getRightExpression() instanceof LongValue) {
						long newLowKey=((LongValue)expression.getRightExpression()).getValue() + (long)1.0;
						if(newLowKey > lowKey ){
							lowKey = newLowKey  ;
						}
					} else if(expression.getLeftExpression() instanceof LongValue) {
						long newHighKey=((LongValue)expression.getLeftExpression()).getValue() - (long)1.0;
						if(newHighKey < highKey ){
							highKey = newHighKey  ;
						}
					}
				} else if(expression instanceof GreaterThanEquals){
//					lowKey = number;
					if(expression.getRightExpression() instanceof LongValue) {
						long newLowKey =  ((LongValue)expression.getRightExpression()).getValue();
						if(newLowKey > lowKey ){
							lowKey = newLowKey  ;
						}
					} else if(expression.getLeftExpression() instanceof LongValue) {
						long newHighKey =  ((LongValue)expression.getLeftExpression()).getValue();
						if(newHighKey < highKey ){
							highKey = newHighKey  ;
						}
					}
				}else
//			} else {
				if(expression instanceof MinorThan){
//					highKey = number-1
					if(expression.getRightExpression() instanceof LongValue) {
						long newHighKey =  ((LongValue)expression.getRightExpression()).getValue() - (long)1.0;
						if(newHighKey < highKey ){
							highKey = newHighKey  ;
						}
					} else if(expression.getLeftExpression() instanceof LongValue) {
						long newLowKey =  ((LongValue)expression.getLeftExpression()).getValue() + (long)1.0;
						if(newLowKey > lowKey ){
							lowKey = newLowKey  ;
						}
					}
				} else if(expression instanceof MinorThanEquals){
//					highKey = number
					if(expression.getRightExpression() instanceof LongValue) {
						long newHighKey =  ((LongValue)expression.getRightExpression()).getValue() ;
						if(newHighKey < highKey ){
							highKey = newHighKey  ;
						}
					} else if(expression.getLeftExpression() instanceof LongValue) {
						long newLowKey =  ((LongValue)expression.getLeftExpression()).getValue() ;
						if(newLowKey > lowKey ){
							lowKey = newLowKey  ;
						}
					} 
				} else if(expression instanceof EqualsTo){
					if(expression.getRightExpression() instanceof LongValue) {
						long newKey =  ((LongValue)expression.getRightExpression()).getValue() ;
						if(newKey < highKey ){
							highKey = newKey  ;
						}
						if(newKey > lowKey) {
							lowKey = newKey;
						}
					} else if(expression.getLeftExpression() instanceof LongValue) {
						long newKey =  ((LongValue)expression.getLeftExpression()).getValue() ;
						if(newKey < highKey ){
							highKey = newKey  ;
						}
						if(newKey > lowKey) {
							lowKey = newKey;
						}
					}
					
				}
//			}
		} else {
			selectExpressions.add(expression);
		}
	}
	
	/**
	 * given and expression, this function returns true if the expression is an index expression
	 * @param expression
	 * @return
	 */
	private boolean isAnIndexExpression(BinaryExpression expression){
		
		if(expression.getLeftExpression() instanceof Column && expression.getRightExpression() instanceof LongValue){
			Table tableToCompare = ((Column)expression.getLeftExpression()).getTable();
			String columnnameToCompare = ((Column)expression.getLeftExpression()).getColumnName();
			if(isTableMatching(tableToCompare, indexTable) == true && columnnameToCompare != null && columnnameToCompare.equals(indexColumnName)){
				return true;
			}
		}else if (expression.getLeftExpression() instanceof LongValue && expression.getRightExpression() instanceof Column){
			Table tableToCompare = ((Column)expression.getRightExpression()).getTable();
			String columnnameToCompare = ((Column)expression.getRightExpression()).getColumnName();
			if(isTableMatching(tableToCompare, indexTable) == true && columnnameToCompare != null && columnnameToCompare.equals(indexColumnName)){
				return true;
			}
		}
		
		return false;
		
	}
	
	
	

	/**
	 * Method to check two tables given as arguments match
	 * @param tables
	 * @return
	 */
	private boolean isTableMatching(Table leftTable,Table rightTable){
		if(leftTable.getAlias()!= null) {
			if(leftTable.getAlias().equals(rightTable.getAlias())){
				 return true;
			}
		}else {
			if(rightTable.getAlias() != null) {
				if(leftTable.getName().equals(rightTable.getAlias())){
					 return true;
				}
			} else {
				if(leftTable.getName().equals(rightTable.getName())){
					 return true;
				}
			}
		}
		return false;
	}
	
	public List<Expression> getSelectExpressions() {
		return selectExpressions;
	}

	public void setSelectExpressions(List<Expression> selectExpressions) {
		this.selectExpressions = selectExpressions;
	}

	public List<Expression> getIndexExpressions() {
		return indexExpressions;
	}

	public void setIndexExpressions(List<Expression> indexExpressions) {
		this.indexExpressions = indexExpressions;
	}

	public long getLowKey() {
		return lowKey;
	}

	public void setLowKey(long lowKey) {
		this.lowKey = lowKey;
	}

	public long getHighKey() {
		return highKey;
	}

	public void setHighKey(long highKey) {
		this.highKey = highKey;
	}

	
	
}

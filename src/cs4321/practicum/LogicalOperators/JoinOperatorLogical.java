package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.Tuple;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;

/**
 * Class for Join Logical Operator. Would be converted to Physical Operator during Physical Query Plan building
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */

public class JoinOperatorLogical implements LogicalOperator {
	
	//Left and right child of the join operator
//	LogicalOperator leftChild;
//	LogicalOperator rightChild;
	ArrayList<Expression> unusableComparisions = null;
	ArrayList<LogicalOperator> childrenOfJoin = new ArrayList<LogicalOperator>();
	private boolean isVisited=false;


	//The left and the right tuple getting joined
	Tuple leftTuple=null;
	Tuple rightTuple =null;
	
	//Join Expression
	Expression expression;
	//The tables gettin joined
	List<Table> joinedTables;
	UnionFind unionFindForJoin = null;
	


	public JoinOperatorLogical(ArrayList<LogicalOperator> LogicalOperator, List<Table> tables, Expression expression, UnionFind unionFind) {
		// TODO Auto-generated constructor stub
		this.childrenOfJoin.addAll(LogicalOperator);
		unionFindForJoin = unionFind;
		this.expression = expression;
		this.joinedTables= tables;
			
	}
	
	public Expression getExpression() {
		return expression;
	}



	@Override
	public void accept(LogicalPlanVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}

	/**
	 * gets the tables part of the join 
	 * 
	 * @return
	 */
	public List<Table> getTables(){
		return this.joinedTables;
	}
	
	
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		return childrenOfJoin;
	}

	public void setChildrenOfJoin(ArrayList<LogicalOperator> childrenOfJoin) {
		this.childrenOfJoin = childrenOfJoin;
	}
	
	
	public UnionFind getUnionFindForJoin() {
		return unionFindForJoin;
	}

	public void setUnionFindForJoin(UnionFind unionFindForJoin) {
		this.unionFindForJoin = unionFindForJoin;
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "Join";
	}

	@Override
	public LogicalOperator getChild() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(getExpression()!=null)
			return "Join["+getExpression().toString()+"]";
		else
			return "Join[null]";
	}
	
	public ArrayList<Expression> getUnusableComparisions() {
		return unusableComparisions;
	}

	public void setUnusableComparisions(ArrayList<Expression> unusableComparisions) {
		this.unusableComparisions = unusableComparisions;
	}
	
}

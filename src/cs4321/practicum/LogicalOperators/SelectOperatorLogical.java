package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;

import net.sf.jsqlparser.expression.Expression;

/**
 * Class for Select Logical Operator. Would be converted to Physical Operator during Physical Query Plan building
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class SelectOperatorLogical implements LogicalOperator {

	private LogicalOperator child;
	private Expression whereClause;
	boolean isVisited=false;
//	Long lowerBound = null;
//	Long higherBound = null;
//	Long equalityConstraint = null;
	

	/**
	 * This constructor can be used if lower bound, higher bound and equality constraints are null
	 * @param child
	 * @param where
	 */
	public SelectOperatorLogical(LogicalOperator child, Expression where) {
		this.child = child;
		this.whereClause = where;
	}
	
//	public SelectOperatorLogical(LogicalOperator child, Expression where, Long lowerBound, Long higherBound, Long equalityConstraint ){
//		this.child = child;
//		this.whereClause = where;
//		this.lowerBound = lowerBound;
//		this.higherBound = higherBound;
//		this.equalityConstraint = equalityConstraint;
//	}


	public LogicalOperator getChild() {
		return child;
	}



	public Expression getWhere() {
		return whereClause;
	}

	public void setWhereClause(Expression whereClause) {
		this.whereClause = whereClause;
	}



	@Override
	public void accept(LogicalPlanVisitor visitor) {
		visitor.visit(this);
		
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "Select";
	}

	@Override
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "Select["+getWhere().toString()+"]";
	}

}

package cs4321.practicum.LogicalOperators;

/**
 * Visitor Interface with all the visit methods 
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public interface LogicalPlanVisitor {

	public void visit(ScanOperatorLogical scanOperatorLogical);
	public void visit(DuplicationEliminationOperatorLogical duplicationEliminationOperatorLogical);
	public void visit(SelectOperatorLogical selectOperatorLogical);
	public void visit(ProjectOperatorLogical projectOperatorLogical);
	public void visit(JoinOperatorLogical joinOperatorLogical);
	public void visit(SortOperatorLogical sortOperatorLogical);


	
}

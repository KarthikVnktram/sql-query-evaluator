package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;

/**
 * Interface for all Logical Operators
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public interface LogicalOperator {
	
	public void accept(LogicalPlanVisitor visitor);

	public String getOprName();
	
	public ArrayList<LogicalOperator> getChildrenOfJoin();
	
	public LogicalOperator getChild();
	
	public boolean getVisited();
	
	public void setVisited(boolean val);
	
	public String getTreePlanCondition();
	
}

package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;

import net.sf.jsqlparser.schema.Table;

/**
 * Class for Scan Logical Operator. Would be converted to Physical Operator during Physical Query Plan building
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class ScanOperatorLogical implements LogicalOperator {
	
	private Table fromTable;
	boolean isVisited=false;
	
	public ScanOperatorLogical(Table fromTable){
		this.fromTable=fromTable;
	}
	
	public Table getFrom(){
		return this.fromTable;
	}
	
	@Override
	public void accept(LogicalPlanVisitor visitor) {
		visitor.visit(this);
		
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "Leaf";
	}

	@Override
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LogicalOperator getChild() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "Leaf["+fromTable.getWholeTableName()+"]";
	}
}

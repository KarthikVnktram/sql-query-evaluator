package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SelectItem;

/**
 * Class for Projection Logical Operator. Would be converted to Physical Operator during Physical Query Plan building
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class ProjectOperatorLogical implements LogicalOperator{

	List<SelectItem> columnNames =null ; 
	LogicalOperator child ;
	boolean isVisited=false;
	
	List<Column> allColumns = null;
	
	public ProjectOperatorLogical(LogicalOperator child, List<SelectItem> columnNames) {
		this.columnNames  = columnNames;
		this.child = child;
	}
	
	public ProjectOperatorLogical(List<Column> columnNames, LogicalOperator child) {
		this.allColumns = columnNames;
		this.child = child;
	} 
	
	
	public List<SelectItem> getColumnNames() {
		return columnNames;
	}

	public LogicalOperator getChild() {
		return child;
	}
	
	public List<Column> getAllColumns() {
		return allColumns;
	}

	@Override
	public void accept(LogicalPlanVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "Project";
	}

	@Override
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(allColumns!=null)
			return null;
		else
			return "Project"+columnNames.toString();
	}

}

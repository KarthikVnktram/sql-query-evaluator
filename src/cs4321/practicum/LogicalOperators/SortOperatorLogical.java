package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.OrderByElement;

/**
 * Class for Sort Logical Operator. Would be converted to Physical Operator during Physical Query Plan building
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class SortOperatorLogical implements LogicalOperator{

	LogicalOperator child=null; 
	boolean isOrderByPresent = true;
	boolean isVisited=false;
	
	public LogicalOperator getChild() {
		return child;
	}

	List<OrderByElement> orderByElements=new ArrayList<OrderByElement>();
	List<Column> columns = new ArrayList<Column>();


	public List<OrderByElement> getOrderByElements() {
		return orderByElements;
	}

	public void setOrderByElements(List<OrderByElement> orderByElements) {
		this.orderByElements = orderByElements;
	}

	public SortOperatorLogical(LogicalOperator child,List<OrderByElement> orderByElements){
		this.child=child;
		this.orderByElements=orderByElements;
	}
	
	
	public SortOperatorLogical(List<Column> columns,LogicalOperator child){
		this.child=child;
		this.columns=columns;
	}
	
//	public SortOperatorLogical(HashMap<List<Table>, List<Column>> columnsToPerformDistinctOn,
//			ProjectOperatorLogical projectOperator) {
//		// TODO Auto-generated constructor stub
//		this.child = projectOperator;
//		
//		for(List<Table> tables : columnsToPerformDistinctOn.keySet()){
//			this.tables = tables;
//			break;
//			
//		}
//		this.columns = columnsToPerformDistinctOn.get(this.tables);
//	}

	public SortOperatorLogical(ProjectOperatorLogical projectOperator,List<Column> columns,boolean isOrderByPresent) {
		// TODO Auto-generated constructor stub
		this.child = projectOperator;
		this.columns = columns;
		this.isOrderByPresent = isOrderByPresent;
	}
	@Override
	public void accept(LogicalPlanVisitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}

	
	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}
	
	public boolean isOrderByPresent() {
		return isOrderByPresent;
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "Sort";
	}

	@Override
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(orderByElements.size()>0)
			return "Sort"+orderByElements.toString();
		else
			return "Sort"+columns.toString();		
	}
}

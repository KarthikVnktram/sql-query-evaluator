package cs4321.practicum.LogicalOperators;

import java.util.ArrayList;

/**
 * Class for Duplication Elimination Logical Operator
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class DuplicationEliminationOperatorLogical implements LogicalOperator {

	private LogicalOperator child;
	private boolean isVisited=false;
	
	public DuplicationEliminationOperatorLogical(LogicalOperator child) {
		super();
		this.child = child;
	}

	public LogicalOperator getChild() {
		return child;
	}


	@Override
	public void accept(LogicalPlanVisitor visitor) {
		visitor.visit(this);
		
	}

	@Override
	public String getOprName() {
		// TODO Auto-generated method stub
		return "DupElim";
	}

	@Override
	public ArrayList<LogicalOperator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited=val;
	}

	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "DupElim";
	}

	
}

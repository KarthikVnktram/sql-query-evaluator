package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.ExpressionEvaluator;
import cs4321.practicum.Tuple;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;

/**
 * Implementation of Block nested loop join 
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class BNLJOperator extends Operator{

	private Operator leftChild;
	private Tuple nextBlockPointer;
	private int maxTuplesInABlock;
	private ArrayList<Tuple> allTuplesPerBlock;
	private int maxTuplesInABlockIterator;
	private Tuple outerTuple ;
	private int blockPointer = 0;
	private Tuple innerTuple;
	private Expression matchingExpression;
	boolean hasNext = true;
	private Operator rightChild;
	boolean isVisited = false;
	List<Table> tables = new ArrayList<Table>();
	
	/**
	 * Inititalization of the BNLJ Operator
	 * @param Operator
	 * @param Operator
	 * @param Expression
	 * @param int
	 */

	public BNLJOperator(Operator leftChild,Operator rightChild, Expression match, int pages,List<Table> tables) {
		this.leftChild = leftChild;
		this.rightChild = rightChild;
		this.matchingExpression = match;
		outerTuple = leftChild.getNextTuple();
		if (outerTuple != null){
			//buffer_max_size contains the total number of tuples for the pages received as input ( it contains total number of tuples in a block )
			int size_of_a_tuple=4*outerTuple.tuple.size();
			maxTuplesInABlock = 4096*pages/size_of_a_tuple;
			nextBlockPointer = outerTuple;
			loadBuffer();
		}
		else {
			maxTuplesInABlock = 0;
			maxTuplesInABlockIterator = 0;
			allTuplesPerBlock=null;
		}
		innerTuple = this.rightChild.getNextTuple();
		this.tables.addAll(tables);
	}
	
	public List<Table> getTables() {
		return tables;
	}
	/**Method to load buffer in BNLJ
	 */
	private void loadBuffer(){
		allTuplesPerBlock = new ArrayList<Tuple>();
		Tuple currentTuplePointer = nextBlockPointer;
		for(maxTuplesInABlockIterator=0;maxTuplesInABlockIterator<maxTuplesInABlock;maxTuplesInABlockIterator++){
			if(currentTuplePointer==null){
				break;
			}
			allTuplesPerBlock.add(currentTuplePointer);
			currentTuplePointer=this.leftChild.getNextTuple();//updating the currentTuplePointer
		}
		nextBlockPointer = currentTuplePointer;
		blockPointer = 0;
	}

	/**
	 * To send the next tuple whenever requested by the parent.
	 * @return Tuple
	 */

	@Override
	public Tuple getNextTuple() {
		
		for (;maxTuplesInABlockIterator > 0;){//loop over all the blocks which is in terms of tuples
			while (innerTuple != null){//inner relation tuple loop
				for(;blockPointer < maxTuplesInABlockIterator;){//outer relation tuple loop
					outerTuple=allTuplesPerBlock.get(blockPointer);
					blockPointer+=1;
					List<Integer> outerTupleInstance = outerTuple.tuple;
					List<Integer> innerTupleInstance = innerTuple.tuple;
					List<Integer> joinedTupleInstance = new ArrayList<Integer>();
					//join both the tuples
					joinedTupleInstance.addAll(outerTupleInstance);
					joinedTupleInstance.addAll(innerTupleInstance);
					ArrayList<Table> tables = new ArrayList<Table>();
					for(Table table:outerTuple.getTables()){
						tables.add(table);
					}
					for(Table table:innerTuple.getTables()){
						tables.add(table);
					}
					Tuple joinedTuple = new Tuple(tables, joinedTupleInstance);
					if (matchingExpression != null){						
						ExpressionEvaluator expressionEvaluator = null;
						expressionEvaluator = new ExpressionEvaluator(outerTuple,innerTuple);//Evaluate the matching condition
						this.matchingExpression.accept(expressionEvaluator);
						if (expressionEvaluator.getResult()){
							return joinedTuple;
						}
					}
					else {
						return joinedTuple;
					}
				}
				blockPointer = 0;
				innerTuple = this.rightChild.getNextTuple();
			}
			if(true){
				this.rightChild.reset();
				innerTuple=this.rightChild.getNextTuple();
				this.loadBuffer();
			}
		}
		hasNext = false;
		return null;
	}

	
	/**
	 * to reset both the child of BNLJ Operator
	 */

	@Override
	public void reset() {
		this.leftChild.reset();
		this.rightChild.reset();
		
	}

	/**
	 * to close all unclosed children
	 */

	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.leftChild.closeAllUnclosed();
		this.rightChild.closeAllUnclosed();
	}

	public Operator getChild() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Method to get children of join
	 */
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		ArrayList<Operator> children = new ArrayList<Operator>();
		children.add(leftChild);
		children.add(rightChild);
		return children;
	}
	
	/**
	 * Method to get Visited for jion
	 */
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	/**
	 * method to set visited for DFS
	 */
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(matchingExpression!=null)
			return "BNLJ["+matchingExpression.toString()+"]";
		else
			return "BNLJ[null]";
	}
	
	


}

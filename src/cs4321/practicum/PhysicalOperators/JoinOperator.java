package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cs4321.practicum.ExpressionEvaluator;
import cs4321.practicum.Tuple;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;

/**
 *  This class outputs 
 *           Performs Join on two tables and returns the Tuples which satisfy the condition
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class JoinOperator extends Operator {
	
	//Left and right child of the join operator
	Operator leftChild;
	Operator rightChild;
	
	//Join Expression
	Expression expression=null;
	
	//The left and the right tuple getting joined
	Tuple leftTuple=null;
	Tuple rightTuple =null;
	
	//The tables gettin joined
	List<Table> joinedTables;
	boolean isVisited = false;
	
	
	public JoinOperator(Operator leftChild, Operator rightChild, List<Table> tables, Expression expression) {
		// TODO Auto-generated constructor stub
		this.leftChild = leftChild;
		this.rightChild = rightChild;
		this.expression = expression;
		this.joinedTables= tables;
		Tuple tuple = leftChild.getNextTuple();
		if(tuple != null) {
			leftTuple = tuple;
		} 
		
	}
	

	/**
	 * Takes the leftTuple and compares it to the right tuple
	 * if right tuple reaches the end of right table , then increment and check the next left Tuple
	 * 
	 * Evaluates the condition and returns the tuple satisfying the condition
	 * 
	 */
	@Override
	public Tuple getNextTuple() {
		// TODO Auto-generated method stub
		
		rightTuple = this.rightChild.getNextTuple();
		
		while( leftTuple !=null || rightTuple != null ){
			if(rightTuple == null) {
				Tuple tuple = this.leftChild.getNextTuple();
				if(tuple != null) {
					leftTuple = tuple;
					this.rightChild.reset();
					rightTuple = this.rightChild.getNextTuple();
					if(rightTuple == null){
						return null;
					}
				}else {
					return null;
				}
			}
			if(leftTuple == null){
				return null;
			}
			LinkedList<Integer> joinedValues = new LinkedList<Integer>();
			
			if(expression !=null){

				ExpressionEvaluator evaluator =  new ExpressionEvaluator(leftTuple,rightTuple);
				expression.accept(evaluator);
				if(evaluator.getResult()){
					joinedValues.addAll(leftTuple.tuple);
					joinedValues.addAll(rightTuple.tuple);
					LinkedList<Table> tables = new LinkedList<Table>();
					tables.addAll(leftTuple.getTables());
					tables.addAll(rightTuple.getTables());
					return new Tuple(tables,joinedValues);
				}else {
					rightTuple = this.rightChild.getNextTuple();
				}
			}  else{
				

				joinedValues.addAll(leftTuple.tuple);
				joinedValues.addAll(rightTuple.tuple);
				LinkedList<Table> tables = new LinkedList<Table>();
				tables.addAll(leftTuple.getTables());
				tables.addAll(rightTuple.getTables());
				return new Tuple(tables,joinedValues);
			}
			
		}
		return null;
		
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		this.leftChild.reset();
		this.rightChild.reset();
	}

	
	/**
	 * sets the tables part of join
	 * 
	 * @param tables
	 */
	public void setTables(List<Table> tables){
		this.joinedTables = tables;
	}
	
	/**
	 * gets the tables part of the join 
	 * 
	 * @return
	 */
	public List<Table> getTables(){
		return this.joinedTables;
	}


	/**
	 * 
	 * method to close all unclosed channels after query execution
	 */
	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.leftChild.closeAllUnclosed();
		this.rightChild.closeAllUnclosed();
	}
	
	public Operator getChild() {
		// TODO Auto-generated method stub
		return null;
	}


	
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		ArrayList<Operator> children = new ArrayList<Operator>();
		children.add(leftChild);
		children.add(rightChild);
		return children;
	}


	
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}


	
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited =  val;
	}


	/**
	 * Method to get the Tree Plan Condition
	 */
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(expression!=null)
			return "TNLJ["+expression.toString()+"]";
		else
			return "TNLJ[null]";
	}
	

}

package cs4321.practicum.PhysicalOperators;

import java.io.File;
import java.util.ArrayList;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import cs4321.practicum.io.TupleWriterBinaryFormat;
import cs4321.practicum.io.TupleWriterHumanReadable;
import cs4321.practicum.utilities.ConversionUtility;

/**
 * An Abstract class that every type of Operator extends so as to ensure minimum set of functionality to be performed by them using abstract functions.
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */


public abstract class Operator {
	/**
	 * Stores the path and name of the file on which dump() is needed to be performed on. 
	 */
	String FileName=DatabaseGlobalCatalog.getInstance().getOutputFilePath()+File.separator+DatabaseGlobalCatalog.getInstance().getOutputFileName();

	public abstract Tuple getNextTuple();
	
	public abstract void reset();
	/**
	 * To store the result of the query executed in appropriate location using FileName defined above.
	 * @param int
	 */
	public void dump(int fileNumber){
		FileName=FileName+fileNumber;
		TupleWriterBinaryFormat writer=new TupleWriterBinaryFormat(FileName);
		Tuple tuple =this.getNextTuple();
		if(tuple != null) {
			while(tuple != null){
				writer.write(tuple.tuple);
				tuple =this.getNextTuple();
			}
			writer.flush();
			writer.close();
		}
//		ConversionUtility.convertBinaryFileToHumanReadableFile(FileName, FileName+"_HumanReadable");
		this.closeAllUnclosed();
		this.reset();
	}
	
	public void dump(String filePath){
		
		TupleWriterBinaryFormat writer = new TupleWriterBinaryFormat(filePath);
		Tuple t = this.getNextTuple();
		if (t != null){//Edge case where the result is empty set, we should return an empty file
			while (t!=null){
				writer.write(t.tuple);
				t = this.getNextTuple();
			}
			writer.flush();
		}
		
	}
	
	/**
	 * Over Ridden in SortOperator and ExternalSortOperator
	 * @param i
	 */
	public void reset(int i){
		
	}
	
	
	public abstract void closeAllUnclosed();
	
	public abstract Operator getChild();
	
	public abstract ArrayList<Operator> getChildrenOfJoin();
	
	public abstract boolean getVisited();
	
	public abstract void setVisited(boolean val);
	
	public abstract String getTreePlanCondition();
}

package cs4321.practicum.PhysicalOperators;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import net.sf.jsqlparser.schema.Table;

/**
 * To Perform Scan Operation of Database Systems to read the tuples from a file one by one as and when requested by the parent
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class ScanOperator extends Operator {

	private Table fromTable;
	
	private TupleReaderBinaryFormat scanner;
   boolean isVisited = false;
	/**
	 * Constructor setting all the variables of scan operator.
	 * @param Table
	 */
	public ScanOperator(Table fromTable)
	{
		this.fromTable = fromTable;
		this.scanner=new TupleReaderBinaryFormat(DatabaseGlobalCatalog.getInstance().getDataPath()+File.separator+this.fromTable.getName());
	}
	
	public ScanOperator(Table fromTable, String path) {
		// TODO Auto-generated constructor stub
		this.fromTable = fromTable;
		this.scanner=new TupleReaderBinaryFormat(path);
	}
	/**
	 * To send the next tuple to the parent when requested for.
	 * @param Tuple
	 */
	@Override
	public Tuple getNextTuple() {
		
		List<Integer> tableValues = new ArrayList<Integer>();
    	//scanner.useDelimiter(",");
		List<Integer> tuple = scanner.getNextTuple();
		if(tuple != null){
			tableValues.addAll(tuple);
			 Tuple tupleOfTuple = new Tuple(fromTable, tableValues);
            return tupleOfTuple;
		}
        return null;
           
	}
	/**
	 * To set the pointer to the beginning of the file to start reading the tuples again from start.
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		scanner.close();
		scanner.reset();
	}
	
	/**
	 * To check whether the input file contains more tuples or not.
	 */	
		
	public Table getFromTable() {
		return fromTable;
	}

	@Override
	public void reset(int i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.scanner.close();
	}

	public TupleReaderBinaryFormat getScanner() {
		return scanner;
	}

	public void setScanner(TupleReaderBinaryFormat scanner) {
		this.scanner = scanner;
	}

	public Operator getChild() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "TableScan["+fromTable.getWholeTableName()+"]";
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}
}

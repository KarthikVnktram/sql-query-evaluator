package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.Tuple;

/**
 * Class for DISTINCT Operation. Eliminates duplicates
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class DuplicateEliminationOperator extends Operator {

	private Tuple tupleToKeepTrack;

	Operator child;
	boolean isVisited = false;
	/**
	 * Constructor only performs certain operations as soon as it is initialize so as to prevent calling it from main class.
	 * @param child
	 */
	public DuplicateEliminationOperator(Operator child) {
		// TODO Auto-generated constructor stub
		this.child=child;
		this.tupleToKeepTrack = null;

	}
	/**
	 * to send next tuple whenever it is requested by its parent
	 * @param Tuple
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple nextTuple = this.child.getNextTuple();
		if (tupleToKeepTrack != null){
			while (nextTuple != null){

				if(isTupleEqual(nextTuple, this.tupleToKeepTrack)) {
					nextTuple = this.child.getNextTuple();

				} else {

					tupleToKeepTrack=nextTuple;
					break;

				}
			}
			return nextTuple;

		} else {
			if (nextTuple != null){
				tupleToKeepTrack = nextTuple;
				return nextTuple;
				
			} else {
				return null;
			}
		}
	}

	@Override
	public void reset() {
		this.child.reset();
	}

	public boolean isTupleEqual(Tuple firstTuple, Tuple secondTuple){

		List<Integer> tupleFirstList = firstTuple.tuple;
		List<Integer> tupleSecondList = secondTuple.tuple;

		boolean isEquals = true;

		for(int i = 0 ; i < tupleFirstList.size() ; i++){
			if(tupleFirstList.get(i).equals(tupleSecondList.get(i)) == false){
				return false;
			}
		}
		return isEquals;
	}
	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.child.closeAllUnclosed();
	}
	
	public Operator getChild() {
		// TODO Auto-generated method stub
		return child;
	}
	@Override
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}
	@Override
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}
	
	/**
	 * Method to get the Tree Plan Condition
	 */
	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "DupElim";
	}

}

package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

/**
 * To Perform Project Operation of Database Systems.
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */


public class ProjectOperator extends Operator {

	List<SelectItem> columnNames  ; 

	Operator child ;
	String line;
	boolean isVisited=false;
	/**
	 * columnsFiltered is used to store all the columns present in the Project Operator.
	 */
	List<Column> columnsFiltered = new ArrayList<Column>();
	/**
	 * It stores all the columns of the table when * is used e.g. SELECT * 
	 */
	List<Column> allColumns = new ArrayList<Column>();
	

	/**
	 * Constructor Overloading
	 */
	public ProjectOperator(Operator child, List<SelectItem> columnNames) {
		this.columnNames  = columnNames;
		this.child = child;
	}
	
	public ProjectOperator(List<Column> columnNames, Operator child) {
		this.allColumns = columnNames;
		this.child = child;
	} 
	/**
	 * To send the next tuple to the parent whenever request for it is received.
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple tuple;
		
		while( child != null && (tuple=child.getNextTuple()) != null){
			List<Integer> columnData = new ArrayList<Integer>(); 
			/**
			 * ColumnsNames is a List<SelectItem> storing the same thing as columnsFiltered.
			 */
			if(columnNames != null) {
				for(SelectItem columnName: columnNames ){
					if(columnName instanceof SelectExpressionItem){
						Column column = (Column)((SelectExpressionItem) columnName).getExpression();
						if(tuple.table != null){
							columnData.add(DatabaseGlobalCatalog.getInstance().getAttributes(tuple, column));
						} else {
							Table tableToCheck = null;
							boolean isFirst = false;
							for( int i = 0 ; i < tuple.tables.size() ; i++ ){
								Table currentTable = tuple.tables.get(i);
								String tableName = null;
								String columnTableName = null;
								if(currentTable.getAlias() != null){
									tableName = currentTable.getAlias();
								} else {
									tableName =  currentTable.getName();
								}
								
								if(column.getTable().getAlias() != null){
									columnTableName = column.getTable().getAlias();
								} else {
									columnTableName =  column.getTable().getName();
								}
								if(tableName.equals(columnTableName)){
									tableToCheck = currentTable;
									if(i==0){
										isFirst =true;
									}
									break;
								}
							}
							columnData.add(DatabaseGlobalCatalog.getInstance().getAttributesFromTable(tuple, tableToCheck,column,isFirst));
						}
					} 
					
				}
			} else if(allColumns != null){
					for(Column column : allColumns){
												
						if(tuple.table != null){
							columnData.add(DatabaseGlobalCatalog.getInstance().getAttributes(tuple, column));
						} else {
							Table tableToCheck = null;
							boolean isFirst = false; //tuple.tables
							for( int i = 0 ; i < tuple.tables.size() ; i++ ){
								Table currentTable = tuple.tables.get(i);
								String tableName = null;
								String columnTableName = null;
								if(currentTable.getAlias() != null){
									tableName = currentTable.getAlias();
								} else {
									tableName =  currentTable.getName();
								}
								
								if(column.getTable().getAlias() != null){
									columnTableName = column.getTable().getAlias();
								} else {
									columnTableName =  column.getTable().getName();
								}
								if(tableName.equals(columnTableName) ){
									tableToCheck = currentTable;
									if(i ==0){ 
										isFirst =true;
									}
									break;
								}
							}
							columnData.add(DatabaseGlobalCatalog.getInstance().getAttributesFromTable(tuple, tableToCheck,column,isFirst));
						}
					}
				
			}
			if(tuple.table != null){
				
				return new Tuple(tuple.table, columnData);
				
			} else {
				
				return new Tuple(tuple.tables, columnData);
				
			}
			
		} 
		return null;
	}

	
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		child.reset();
	}

	/**
	 * Store all the columns to be projected in the returned variable
	 * @return List<Column>
	 */
	public List<Column> getColumnFiltered(){
		columnsFiltered = new ArrayList<Column>();
		if(columnNames != null){
			for(SelectItem columnName: this.columnNames){
				if(columnName instanceof SelectExpressionItem){
					Column column = (Column)((SelectExpressionItem) columnName).getExpression();
					columnsFiltered.add(column);
				}
			}
		} else {
			for(Column columnName: this.allColumns){
				columnsFiltered.add(columnName);
			}
		}
		return columnsFiltered;
	}

	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.child.closeAllUnclosed();
		
	}	
	
	public Operator getChild(){
		return child;
	}
	
	public boolean getVisited(){
		return isVisited;
	}
	
	public void setVisited(boolean val){
		isVisited = val; 
	}
	
	public String getTreePlanCondition(){
		if(allColumns.size() != 0)
			return null;
		else
			return "Project"+columnNames.toString();
	}
	
	/**
	 * Method to get the Tree Plan Condition
	 */
	@Override
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}

}

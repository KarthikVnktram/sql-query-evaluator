package cs4321.practicum.PhysicalOperators;



/**
 *  This class performs the Sort Merge Join on two Relations
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.SMJWhereClauseVisitor;
import cs4321.practicum.Tuple;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

public class SMJOperator extends Operator{

	private Operator leftChild;
	private Operator rightChild;
	
	
	private Tuple leftTuple;
	private Tuple rightTuple;
	private int leftPointer;
	private Expression expression=null;
	private Tuple lastRightTuple;
	private Map<Table,List<Column>> tableAttributesMap;
	int restoreIndex=1;
	private List<Table> joinedTables = new ArrayList<Table>();
	boolean hasNext = true;
	boolean isVisited = false;
	
	/**
	 * Constructor
	 * @param leftChild
	 * @param rightChild
	 * @param expression
	 * @param numberBufferPagesJoin if buffer is provided. Not in this case.
	 * @param joinedTables
	 */
	public SMJOperator(Operator leftChild, Operator rightChild, Expression expression, int numberBufferPagesJoin, List<Table> joinedTables)
	{
		this.leftChild=leftChild;
		this.rightChild=rightChild;
		this.expression=expression;
		leftTuple = leftChild.getNextTuple();
		rightTuple = rightChild.getNextTuple();
		this.joinedTables.addAll(joinedTables);
		
	}
	
	/**
	 * get the next joined tuple
	 */
	@Override
	public Tuple getNextTuple()
	{
		while(leftTuple!=null){	
			
			if(rightTuple!=null)
			{
					int comparedValue=compareTuples(leftTuple,rightTuple);
					if(comparedValue==0)//both are equal
					{
						Tuple joinedTuple= joinedTuple(leftTuple, rightTuple);
						lastRightTuple=rightTuple;
						this.rightTuple=this.rightChild.getNextTuple();
						return joinedTuple;
					}
					else if(comparedValue==-1)//right is greater 
					{
						leftTuple=this.leftChild.getNextTuple();
						lastRightTuple=rightTuple;
						this.rightChild.reset(restoreIndex);
						this.rightTuple = this.rightChild.getNextTuple();
					}
					else //left is greater
					{
						lastRightTuple=rightTuple;
						rightTuple = this.rightChild.getNextTuple();
						restoreIndex++;
					}
					
			}else
			{
				if(lastRightTuple == null) {
					return null;
				}
				leftTuple=leftChild.getNextTuple();
				if(leftTuple!=null)
				{
					int comparedValue=compareTuples(leftTuple, lastRightTuple);
					if(comparedValue==0){
						this.rightChild.reset(restoreIndex);
						this.rightTuple=this.rightChild.getNextTuple();
					}
					else{
						hasNext = false;
						break;
					}
				}
				else{
					hasNext = false;
					break;
				}
			}
		}
		hasNext = false;
		return null;
		
	}
	
	
	/*
	 * the tuples are compared and returns -1(left<right), 0(left=right) , 1(left > right)
	 * @param Tuple
	 * @param Tuple
	 * @return int
	 */

	private int compareTuples(Tuple leftTuple, Tuple rightTuple) {
		int comparedValue = 0;
		SMJWhereClauseVisitor smjWhereClauseVisitor=new SMJWhereClauseVisitor();
		smjWhereClauseVisitor.setLeftTuple(leftTuple);
		smjWhereClauseVisitor.setRightTuple(rightTuple);
		expression.accept(smjWhereClauseVisitor);
		tableAttributesMap=smjWhereClauseVisitor.getTableAttributeMap();
		List<Column> columnsLeft = smjWhereClauseVisitor.getLeftColumns();
		List<Column> columnsRight = smjWhereClauseVisitor.getRightColumns();
		boolean isFirst = false;
		for(int i=0 ; i < columnsLeft.size() ; i++){
			isFirst = isFirstTable(columnsLeft.get(i).getTable(),leftTuple.tables);
			int leftValue=DatabaseGlobalCatalog.getInstance().getAttributesFromTableWithColumnName(leftTuple, columnsLeft.get(i).getTable(), columnsLeft.get(i).getColumnName(), isFirst);
			int rightValue=DatabaseGlobalCatalog.getInstance().getAttributesFromTableWithColumnName(rightTuple, columnsRight.get(i).getTable(), columnsRight.get(i).getColumnName(), true);
			if(leftValue==rightValue){
				comparedValue=0;
				continue;
			}
			else if(leftValue<rightValue){
				comparedValue=-1;
				return comparedValue;
			}
			else{
				comparedValue=1;
				return comparedValue;
			}
			
		}
		return comparedValue;
		
		/*List<Table> table=leftTuple.getTables();
		boolean isFirst=false;
		List<Table> tables=new ArrayList<Table>();//contains the tables of tableAttributeMap
		List<Table> actualtables = new ArrayList<Table>();		
		for(Table key:tableAttributesMap.keySet())
		{   
			String toCompare = "";
			if(table.get(0).getAlias() != null){
				toCompare = table.get(0).getAlias();
			} else {
				toCompare = table.get(0).getName();
			}
			
			if(key.getName().equals(toCompare))
				isFirst=true;
		}
		int flag=0;
		for(Table key:tableAttributesMap.keySet())
		{		
			for(Table checktable: leftTuple.getTables())
			{	
				String checkTable = "";
				if(checktable.getAlias() != null){
					checkTable = checktable.getAlias();
				} else {
					checkTable = checktable.getName();
				}
				if(checkTable.equals(key.getName()))
				{	
					if(flag==0)
					{
						actualtables.add(checktable);
						tables.add(key);
						flag=1;
					}
					else
					{
						actualtables.set(0,checktable);
						tables.set(0,key);
					}
				}									
			}
			
			for(Table checktable :rightTuple.getTables())
			{
				String checkTbl = "";
				if(checktable.getAlias() != null){
					checkTbl = checktable.getAlias();
				} else {
					checkTbl = checktable.getName();
				}
				if(checkTbl.equals(key.getName()))
				{
					if(flag==0)
					{						
						actualtables.add(checktable);
						tables.add(key);
						actualtables.add(checktable);
						tables.add(key);
						flag=1;
					}
					else
					{
						actualtables.add(checktable);
						tables.add(key);
					}
				}
			}
			//tables.add(key);
		}*/
		
	}
		
	private boolean isFirstTable(Table table, List<Table> tables) {
		// TODO Auto-generated method stub
		
		if(tables != null && tables.size() > 0){
			
			for(int i=0; i < tables.size(); i++){
				if( i==0 && tableStringToCompare(table).equals(tableStringToCompare(tables.get(i)))){
					return true;
				}
			}
			
		}
		if(tables == null && table != null){
			return true;
		} else {
			return false;
		}
	}

	private String tableStringToCompare(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
		
	}
	/**
	 * Code to combine the tuples whenever the join condition is successful.
	 * @param Tuple
	 * @param Tuple
	 * @return Tuple
	 */
	private Tuple joinedTuple(Tuple leftTuple,Tuple rightTuple)
	{
		List<Integer> outerTupleInstance = leftTuple.tuple;
		List<Integer> innerTupleInstance = rightTuple.tuple;
		List<Integer> joinedTupleInstance = new ArrayList<Integer>();
		//join both the tuples

		joinedTupleInstance.addAll(outerTupleInstance);
		joinedTupleInstance.addAll(innerTupleInstance);
		ArrayList<Table> tables = new ArrayList<Table>();
		tables.addAll(leftTuple.getTables());
		tables.addAll(rightTuple.getTables());
		Tuple joinedTuple = new Tuple(tables, joinedTupleInstance);
		return joinedTuple;
	}


	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
	
	public List<Table> getJoinedTables() {
		return joinedTables;
	}

	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.leftChild.closeAllUnclosed();
		this.rightChild.closeAllUnclosed();
	}

	public Operator getChild() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(expression!=null)
			return "SMJ["+expression.toString()+"]";
		else
			return "SMJ[null]";
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		ArrayList<Operator> children = new ArrayList<Operator>();
		children.add(leftChild);
		children.add(rightChild);
		return children;
	}

}

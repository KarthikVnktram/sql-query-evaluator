
package cs4321.practicum.PhysicalOperators;
import cs4321.TreeNodes.Rid;
import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.OrderByElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 * To Perform Sort/OrderBy Operation of Database Systems.
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */


public class SortOperator extends Operator {
	/**
	 * allTuples stores all the tuples received for child in it.
	 */
	ArrayList<Tuple> allTuples=new ArrayList<Tuple>();

	Operator child=null; 
	List<OrderByElement> orderByElements=new ArrayList<OrderByElement>();
	/**
	 * It stores the indices of the attributes according to the position of the attributes in Schema.txt 
	 */
	List<Integer> indices=new ArrayList<Integer>();
	/**
	 * Used in getNextTuple() to ensure proper passage of tuples to the parent operator.
	 */
	int sortCurrentTupleIndex;
	Table table = null;
	List<Table> tables = new ArrayList<Table>();
	List<Table> joinedTables;
	boolean isVisited = false;
	/**
	 * A number of methods mandatory for successful operation of SortOperator are called from the constructor to avoid calling it from the main method.
	 * @param Operator
	 * @param List<OrderByElement>
	 */
	public SortOperator(Operator child,List<OrderByElement> orderByElements){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.orderByElements=orderByElements;
		this.setAllTuplesForSort();
		this.setIndices(((ProjectOperator)this.child).getColumnFiltered());
	}
	
	public SortOperator(Operator child,Table table, List<Column> columnNames){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.table = table;
		this.setAllTuplesForSort();
		setIndicesForColumnNames(columnNames, this.table);
	}
	
	/**
	 * This is exclusively used for Index Sort
	 * 
	 * @param child
	 * @param table
	 * @param columnNames
	 */
	public SortOperator(Table table, Operator child,List<Column> columnNames){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.table = table;
		int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columnNames.get(0).getColumnName());
		this.setAllTuplesForIndexSort(index);
		setIndicesForColumnNames(columnNames, this.table);
	}
	
	
	public SortOperator(List<Column> columnNames,Operator child){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.setAllTuplesForSort();
		setIndicesForAllColumnNames(columnNames);
	}
	
	public SortOperator(Operator operator, Table table,List<Column> columnNames, List<Table> joinedTables) {
		// TODO Auto-generated constructor stub
		this.child=operator;
		this.sortCurrentTupleIndex=0;
		this.table = table;
		this.joinedTables = joinedTables;
		this.setAllTuplesForSort();
		setIndicesForColumnNamesMultiple(columnNames, this.table, this.joinedTables);
	}

	/**
	 * To read all the tuples from the child Operator and store it in the buffer. This is needed to be done as sort is a blocking operator and needs all the tuples for performing its operation
	 */
	@Override
	public Tuple getNextTuple() {
		
		while(this.sortCurrentTupleIndex<allTuples.size())
		{
			Tuple tuple= allTuples.get(sortCurrentTupleIndex);
			this.sortCurrentTupleIndex+=1;
			return tuple;
		}
		this.sortCurrentTupleIndex=0;
		return null; 
		// TODO Auto-generated method stub
		
	}
	/**
	 * It uses the Schema.txt to set indices on the basis of which the sorting happens in the comparator. E.g Select A,B from Sailors Order By B. In this case, the indices will be [1,0,2] which corresponds to [B,A,C] which is according to the prescribed requirements.
	 * @param List<Column>
	 */
	public void setIndices(List<Column> columnsFiltered)
	{
		//#tag
		indices.clear();
		for(int i=0;i<orderByElements.size();i++)
		{	
		    int index = getIndexMatch(columnsFiltered,orderByElements.get(i));
			if( index != -1){
				this.indices.add(index);
			} else {
				
			}
		}
		for(int i=0;i<columnsFiltered.size();i++)
		{
			if(isPresent(columnsFiltered.get(i))==false)
			{
				this.indices.add(i);
			}
		}
	}
	
	/**
	 * returns the index where the match between the orderbyelement and column of Project occurs.
	 * @param List<Column>
	 * @param OrderByElement
	 * @return int
	 */
	
	private int getIndexMatch(List<Column> columnsFiltered, OrderByElement columnToCompare) {
		// TODO Auto-generated method stub
		for(int i=0 ; i < columnsFiltered.size();i++) {
			
			String tableToCompare = "";
			if(columnsFiltered.get(i).getTable().getAlias() != null){
				tableToCompare = columnsFiltered.get(i).getTable().getAlias();
			} else {
				tableToCompare = columnsFiltered.get(i).getTable().getName();
			}
			
			if(columnToCompare.toString().equals(columnsFiltered.get(i).toString())){
				return i;
			} else if(columnToCompare.toString().equals(tableToCompare+"."+columnsFiltered.get(i).getColumnName())){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Check whether OrderByElements is present in columnFiltered
	 * @param Column
	 * @return boolean
	 */
	
	private boolean isPresent(Column columnFiltered)
	{
		for(int i=0;i<orderByElements.size();i++)
		{
			if(orderByElements.get(i).toString().equals(columnFiltered.toString()))
				{
					return true;
				}
		}
		return false;
	}
	/**
	 * Store all the tuples obtained from the child to help SortOperator perform its sort operation 
	 */
	public void setAllTuplesForSort() {
		Tuple temp=this.child.getNextTuple();
		while(temp != null)
		{
			this.allTuples.add(temp);
			temp=this.child.getNextTuple();
		}
		return;
		// TODO Auto-generated method stub
		
	}
	
	HashSet<Integer> distinctTuples = new HashSet<Integer>();

	public void setAllTuplesForIndexSort(int index) {
		Tuple temp=this.child.getNextTuple();
		int numberOfTuplesPerPage = ((TupleReaderBinaryFormat)((ScanOperator)this.child).getScanner()).getNumTuplesPerPage();
		int currentTupleIndex = -1;
		int currentPageIdentifier = 0;
		while(temp != null)
		{
			currentTupleIndex++;
			Rid rid = new Rid(currentPageIdentifier, currentTupleIndex);
			temp.setRid(rid);
			distinctTuples.add(temp.tuple.get(index));
			if(currentTupleIndex == numberOfTuplesPerPage){
				currentTupleIndex = 0;
				currentPageIdentifier++;
			}
			
			this.allTuples.add(temp);
			temp=this.child.getNextTuple();
		}
		return;
		// TODO Auto-generated method stub
		
	}

	public HashSet<Integer> getDistinctTuples() {
		return distinctTuples;
	}

	public void setDistinctTuples(HashSet<Integer> distinctTuples) {
		this.distinctTuples = distinctTuples;
	}
	
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		this.child.reset();
	}

	
	/**
	 * Perform the sort operation on all the tuples obtained from the child and stored in the buffer
	 */
	public void sorter()
	{
		
		if(allTuples.size()>0){
			setRemainingIndices(allTuples.get(0).tuple.size());
		}
		/**
		 * If the total number of tuples is 1 or 0, no need to perform sort operation.
		 */
		if(allTuples.size()>1)
		{
			Collections.sort(allTuples,new OrderByComparator(indices));
		}
	}
	
	public void setRemainingIndices(int index){
		
		for(int i = 0; i< index ; i ++){
			if(this.indices != null && this.indices.contains(i) == false){
				this.indices.add(i);
			}
		}
		
	}
	
	public void setIndicesForColumnNames(List<Column> columnsNames, Table table){
		
		for(Column column : columnsNames){
			String columname = column.getColumnName();
			if(columname.contains(".")){
				columname = columname.split("\\.")[1];
			}
			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columname);
			indices.add(index);	
		}

	}
	
	public void setIndicesForAllColumnNames(List<Column> columnNames){
		
		for(int i = 0 ; i < columnNames.size(); i++){
			indices.add(i);	
		}

	}

	
	/**
	 * method used in SMJOperator.java . This resets the outer relation to an index specified . So that we can rerun the merge from the new index
	 */
	@Override
	public void reset(int indexToReset) {
		// TODO Auto-generated method stub
		
		sortCurrentTupleIndex = indexToReset-1;
	}
	
	/**
	 * Method to set indices for sorting 
	 * @param columnsNames
	 * @param table
	 * @param tables
	 */
	public void setIndicesForColumnNamesMultiple(List<Column> columnsNames, Table table, List<Table> tables){
		indices.clear();
		for(Column column : columnsNames){
			String columname = column.getColumnName();
			if(columname.contains(".")){
				columname = columname.split("\\.")[1];
			}
			boolean isFirst = false;

			if(tables == null) {
				isFirst = true;
			} else {
				for(int i=0; i< tables.size(); i++){
					String tableToCompareLeft = "";
					
					if(table.getAlias() != null){
						tableToCompareLeft = table.getAlias();
					} else {
						tableToCompareLeft = table.getName();
					}
				
					String tableToCompareRight = "";
					
					if(tables.get(i).getAlias() != null){
						tableToCompareRight = tables.get(i).getAlias();
					} else {
						tableToCompareRight = tables.get(i).getName();
					}
					
					if(i ==0 && tableToCompareLeft.equals(tableToCompareRight)){
						isFirst = true;
						break;
					}
				}
			}
			int index = DatabaseGlobalCatalog.getInstance().getIndexFromMultipleTables(table, columname, tables, isFirst);
//			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columname);
			indices.add(index);	
		}
	}

	
	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.child.closeAllUnclosed();
		
	}
	
	public ArrayList<Tuple> getAllTuples() {
		return allTuples;
	}

	public void setAllTuples(ArrayList<Tuple> allTuples) {
		this.allTuples = allTuples;
	}

	public Operator getChild() {
		// TODO Auto-generated method stub
		return child;
	}


	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}


	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}


	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		
		return "InMemorySort"+orderByElements.toString();
			
	}
}

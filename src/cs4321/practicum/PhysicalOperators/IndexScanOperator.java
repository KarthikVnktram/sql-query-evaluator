package cs4321.practicum.PhysicalOperators;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import cs4321.TreeNodes.IndexNode;
import cs4321.TreeNodes.LeafNode;
import cs4321.TreeNodes.Node;
import cs4321.TreeNodes.Rid;
import cs4321.practicum.Tuple;
import cs4321.practicum.io.BinaryTreeReader;
import cs4321.practicum.io.TupleReaderBinaryFormat;

/**
 * Operator to perform Index scan and return the next tuple based on lowKey and highKey
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 */
public class IndexScanOperator extends Operator{
	Table fromTable;

	TupleReaderBinaryFormat scanner;

	long lowKey = 0;

	long highKey = 0;

	int initialKeyIndicator=0;
	int  initialRidIndicator=0;
	LeafNode leafNodeInContext;
	int initAddrOfLeaf = 1;

	int addressOfRootPage=1;
	int numberOfLeaves=0;
	int order = 0;


	String indexFilePath;
	boolean isClustered;


	boolean isInitialTupleBeingFetched;
	BinaryTreeReader binaryTreeReader;

	int indexColumn;

	int firstLeafNodeAddress;
	LeafNode firstLeafNode;
	
	String columnName;

	int firstKeyPointer;
	boolean isVisited = false;
	//String columnName="columnName";

	/**
	 * constructor to initialize the IndexScan Operator
	 * @param fromTable
	 * @param baseTablePath
	 * @param lowKey
	 * @param highKey
	 * @param indexPath
	 * @param clustered
	 * @param columnIndex
	 */
	public IndexScanOperator(Table fromTable, String baseTablePath,long lowKey, long highKey,String indexPath, boolean clustered,int columnIndex,String columnName) {
		// TODO Auto-generated constructor stub
		this.fromTable = fromTable;
		this.scanner=new TupleReaderBinaryFormat(baseTablePath);
		this.lowKey = lowKey; // lowKey being equal to Long.MinValue indicates that no lowKey found
		this.highKey = highKey;// highKey being equal to Long.MinValue indicates that no highKey found
		this.indexFilePath = indexPath;
		this.isClustered =clustered;
		this.columnName = columnName;
		this.indexColumn = columnIndex;
		this.isInitialTupleBeingFetched = true;

		try {
			FileInputStream fileInputStream = new FileInputStream(indexFilePath);
			FileChannel channel = fileInputStream.getChannel();
			ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
			buffer.clear();
			//read data from file to buffer
			channel.read(buffer);
			this.addressOfRootPage = buffer.getInt(0);
			this.numberOfLeaves= buffer.getInt(4);
			this.order = buffer.getInt(8);
			channel.close();
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Method to identify the first Leaf Node pointer
		this.init();
		this.firstKeyPointer= initialKeyIndicator;
		this.firstLeafNodeAddress = initAddrOfLeaf;
		this.firstLeafNode = leafNodeInContext;


	}

	
	/**
	 * method to update all the indicators and the leaf node appropriately
	 * @param leafKeys
	 * @param key
	 */
	private void updateValues(ArrayList<Integer> leafKeys,int key) {
		// TODO Auto-generated method stub
		initialRidIndicator++;
		if (initialRidIndicator >= leafNodeInContext.getLeafDataEntry().get(key).size()){
			initialKeyIndicator++;
			initialRidIndicator=0;
			if (initialKeyIndicator >= leafKeys.size()){
				initialKeyIndicator=0;
				initAddrOfLeaf++;
				if (initAddrOfLeaf>numberOfLeaves){
					leafNodeInContext=null;
				}
				else{
					leafNodeInContext = (LeafNode)binaryTreeReader.readPageWithPageNumber(initAddrOfLeaf);
				}
			}
		}
	}
	
	
	/**
	 * 
	 * Method to find and return the first tuple
	 * 
	 * @param newTupleToReturn
	 * @return
	 */
	private Tuple handleFirstNode(Tuple newTupleToReturn) {
		// TODO Auto-generated method stub

		ArrayList<Integer> leafKeys  = new ArrayList<Integer>(leafNodeInContext.getLeafDataEntry().keySet());
		int keyConsidered = leafKeys.get(initialKeyIndicator);
		if (highKey != Long.MAX_VALUE && keyConsidered <= highKey){
			Rid rid = leafNodeInContext.getLeafDataEntry().get(keyConsidered).get(initialRidIndicator);
			scanner.pointToPage(rid.getPageId(), rid.getTupleId());
			List<Integer> tuple = scanner.getNextTuple();
			if (tuple != null){
				newTupleToReturn = new Tuple(fromTable, tuple);
				updateValues(leafKeys, keyConsidered);
			}
		}else if (highKey == Long.MAX_VALUE){

			Rid rid = leafNodeInContext.getLeafDataEntry().get(leafKeys.get(initialKeyIndicator)).get(initialRidIndicator);
			scanner.pointToPage(rid.getPageId(), rid.getTupleId());
			List<Integer> tuple = scanner.getNextTuple();
			if (tuple != null){
				newTupleToReturn = new Tuple(fromTable, tuple);
				updateValues(leafKeys,keyConsidered);
				

			}

		}
		return newTupleToReturn;


	}
	
  /**
	*if after the init() method call, no leaf node found 
	*	return null
	*
	*	if clustered index 
	*		read the tuple from the filereader, already pointing to the correct page and offset
	*		
	*		if tuple is found 
	*			check if the tuple is in the range of <= highKey
	*			return the tuple to the operator above
	*	
	*	else if unclustered index 
	*
	*		get the Key set by init() method
	*		
	*		check if key <= highKey
	*		
	*		get the rid specific to the key
	*		
	*		point the filereader to the page corrsponding to the page id and tuple id from the tuple
	*		
	*		read the next tuple and return it to the higher order operators
	*
    * @return
    */
	@Override
	public Tuple getNextTuple() {
		// TODO Auto-generated method stub

		Tuple newTupleToReturn = null;
		if (this.isInitialTupleBeingFetched){
			if (leafNodeInContext == null){
				return null;
			}else {
				newTupleToReturn = handleFirstNode(newTupleToReturn);
			}
		}else {
			if (isClustered){
				List<Integer> tuple = scanner.getNextTuple();
				if (tuple != null){
					if (highKey != Long.MAX_VALUE && tuple.get(indexColumn) <= highKey){
						newTupleToReturn = new Tuple(fromTable, tuple);
					}
					else if (highKey == Long.MAX_VALUE){
						newTupleToReturn = new Tuple(fromTable, tuple);
					}
				}
			} else {
				if (leafNodeInContext == null){
					return null;
				}
				else {
					newTupleToReturn = handleNonFirstNode(newTupleToReturn);
				}

			}
		}
		return newTupleToReturn;

	}
/**
*	Open a file pointer to the indexFile
*	
*	Read the root
*	
*	if lowKey has been set and not null
*		locate the key from the node , such that, the value of the key is <= lowKey
*	
*	if the new node found is index node, then rerun with actual Keys of index node
*	
*	if new node found is leaf node
*
*	Now, inside the leaf node identify the corresponding key
*	Once Mathcing Key is found, 
*		deserialize the leaf node
*		set the rid and key pointers correctly
*
*/	
	private void init() {

		//initialize binary tree reader to the index file path
		binaryTreeReader = new BinaryTreeReader(this.order, this.indexFilePath);
		//read the root page first
		IndexNode indexNode = (IndexNode)binaryTreeReader.readPageWithPageNumber(this.addressOfRootPage);
		//if low key is not set, reset to leftmost leaf
		if (lowKey != Long.MIN_VALUE){
			boolean childAddressGreaterThanLeaves = true;

			while (childAddressGreaterThanLeaves){

				ArrayList<Integer> actualKeys = (ArrayList<Integer>) indexNode.getActualKeys();

				int runningKeyIndex = 0;

				while (runningKeyIndex < actualKeys.size()){

					int currentValue = actualKeys.get(runningKeyIndex);

					if (currentValue > lowKey){

						break;

					}else {
						runningKeyIndex++;
					}
				}

				int chilePageNumber = indexNode.getAddressOfChildren().get(runningKeyIndex);

				if (chilePageNumber > this.numberOfLeaves){

					indexNode=(IndexNode)binaryTreeReader.readPageWithPageNumber(chilePageNumber);					

				}else{
					childAddressGreaterThanLeaves =false;
					this.initAddrOfLeaf = chilePageNumber;

					this.leafNodeInContext = (LeafNode)binaryTreeReader.readPageWithPageNumber(chilePageNumber);		
				}
			}
			ArrayList<Integer> keysList = new ArrayList<Integer>(leafNodeInContext.getLeafDataEntry().keySet());

			boolean exactMatchFound=false;
			int runningKeyIndex = 0;

			while (runningKeyIndex < keysList.size()){

				if ( keysList.get(runningKeyIndex)> lowKey  ){

					break;

				} else if (keysList.get(runningKeyIndex) == lowKey){
					exactMatchFound = true;
					break;


				} else {

					runningKeyIndex++;
				}
			}
			if (exactMatchFound == true){

				if (runningKeyIndex == keysList.size()) {

					Node newNode = binaryTreeReader.readPageWithPageNumber(initAddrOfLeaf);

					initAddrOfLeaf++;


					if (newNode instanceof LeafNode == true){

						resetRidAndKeyInitIndicators(0,0);
						leafNodeInContext = (LeafNode)newNode;


					} else{

						leafNodeInContext=null;
					}
				}else {
					//assign initial key index and reset the rid pointer
					resetRidAndKeyInitIndicators(0,runningKeyIndex);

				}


			}else {
				if(runningKeyIndex == keysList.size() ){
					leafNodeInContext = null;
					return;
				}
				//assign initial key index and reset the rid pointer
				resetRidAndKeyInitIndicators(0,runningKeyIndex);
				
			}


		}else {
			resetRidAndKeyInitIndicators(0,0);

			leafNodeInContext = (LeafNode)binaryTreeReader.readPageWithPageNumber(1);

		}

	}
	
	/**
	 * method to scan and return the tuple matching the condition (lying in between highKey and lowKey)
	 * @param newTupleToReturn
	 * @return
	 */
	private Tuple handleNonFirstNode(Tuple newTupleToReturn) {

		// TODO Auto-generated method stub

		ArrayList<Integer> leafKeys = new ArrayList<Integer>(leafNodeInContext.getLeafDataEntry().keySet());
		int key = leafKeys.get(initialKeyIndicator);
		if (highKey != Long.MAX_VALUE && key <= highKey ){
			
			Rid rid = leafNodeInContext.getLeafDataEntry().get(leafKeys.get(initialKeyIndicator)).get(initialRidIndicator);
			scanner.pointToPage(rid.getPageId(), rid.getTupleId());
			List<Integer> tuple = scanner.getNextTuple();
			
			if (tuple != null){
				newTupleToReturn = new Tuple(fromTable, tuple);
				updateValues(leafKeys,key);
				
			}

			
		}else if (highKey == Long.MAX_VALUE){
			Rid rid = leafNodeInContext.getLeafDataEntry().get(leafKeys.get(initialKeyIndicator)).get(initialRidIndicator);
			scanner.pointToPage(rid.getPageId(), rid.getTupleId());
			List<Integer> tuple = scanner.getNextTuple();
			if (tuple != null){
				newTupleToReturn = new Tuple(fromTable, tuple);
				updateValues(leafKeys,key);
				

			}

		}

		return newTupleToReturn;
	}


	/**
	 * method to reset the Rid and Key indicators
	 * @param initRidIndicator
	 * @param initKeyIndicator
	 */

	private void resetRidAndKeyInitIndicators(int initRidIndicator, int initKeyIndicator){
		this.initialRidIndicator=initRidIndicator;
		this.initialKeyIndicator=initKeyIndicator;
	}

	/**
	 * Method to reset all the  pointers
	 */
		@Override
	public void reset() {
		//TODO Auto-generated method stub
		this.isInitialTupleBeingFetched = true;

		this.leafNodeInContext = this.firstLeafNode;
		this.initialRidIndicator = 0;
		this.initAddrOfLeaf = this.firstLeafNodeAddress;
		this.initialKeyIndicator = this.firstKeyPointer;

	}

	
	/**
	 * Method to close all open channels
	 * 
	 */
	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.scanner.close();
		this.scanner.reset();
	}

	public TupleReaderBinaryFormat getScanner() {
		return scanner;
	}

	public void setScanner(TupleReaderBinaryFormat scanner) {
		this.scanner = scanner;
	}
	
	public Operator getChild() {
		// TODO Auto-generated method stub
		return null;
	}


	
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}


	
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}


	
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	public Table getFromTable() {
		return fromTable;
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	@Override
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "IndexScan["+fromTable.getWholeTableName()+","+getColumnName()+","+lowKey+","+highKey+"]";
	}
	
	public String getColumnName() {
		return columnName;
	}

}

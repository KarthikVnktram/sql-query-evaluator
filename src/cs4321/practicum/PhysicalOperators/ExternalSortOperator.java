package cs4321.practicum.PhysicalOperators;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.Tuple;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import cs4321.practicum.io.TupleReaderHumanReadable;
import cs4321.practicum.io.TupleWriterBinaryFormat;
import cs4321.practicum.utilities.ConversionUtility;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.OrderByElement;

/**
 * Class for External Sort Operation
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class ExternalSortOperator  extends Operator{

	ArrayList<Tuple> allTuples=new ArrayList<Tuple>();
	Operator child=null; 
	List<Column> columnNames=new ArrayList<Column>();
	/**
	 * Used in getNextTuple() to ensure proper passage of tuples to the parent operator.
	 */
	int sortCurrentTupleIndex;

	int bufferPagesAvailable = 0;
	int endOfBufferPointer=0;
	int pass0FileNumber  = 0;
	private ArrayList<Tuple> allTuplesPerBlock;
	private int maxTuplesInAblk;
	private int maximumTuplesInABlockCurrent;
	boolean isVisited = false;
	
	List<OrderByElement> orderByElements=new ArrayList<OrderByElement>();
	List<Table> tables = null;

	/**
	 * A number of methods mandatory for successful operation of SortOperator are called from the constructor to avoid calling it from the main method.
	 * @param Operator
	 * @param List<OrderByElement>
	 */
	List<Integer> indices=new ArrayList<Integer>();
	private Table table;
	TupleReaderHumanReadable tupleReaderExternalSort  = null;
	
	
	/**
	 * Constructor for External Sort
	 * @param child
	 * @param table
	 * @param leftAttributes
	 * @param bufferPagesAvailable
	 * @param tables
	 */
	public ExternalSortOperator(Operator child, Table table, List<Column> leftAttributes,int bufferPagesAvailable,List<Table> tables){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.columnNames=leftAttributes;
		this.bufferPagesAvailable = bufferPagesAvailable;
		endOfBufferPointer = 0;
		this.table = table;
		this.tables = tables;
		pass0FileNumber  = 0;
		loadBuffer();
		if(maximumTuplesInABlockCurrent == 0){
			return;
		}
		//		this.setAllTuplesForSort();
		if(this.child instanceof ProjectOperator == false){
			this.setIndicesForColumnNames(leftAttributes, table,tables );	
		}
		String fileToReadFrom = sortFile();
		tupleReaderExternalSort = new TupleReaderHumanReadable(fileToReadFrom);
	}
	
	public ExternalSortOperator( Table table, List<Column> leftAttributes,int bufferPagesAvailable,List<Table> tables,Operator child){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.columnNames=leftAttributes;
		this.bufferPagesAvailable = bufferPagesAvailable;
		endOfBufferPointer = 0;
		this.table = table;
		this.tables = tables;
		pass0FileNumber  = 0;
		loadBuffer();
		if(maximumTuplesInABlockCurrent == 0){
			return;
		}
		//		this.setAllTuplesForSort();
		if(this.child instanceof ProjectOperator == false){
			this.setIndicesForColumnNames(leftAttributes,tables );	
		}
		String fileToReadFrom = sortFile();
		tupleReaderExternalSort = new TupleReaderHumanReadable(fileToReadFrom);
	}
	/**
	 * Constructor for External Sort
	 * @param child
	 * @param columnNames
	 * @param bufferPagesAvailable
	 * @param orderByElements
	 */
	public ExternalSortOperator(Operator child , List<Column> columnNames,int bufferPagesAvailable,List<OrderByElement> orderByElements){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.columnNames=columnNames;
		this.bufferPagesAvailable = bufferPagesAvailable;
		endOfBufferPointer = 0;
		this.orderByElements = orderByElements;
		loadBuffer();

		if(maximumTuplesInABlockCurrent == 0){
			return;
		}
		
		//		this.setAllTuplesForSort();
		if(this.child instanceof ProjectOperator){
			this.setIndices(columnNames);	
		}
		String fileToReadFrom = sortFile();
		tupleReaderExternalSort = new TupleReaderHumanReadable(fileToReadFrom);

	}
	
	/**
	 * Constructor for External Sort
	 * @param child
	 * @param leftAttributes
	 * @param bufferPagesAvailable
	 */
	public ExternalSortOperator(Operator child, List<Column> leftAttributes,int bufferPagesAvailable){
		this.child=child;
		this.sortCurrentTupleIndex=0;
		this.columnNames=leftAttributes;
		this.bufferPagesAvailable = bufferPagesAvailable;
		endOfBufferPointer = 0;
		pass0FileNumber  = 0;
		loadBuffer();
		if(maximumTuplesInABlockCurrent == 0){
			return;
		}
		//		this.setAllTuplesForSort();
		
		setIndicesForAllColumnNames(columnNames);
		
		String fileToReadFrom = sortFile();
		tupleReaderExternalSort = new TupleReaderHumanReadable(fileToReadFrom);
	}

	/**
	 * Constructor for External Sort
	 * @param columnNames
	 */
	public void setIndicesForAllColumnNames(List<Column> columnNames){
		
		for(int i = 0 ; i < columnNames.size(); i++){
			indices.add(i);	
		}

	}
	
	/**
	 * set Index for column names. used while sorting
	 * @param columnsNames
	 * @param table
	 * @param tables
	 */
	public void setIndicesForColumnNames(List<Column> columnsNames, Table table, List<Table> tables){
		indices.clear();
//		
			for(Column column : columnsNames){
				
				String columname = column.getColumnName();
				if(columname.contains(".")){
					columname = columname.split("\\.")[1];
				}
				boolean isFirst = false;
	
				if(tables == null) {
					isFirst = true;
				} else {
					for(int i=0; i< tables.size(); i++){
						String tableToCompareLeft = "";
						
						if(table.getAlias() != null){
							tableToCompareLeft = table.getAlias();
						} else {
							tableToCompareLeft = table.getName();
						}
					
						String tableToCompareRight = "";
						
						if(tables.get(i).getAlias() != null){
							tableToCompareRight = tables.get(i).getAlias();
						} else {
							tableToCompareRight = tables.get(i).getName();
						}
						
						if(i ==0 && tableToCompareLeft.equals(tableToCompareRight)){
							isFirst = true;
							break;
						}
					}
				}
				int index = DatabaseGlobalCatalog.getInstance().getIndexFromMultipleTables(table, columname, tables, isFirst);
	//			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columname);
				if(index != -1) {
					indices.add(index);
				}
			}
		
	}

	
	public String getTableToCompare(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	public void setIndicesForColumnNames(List<Column> columnsNames, List<Table> tables){
		indices.clear();
		for(Table tableToComapre : tables){
			for(Column column : columnsNames){
				
				String columname = column.getColumnName();
				if(getTableToCompare(column.getTable()).equals(getTableToCompare(tableToComapre))  == false){
					continue;
				}
				if(columname.contains(".")){
					columname = columname.split("\\.")[1];
				}
				boolean isFirst = false;
	
				if(tables == null) {
					isFirst = true;
				} else {
					for(int i=0; i< tables.size(); i++){
						String tableToCompareLeft = "";
						
						if(tableToComapre.getAlias() != null){
							tableToCompareLeft = tableToComapre.getAlias();
						} else {
							tableToCompareLeft = tableToComapre.getName();
						}
					
						String tableToCompareRight = "";
						
						if(tables.get(i).getAlias() != null){
							tableToCompareRight = tables.get(i).getAlias();
						} else {
							tableToCompareRight = tables.get(i).getName();
						}
						
						if(i ==0 && tableToCompareLeft.equals(tableToCompareRight)){
							isFirst = true;
							break;
						}
					}
				}
				int index = DatabaseGlobalCatalog.getInstance().getIndexFromMultipleTables(tableToComapre, columname, tables, isFirst);
	//			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(table, columname);
				if(index != -1) {
					indices.add(index);
				}
			}
		}
		
	}
	
	/**
	 * Set indices for column names
	 * @param columnsFiltered
	 */
	public void setIndices(List<Column> columnsFiltered)
	{
		//#tag
		indices.clear();
		for(int i=0;i<orderByElements.size();i++)
		{	
			int index = getIndexMatch(columnsFiltered,orderByElements.get(i));
			if( index != -1){
				this.indices.add(index);
			} else {

			}
		}
		for(int i=0;i<columnsFiltered.size();i++)
		{
			if(isPresent(columnsFiltered.get(i))==false)
			{
				this.indices.add(i);
			}
		}
	}


	/**
	 * returns the index where the match between the orderbyelement and column of Project occurs.
	 * @param List<Column>
	 * @param OrderByElement
	 * @return int
	 */
	private int getIndexMatch(List<Column> columnsFiltered, OrderByElement columnToCompare) {
		// TODO Auto-generated method stub
		for(int i=0 ; i < columnsFiltered.size();i++) {
			String tableToCompare = "";
			if(columnsFiltered.get(i).getTable().getAlias() != null){
				tableToCompare = columnsFiltered.get(i).getTable().getAlias();
			} else {
				tableToCompare = columnsFiltered.get(i).getTable().getName();
			}
			if(columnToCompare.toString().equals(columnsFiltered.get(i).toString())){
				return i;
			} else if(columnToCompare.toString().equals(tableToCompare+"."+columnsFiltered.get(i).getColumnName())){
				return i;
			}
		}
		return -1;
	}

	
	
	/**
	 * is Column present
	 * @param columnFiltered
	 * @return
	 */
	private boolean isPresent(Column columnFiltered)
	{
		for(int i=0;i<columnNames.size();i++)
		{
			if(columnNames.get(i).toString().equals(columnFiltered.toString()))
			{
				return true;
			}
		}
		return false;
	}

	private void loadBuffer(){
		maximumTuplesInABlockCurrent = 0;
		Tuple tuple = this.child.getNextTuple();
		if(tuple != null){
			
			int tupleSize = tuple.tuple.size();


			maxTuplesInAblk = bufferPagesAvailable*4096/tupleSize;

			//		Tuple currentTuplePointer = nextBlockPointer;

			allTuplesPerBlock = new ArrayList<Tuple>();
			while(tuple != null ){
				allTuplesPerBlock.add(tuple);
				maximumTuplesInABlockCurrent +=1;
				if( maximumTuplesInABlockCurrent == maxTuplesInAblk) {
					break;
				}

				tuple=this.child.getNextTuple();

			}
		}
	}


	private String pass0(Table table){
		String TemporaryDirPath = DatabaseGlobalCatalog.getInstance().getTempDirectory();
		String columns = "";

		for(Column column : columnNames){
			columns += column.getColumnName()+"_";

		}
		
		String uniqueDirName = "";
		if(table != null){
			String tableName = "";
			if(table.getAlias() != null){
				tableName = table.getAlias();
			} else{
				tableName = table.getName();
			}
			
			uniqueDirName = "ESort"+"_"+tableName+"_" + columns.substring(0, columns.length()-1);
		} else {
			uniqueDirName = "ESort"+"_"+columns.substring(0, columns.length()-1);
		}
		

		String dirPath = TemporaryDirPath+File.separator+uniqueDirName;

		while(maximumTuplesInABlockCurrent > 0){
			Collections.sort(allTuplesPerBlock, new OrderByComparator(indices));


			File file = new File(dirPath);
			if(file.exists() == false){
				file.mkdir();
			}

			String fileName = "pass0_"+pass0FileNumber;
			TupleWriterBinaryFormat tupleWriterBinaryFormat = new TupleWriterBinaryFormat(dirPath+File.separator+fileName);
			for(Tuple tuple : allTuplesPerBlock){
				tupleWriterBinaryFormat.write(tuple.tuple);
			}
			tupleWriterBinaryFormat.flush();
			tupleWriterBinaryFormat.close();
			ConversionUtility.convertBinaryFileToHumanReadableFile(dirPath+File.separator+fileName, dirPath+File.separator+fileName+"_human");
			loadBuffer();
			pass0FileNumber++;
		}
		return dirPath;
	}





	@Override
	public Tuple getNextTuple() {
		// TODO Auto-generated method stub
		if(tupleReaderExternalSort == null) { return null; }
		List<Integer> tuple = tupleReaderExternalSort.getNextTuple();
		Tuple tupleOfTuple = null;
		if(tuple != null){
			if(tables != null){
				tupleOfTuple = new Tuple(tables, tuple);
	            
			} else {
				tupleOfTuple = new Tuple(table, tuple);
			}
			return tupleOfTuple;
		}
//		tupleReaderExternalSort.reset();
//		tupleReaderExternalSort.close();
		return null;
		
	}

	/**
	 * 
	 * Reset the reader to the starting point
	 * 
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		if(tupleReaderExternalSort != null){
			tupleReaderExternalSort.reset();
		}
		
		this.child.reset();

	}


	/**
	 * Sorts and returns the path of the file sorted using external sort
	 * 
	 * @return
	 */
	public String sortFile() {
		// TODO Auto-generated method stub
		
		if(allTuplesPerBlock != null && allTuplesPerBlock.size() >0 ){
			setRemainingIndices(allTuplesPerBlock.get(0).tuple.size());
		}
		
		String pass0DirPath = pass0(this.table);
		int numFiles = pass0FileNumber;
		int currentPass = 0 ;
		
		if(pass0FileNumber == 1){
			return pass0DirPath+File.separator+"pass0_0_human";
		}
		
		while(numFiles>1){
			numFiles = mergePass(pass0DirPath, numFiles, currentPass);
			currentPass++;
		}
		
		System.out.println(pass0DirPath+File.separator+"pass"+currentPass+"_0");
		//subsequent passes
		return pass0DirPath+File.separator+"pass"+currentPass+"_0_human";
	}

	/**
	 * Merge pass of External Sort
	 * 
	 * @param pass0DirPath
	 * @param previousPassMaxNumber
	 * @param currentPass
	 * @return
	 */
	private int mergePass(String pass0DirPath,int previousPassMaxNumber,int currentPass) {
		// TODO Auto-generated method stub

		//go through multiple passes until a single file is created
		// create fileReaderpointing to B-1 buffers
		// fill in each buffer with 1 page data
		// create file writer corresponding to one buffer
		// match the tuple to the corresponding the tuple in the B-1 buffer or track which buffer it belongs to
		// increment the buffer to which it belongs to 
		// compare the rest
		// when a buffer is empty, refill it 
		// go on till all are empty

		int prevPassRunningIndex = 0;
		List<List<Integer>> tuplesToMerge = new ArrayList<List<Integer>>();
		List<TupleReaderBinaryFormat> tupleReaderBinaryFormats= new ArrayList<TupleReaderBinaryFormat>();
		List<Integer> tupleIndices = new ArrayList<Integer>();
		int passNumber =0;
		int currentPassFileIndex = 0;
		
//		if(pass0FileNumber == 1){
//			return 1;
//		}
		
		int previousPassMaxFileNumber = previousPassMaxNumber;
		
		
		while(prevPassRunningIndex < previousPassMaxFileNumber){
			
			for(int j=0; j<= bufferPagesAvailable -1 ;j++ ){
				if(prevPassRunningIndex == previousPassMaxFileNumber){
					break;
				}
				tupleReaderBinaryFormats.add(j, new TupleReaderBinaryFormat(pass0DirPath+File.separator+"pass"+currentPass+"_"+(prevPassRunningIndex)));
				tuplesToMerge.add(j, tupleReaderBinaryFormats.get(j).getNextTuple());
				tupleIndices.add(j,j);
				prevPassRunningIndex++;
			}	



			TupleWriterBinaryFormat tupleWriterBinaryFormat = new TupleWriterBinaryFormat(pass0DirPath+File.separator+"pass"+(currentPass+1)+"_"+passNumber++);
			while(tuplesToMerge.size() > 0 ) {
	
				TupleIndexComparator tupleIndexComparator = new TupleIndexComparator(indices,tuplesToMerge);
				Collections.sort(tupleIndices,tupleIndexComparator );
	
				int minIndex = tupleIndices.get(0);
				tupleWriterBinaryFormat.write(tuplesToMerge.get(minIndex));
				List<Integer> nextTuple = tupleReaderBinaryFormats.get(minIndex).getNextTuple();
				tuplesToMerge.set(minIndex, nextTuple);
				
				if(nextTuple == null) {
					tupleIndices.remove(minIndex);
					tuplesToMerge.removeAll(Collections.singleton(null));
					tupleReaderBinaryFormats.get(minIndex).close();
					tupleReaderBinaryFormats.remove(minIndex);
				}
				
				tupleIndices = resetTupleIndices(tuplesToMerge.size());
				
			}

			for(int filesOpen = 0 ; filesOpen < tupleReaderBinaryFormats.size() ; filesOpen ++){
	
				tupleReaderBinaryFormats.get(filesOpen).close();
	
			}	

			//close file writer too
			currentPassFileIndex++;
			tupleWriterBinaryFormat.flush();
			tupleWriterBinaryFormat.close();
			ConversionUtility.convertBinaryFileToHumanReadableFile(pass0DirPath+File.separator+"pass"+(currentPass+1)+"_"+(passNumber-1), pass0DirPath+File.separator+"pass"+(currentPass+1)+"_"+(passNumber-1) +"_human");
		
		}
		return currentPassFileIndex;
	}

	/**
	 * Reset all tuple indices
	 * 
	 * @param maxSize
	 * @return
	 */
	private List<Integer> resetTupleIndices(int maxSize) {
		// TODO Auto-generated method stub
		List<Integer> indicesList = new ArrayList<Integer>();
		for(int tupleIndex = 0; tupleIndex < maxSize ; tupleIndex++ ){
			
			indicesList.add(tupleIndex);
			
		}
			return indicesList;
	}

	/**
	 * This method is called in SMJOperator. Used to reset the right tuple to the index previously stored.
	 */
	@Override
	public void reset(int i) {
		// TODO Auto-generated method stub
		tupleReaderExternalSort.reset(i-1);
	}

	public void setRemainingIndices(int index){
		
		for(int i = 0; i< index ; i ++){
			if(this.indices != null && this.indices.contains(i) == false){
				this.indices.add(i);
			}
		}
		
	}

	/**
	 * Close all unclosed streams
	 *	 
	 */
	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.child.closeAllUnclosed();
	}


	public Operator getChild() {
		// TODO Auto-generated method stub
		return child;
	}


	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}


	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}


	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited = val;
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		if(orderByElements.size()>0)
			return "ExternalSort"+orderByElements.toString();
		else
			return "ExternalSort"+columnNames.toString();	
	}

}



package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;

import cs4321.practicum.ExpressionEvaluator;
import cs4321.practicum.Tuple;
import net.sf.jsqlparser.expression.Expression;

/**
 * To Perform Select Operation of Database Systems.
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class SelectOperator extends Operator {

	Operator child =null;
	
	Expression whereClause = null;
	boolean isVisited=false;
	
	public SelectOperator(Operator childOperator,Expression whereClause )
	{
		this.child = childOperator ;
		this.whereClause = whereClause;
	}

	/**
	 * To send the next tuple to the parent when requested for.
	 * @param Tuple
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple tuple = child.getNextTuple();
		while(tuple != null) {
			// Checking whether the where clause holds true or false.
			if(isTupleMeetingCondition(tuple,whereClause) == true){
				//dump(tuple);
				return tuple;
			}
			tuple = child.getNextTuple();
		}
		return null; // no tuple meets condition
	}

	private boolean isTupleMeetingCondition(Tuple tuple,Expression whereClause) {
		/**
		 * Evaluation of Where Clause of the query using Expression Evaluator Visitor.
		 */
		ExpressionEvaluator expressionEvaluator=new ExpressionEvaluator(tuple);
		whereClause.accept(expressionEvaluator);
		boolean conditionTruthValue=expressionEvaluator.getResult();
		return conditionTruthValue;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub11
		 this.child.reset();
	}
	/**
	 * To check whether the child has more tuples or not to ensure proper working of the pipelining
	 */
	
	public Operator getChild() {
		return child;
	}

	@Override
	public void closeAllUnclosed() {
		// TODO Auto-generated method stub
		this.child.closeAllUnclosed();
	}

	
	public boolean getVisited() {
		// TODO Auto-generated method stub
		return isVisited;
	}

	
	public void setVisited(boolean val) {
		// TODO Auto-generated method stub
		isVisited =  val;
	}

	
	public String getTreePlanCondition() {
		// TODO Auto-generated method stub
		return "Select["+whereClause+"]";
	}

	/**
	 * Method to get the Tree Plan Condition
	 */
	public ArrayList<Operator> getChildrenOfJoin() {
		// TODO Auto-generated method stub
		return null;
	}
}

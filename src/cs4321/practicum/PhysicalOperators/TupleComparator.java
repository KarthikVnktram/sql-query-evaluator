package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
/**
 * Custom Comparator to sort the tuples in ascending order. It is called within SortOperator
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class TupleComparator implements Comparator<List<Integer>>{
	
	List<Integer> indices=new ArrayList<Integer>();
	
	public TupleComparator(List<Integer> indices ) {
		// TODO Auto-generated constructor stub
		this.indices = indices;
	}
	
	@Override
	public int compare(List<Integer> tuple1, List<Integer> tuple2) {
		
		for(int i=0;i<indices.size();i++)
		{
			if(tuple1.get(indices.get(i))<tuple2.get(indices.get(i)))
			{
				return -1;
			}
			else if(tuple1.get(indices.get(i))>tuple2.get(indices.get(i)))
			{
				return 1;
			}
		}
		return 0;
	}
}
package cs4321.practicum.PhysicalOperators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
/**
 * Custom Comparator to sort the tuple index in ascending order. It is called within SortOperator. 
 * We can get the index of the tuples in the list, which would be coming at the top after comparision
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class TupleIndexComparator implements Comparator<Integer>{
	
	List<Integer> indices=new ArrayList<Integer>();
	List<List<Integer>> tupleArray = new ArrayList<List<Integer>>();
	
	public TupleIndexComparator(List<Integer> indices,List<List<Integer>> tupleArray ) {
		// TODO Auto-generated constructor stub
		this.indices = indices;
		this.tupleArray.addAll(tupleArray);
	}
	
	@Override
	public int compare(Integer tuple1Index, Integer tuple2Index) {
		
		for(int i=0;i<indices.size();i++)
		{
			if(tupleArray.get(tuple1Index) == null || tupleArray.get(tuple2Index) == null){
				return 2000;
			}
			if(tupleArray.get(tuple1Index).get(indices.get(i))<tupleArray.get(tuple2Index).get(indices.get(i)))
			{
				return -1;
			}
			else if(tupleArray.get(tuple1Index).get(indices.get(i))>tupleArray.get(tuple2Index).get(indices.get(i)))
			{
				return 1;
			}
		}
		return 0;
	}
}
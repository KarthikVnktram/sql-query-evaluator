package cs4321.practicum;

public class IndexInfo {

	String columnName;
	boolean isClustered;
	int order;
	
	public IndexInfo(String columnName, boolean isClustered, int order){
		
		this.columnName = columnName;
		this.isClustered = isClustered;
		this.order = order;
		
	}
	
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public boolean isClustered() {
		return isClustered;
	}

	public void setClustered(boolean isClustered) {
		this.isClustered = isClustered;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	
}

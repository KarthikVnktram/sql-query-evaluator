package cs4321.practicum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 *  This class outputs 
 *           tables names to attribues map. This would be used, to perform sort before pass the table as input to SMJOperator
 *            
 *           
 *           
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class SMJWhereClauseVisitor extends AbstractExpressionEvaluator {

	//Map of Tables Used in Join to Join Condition
	Map<Table,List<Column>> tableAttributeMap = new LinkedHashMap<Table,List<Column>>();
	Map<String,List<Integer>> values = new LinkedHashMap<String,List<Integer>>();
	
	Tuple leftTuple = null ;
	Tuple rightTuple = null;
	
	public SMJWhereClauseVisitor() {
		// TODO Auto-generated constructor stub
	
	}
	/**
	 * visits AndExpression and calls the left and right child
	 */
	@Override
	public void visit(AndExpression andExpression) {
		// TODO Auto-generated method stub
		andExpression.getLeftExpression().accept(this);
		andExpression.getRightExpression().accept(this);
		
	}
	
	public void setLeftTuple(Tuple tuple){
		this.leftTuple = tuple;
	}
	
	public void setRightTuple(Tuple tuple){
		this.rightTuple = tuple;
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		buildExpressions(equalsTo);
		buildExpressionsForCompare(equalsTo);
	}
	

	List<Column> rightColumns = new ArrayList<Column>();
	
	List<Column> leftColumns = new ArrayList<Column>();
	
	
	public List<Column> getRightColumns(){
		return rightColumns;
	}
	
	public List<Column> getLeftColumns(){
		return leftColumns;
	}
	
	private void buildExpressionsForCompare(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		if(leftTuple == null || rightTuple == null){
			return;
		}
		if(equalsTo.getLeftExpression() instanceof Column && equalsTo.getRightExpression() instanceof Column){
			
			if(this.rightTuple.table != null){
				if( getNameAlias(((Column)equalsTo.getLeftExpression()).getTable()).equals(getNameAlias(this.rightTuple.table)) ){
					rightColumns.add((Column)equalsTo.getLeftExpression());
					leftColumns.add((Column)equalsTo.getRightExpression());
				} else if (  getNameAlias(((Column)equalsTo.getRightExpression()).getTable()).equals(getNameAlias(this.rightTuple.table)) ){
					rightColumns.add((Column)equalsTo.getRightExpression());
					leftColumns.add((Column)equalsTo.getLeftExpression());
				} 
			}
			
		}
		
	}
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		buildExpressions(notEqualsTo);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThan greaterThan) {
		buildExpressions(greaterThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		buildExpressions(greaterThanEquals);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThan minorThan) {
		buildExpressions(minorThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		buildExpressions(minorThanEquals);
	}	
	
	public Map<Table,List<Column>> getTableAttributeMap()
	{
		
		/*for(Table table: tableAttributeMap.keySet()){
			HashSet<Column> column = new HashSet<Column>(tableAttributeMap.get(table));
			List<Column> columnList = new ArrayList<Column>(column);
			tableAttributeMap.put(table, columnList);
		}*/
		return this.tableAttributeMap;
	}

	/**
	 * Builds the select expressions map
	 * @return
	 */
	private void buildExpressions(BinaryExpression expression) {
		// TODO Auto-generated method stub
		if(expression.getLeftExpression() instanceof Column && expression.getRightExpression() instanceof Column) {
			List<Column> first=new ArrayList<Column>();
			List<Column> second=new ArrayList<Column>();
			String tableLeft=getNameAlias(((Column)expression.getLeftExpression()).getTable());
			String tableRight=getNameAlias(((Column)expression.getRightExpression()).getTable());
			if(tableAttributeMap.size()<=0){
				if(checkIfListContains(first,(Column)expression.getLeftExpression() ) == false){
					first.add(((Column)expression.getLeftExpression()));
				}
				
				tableAttributeMap.put(((Column)expression.getLeftExpression()).getTable(), first);
				
				if(checkIfListContains(second,(Column)expression.getRightExpression() ) == false){
				
					second.add(((Column)expression.getRightExpression()));
				}
				
				tableAttributeMap.put(((Column)expression.getRightExpression()).getTable(), second);
			} else {
				boolean isFound = false;
				for(Table key:tableAttributeMap.keySet()){
					String keyLoop=getNameAlias(key);
					if(keyLoop.equals(tableLeft)){
						first=tableAttributeMap.get(key);
						if(checkIfListContains(first,(Column)expression.getLeftExpression() ) == false){
							first.add(((Column)expression.getLeftExpression()));
						}
					
//						tableAttributeMap.remove(key);
						tableAttributeMap.put(key, first);
						isFound = true;
					}
				}
				
				if(isFound == false){
					if(checkIfListContains(first,(Column)expression.getLeftExpression() ) == false){
							first.add(((Column)expression.getLeftExpression()));
					}
					tableAttributeMap.put(((Column)expression.getLeftExpression()).getTable(), first);
				}
				
				
				isFound = false;
				for(Table key:tableAttributeMap.keySet()){
					String keyLoop=getNameAlias(key);
					if(keyLoop.equals(tableRight)){
						second=tableAttributeMap.get(key);
						if(checkIfListContains(second,(Column)expression.getRightExpression() ) == false){
									second.add(((Column)expression.getRightExpression()));
						}
//						tableAttributeMap.remove(key);
						tableAttributeMap.put(key, second);
						isFound = true;
					}
				}
				if(isFound == false){
					if(checkIfListContains(second,(Column)expression.getRightExpression() ) == false){
								second.add(((Column)expression.getRightExpression()));
					}
					tableAttributeMap.put(((Column)expression.getRightExpression()).getTable(), second);
				}
				
			}
			/*if(tableAttributeMap.containsKey(((Column)expression.getLeftExpression()).getTable())){
				first=tableAttributeMap.get(((Column)expression.getLeftExpression()).getTable());
				first.add(((Column)expression.getLeftExpression()));
				tableAttributeMap.put(((Column)expression.getLeftExpression()).getTable(), first);
			}
			else{
				first.add(((Column)expression.getLeftExpression()));
				tableAttributeMap.put(((Column)expression.getLeftExpression()).getTable(), first);
			}
			if(tableAttributeMap.containsKey(((Column)expression.getRightExpression()).getTable())){
				second=tableAttributeMap.get(((Column)expression.getRightExpression()).getTable());
				second.add(((Column)expression.getRightExpression()));
				tableAttributeMap.put(((Column)expression.getRightExpression()).getTable(), second);
			}
			else{
				second.add(((Column)expression.getRightExpression()));
				tableAttributeMap.put(((Column)expression.getRightExpression()).getTable(), second);
			}*/
		}
	}
		
	
	private boolean checkIfListContains(List<Column> columns, Column column){
		
		for(Column columnToCompare : columns){
			if(columnToCompare.toString().equals(column.toString())){
				return true;
			}
		}
		return false;
		
		
	}
	private String getNameAlias(Table expression){

		String table=new String();
		if(expression.getAlias()!=null){
			table=expression.getAlias();
		}else{
			table=expression.getName();
		}
		return table;
	}
}

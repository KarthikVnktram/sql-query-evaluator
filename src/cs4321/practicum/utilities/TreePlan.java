package cs4321.practicum.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.LogicalOperators.JoinOperatorLogical;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.LogicalOperators.ProjectOperatorLogical;
import cs4321.practicum.LogicalOperators.ScanOperatorLogical;
import cs4321.practicum.LogicalOperators.SortOperatorLogical;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import cs4321.practicum.LogicalPlanUtilities.UnionFindElement;
import cs4321.practicum.PhysicalOperators.BNLJOperator;
import cs4321.practicum.PhysicalOperators.JoinOperator;
import cs4321.practicum.PhysicalOperators.Operator;
import cs4321.practicum.PhysicalOperators.ProjectOperator;
import cs4321.practicum.PhysicalOperators.SMJOperator;
import net.sf.jsqlparser.statement.select.Union;

/**
 * Class to check Stats Builder
 *
 *  @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class TreePlan {
		
	/**
	 * Method to get the Unvisited Child of a Logical Operator
	 * @param opr
	 * @return
	 */
	public static LogicalOperator getChildUnvisitedLogical(LogicalOperator opr){
		if(opr instanceof JoinOperatorLogical){
			ArrayList<LogicalOperator> list = opr.getChildrenOfJoin();
			for(LogicalOperator childNode:list){
				if(childNode.getVisited()==false)
					return childNode;
			}
		}
		else{
			LogicalOperator childNode = opr.getChild();
			if(childNode!=null && childNode.getVisited()==false)
				return childNode;
		}
		
		return null;
		
	}
	
	/**
	 * Method to create a Logical Tree Plan
	 * @param node
	 * @param filenumber
	 */
	public static void LogicalTreePlan(LogicalOperator node,int filenumber){
		BufferedWriter bw = null;
		try {
			bw=new BufferedWriter(new FileWriter(DatabaseGlobalCatalog.getInstance().getOutputFilePath()+File.separator+"query"+filenumber+"_logicalplan"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stack<LogicalOperator> stack =  new Stack<LogicalOperator>();
		Stack<Integer> dashstack = new Stack<Integer>();
		if((node instanceof ProjectOperatorLogical) && (node.getTreePlanCondition()==null))
			stack.push(node.getChild());
		else
			stack.push(node);
		dashstack.push(0);
		
		while(!stack.isEmpty()){
			LogicalOperator parentNode = stack.peek();
			String dashes="";
			if(parentNode.getVisited()==false){
				for(int i=0;i<dashstack.peek();i++)
					dashes+="-";
				    if(parentNode instanceof JoinOperatorLogical){
//				    	System.out.println(dashes+parentNode.getTreePlanCondition());
				    	try {
							bw.write(dashes+parentNode.getTreePlanCondition());
							bw.newLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	UnionFind unionfind= ((JoinOperatorLogical)parentNode).getUnionFindForJoin();
				    	if(unionfind!=null){
					    	for(UnionFindElement unionfindelement : unionfind.getUnionFindElements()){
					    		String equal="null",low="null",high="null";
					    		if(unionfindelement.getEqualityConstraint()!=null)
					    			equal=unionfindelement.getEqualityConstraint().toString();
					    		if(unionfindelement.getLowerBound()!=null)
					    			low=unionfindelement.getLowerBound().toString();
					    		if(unionfindelement.getUpperBound()!=null)
					    			high=unionfindelement.getUpperBound().toString();
					    		String str ="["+unionfindelement.getAttributes().toString()+", equals "+equal+", min "+low+", max "+high+"]";
//					    		System.out.println(str);
					    		try {
									bw.write(str);
									bw.newLine();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
					    	}
				      }
				    }
				    else{
//				    	System.out.println(dashes+parentNode.getTreePlanCondition());
				    	try {
							bw.write(dashes+parentNode.getTreePlanCondition());
							bw.newLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				    	
				    }
			    parentNode.setVisited(true);
			}
			LogicalOperator childNode ;
			childNode= getChildUnvisitedLogical(parentNode);
			if((childNode instanceof ProjectOperatorLogical) && (childNode.getTreePlanCondition()==null)){
				childNode.setVisited(true);
				childNode=getChildUnvisitedLogical(childNode);
			}
			if(childNode!=null){
				stack.push(childNode);
				dashstack.push(dashstack.peek()+1);
			}
			else{
				stack.pop();
				dashstack.pop();
			}
		}
		try {
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get Child Unvisited Child for Physical Operator
	 * @param opr
	 * @return
	 */
	public static Operator getChildUnvisitedPhysical(Operator opr){
		
		if(opr instanceof BNLJOperator || opr instanceof JoinOperator || opr instanceof SMJOperator){
			ArrayList<Operator> list = opr.getChildrenOfJoin();
			for(Operator childNode:list){
				if(childNode.getVisited()==false)
					return childNode;
			}
		}
		else{
			Operator childNode = opr.getChild();
			if(childNode!=null && childNode.getVisited()==false)
				return childNode;
		}
		
		return null;
		
	}
	
	/**
	 * Method to build Physical Tree Plan
	 * @param node
	 * @param filenumber
	 */
	public static void PhysicalTreePlan(Operator node,int filenumber){
		BufferedWriter bw = null;
		try {
			bw=new BufferedWriter(new FileWriter(DatabaseGlobalCatalog.getInstance().getOutputFilePath()+File.separator+"query"+filenumber+"_physicalplan"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Stack<Operator> stack =  new Stack<Operator>();
		Stack<Integer> dashstack = new Stack<Integer>();
		if((node instanceof ProjectOperator) && (node.getTreePlanCondition()==null))
			stack.push(node.getChild());
		else
			stack.push(node);
		dashstack.push(0);
		//node.setVisited(true);
		
		while(!stack.isEmpty()){
			Operator parentNode = stack.peek();
			String dashes="";	
			if(parentNode.getVisited()==false){
				
				for(int i=0;i<dashstack.peek();i++)
					dashes+="-";
//				System.out.println(dashes+parentNode.getTreePlanCondition());
				try {
					bw.write(dashes+parentNode.getTreePlanCondition());
					bw.newLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    parentNode.setVisited(true);
			}
			Operator childNode ;
			childNode = getChildUnvisitedPhysical(parentNode);
			if((childNode instanceof ProjectOperator) && (childNode.getTreePlanCondition()==null)){
				childNode.setVisited(true);
				childNode=getChildUnvisitedPhysical(childNode);
			}
			if(childNode!=null){
				stack.push(childNode);				
				dashstack.push(dashstack.peek()+1);
			}
			else{
				stack.pop();
				dashstack.pop();
			}		
		}
		
		try {
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
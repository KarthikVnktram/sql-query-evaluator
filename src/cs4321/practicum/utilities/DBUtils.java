package cs4321.practicum.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.IndexInfo;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 * contaings util funtions
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class DBUtils {

	/**
	 * cleans up the directory. here temp directory
	 * @param tempDirectory
	 */
	public static void cleanUpTempDir(File tempDirectory){
		if(tempDirectory == null || tempDirectory.exists() ==  false || tempDirectory.isDirectory() == false){ return; }
		for (File file: tempDirectory.listFiles()) {
			if (file.isDirectory()) {
				cleanUpTempDir(file);
			}
			file.delete();
		}
	}


	public static List<Column> getAllColumnsFromTable(Table table){
		List<Column> columns = new ArrayList<Column>();
		String schema_path=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		try{		
			BufferedReader br =new BufferedReader(new FileReader(schema_path));
			String temp=null;
			boolean tableFound = false;
			while((temp=br.readLine())!=null) {
				String splittedLine[]=temp.split(" ");
				if(splittedLine[0].equals(table.getName()))
				{
					for(int i=1;i<splittedLine.length;i++) {

						columns.add(new Column(table, splittedLine[i]));
						tableFound = true;
					}
				}
				if(tableFound == true){
					break;
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return columns;
	}


	public static List<Column> getSelectedColumnsFromTable(Table table,String columnName){
		List<Column> columns = new ArrayList<Column>();
		String schema_path=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		try{		
			BufferedReader br =new BufferedReader(new FileReader(schema_path));
			String temp=null;
			//				boolean tableFound = false;
			while((temp=br.readLine())!=null) {
				String splittedLine[]=temp.split(" ");
				if(splittedLine[0].equals(table.getName()))
				{
					for(int i=1;i<splittedLine.length;i++) {
						if(splittedLine[i].equals(columnName)){
							columns.add(new Column(table, splittedLine[i]));
							//								tableFound = true;
							break;
						}
					}
				}
				//					if(tableFound == true){
				//						break;
				//					}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return columns;
	}
	public static int getNumberOfLeavesInIndexFilePath(String indexfilepath) throws IOException
	{
		FileInputStream fileInputStream = new FileInputStream(indexfilepath);
		FileChannel channel = fileInputStream.getChannel();
		ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
		buffer.clear();
		//read data from file to buffer
		channel.read(buffer);
		int addressOfRootPage = buffer.getInt(0);
		int numberOfLeaves= buffer.getInt(4);
		int order = buffer.getInt(8);
		channel.close();
		fileInputStream.close();
		return numberOfLeaves;
	}

	public static int getTupleCount(String keyToSearchIndex) {
		BufferedReader br=null;
		String relationStats=null;
		try {
			br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
			relationStats=br.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(relationStats!=null){
			String relationName=relationStats.split(" ")[0];
			if(relationName.equals(keyToSearchIndex)){
				break;
			}
			try {
				relationStats=br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String tupleCount=relationStats.split(" ")[1];
		int count=Integer.parseInt(tupleCount);
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}



	public static double getReductionFactor(String keyToSearchIndex, String columnName, long lowkey, long highkey, Long previousTotalTuples) throws IOException {
		double reductionFactor=0.0d;
		BufferedReader br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
		String line=br.readLine();
		String allFields[]=null;
		while(line!=null){
			allFields=line.split(" ");
			if(allFields[0].equals(keyToSearchIndex)){
				break;
			}
			line=br.readLine();
		}
		int i=2;
		String attributeField[]=null;
		while(i<allFields.length){
			attributeField=allFields[i].split(",");
			if(attributeField[0].equals(columnName)){
				break;
			}
			i++;
		}
		long min=Long.parseLong(attributeField[1]);
		long max=Long.parseLong(attributeField[2]);
		long numerator=1;
		if(highkey==Long.MAX_VALUE && lowkey==Long.MIN_VALUE){
			numerator=DBUtils.getTupleCount(keyToSearchIndex);
		}else{
			if (highkey==Long.MAX_VALUE){
				highkey=max;
			}
			else if(lowkey==Long.MIN_VALUE){
				lowkey=min;
			}
			numerator=highkey-lowkey+1;
		}
		long denominator = max-min+1;
		if(previousTotalTuples != null && previousTotalTuples < denominator){
			denominator = previousTotalTuples;
		}
		reductionFactor=(double)numerator/(denominator);
		br.close();
		return reductionFactor;
	}

	public static double getReductionFactorBaseTable(String keyToSearchIndex, String columnName, long lowkey, long highkey, Long previousTotalTuples) throws IOException {
		double reductionFactor=0.0d;
		long numerator = (long) 1.0;
		long totalTuples =DBUtils.getTupleCount(keyToSearchIndex);
		if(highkey==Long.MAX_VALUE && lowkey==Long.MIN_VALUE){
			numerator=DBUtils.getTupleCount(keyToSearchIndex);
		}else{
			if (highkey==Long.MAX_VALUE){
				highkey = DBUtils.getTupleCount(keyToSearchIndex);
			}
			else if(lowkey==Long.MIN_VALUE){
				lowkey=1;
			}
			numerator=highkey-lowkey+1;
		}
		long denominator =totalTuples;
		if(previousTotalTuples != null && previousTotalTuples < denominator){
			denominator = previousTotalTuples;
		}
		reductionFactor=(double)numerator/(denominator);

		return reductionFactor;
	}

	
	
	
	public static double getNumberOfUniqueValuesPerAttribute(String keyToSearchIndex, String columnName) throws IOException {

		BufferedReader br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
		String line=br.readLine();
		String allFields[]=null;
		while(line!=null){
			allFields=line.split(" ");
			if(allFields[0].equals(keyToSearchIndex)){
				break;
			}
			line=br.readLine();
		}
		int i=2;
		String attributeField[]=null;
		while(i<allFields.length){
			attributeField=allFields[i].split(",");
			if(attributeField[0].equals(columnName)){
				break;
			}
			i++;
		}
		long min=Long.parseLong(attributeField[1]);
		long max=Long.parseLong(attributeField[2]);
		br.close();
		return max-min+1;
	}

	public static Expression buildExpressionFromList(List<Expression> selectExpressions){
		Expression expression = null;
		if(selectExpressions == null){
			return null;
		}
		if(selectExpressions.size() > 1){
			Expression previousExpression = buildAndExpression(selectExpressions.get(0), selectExpressions.get(1));
			for(int i =2 ; i < selectExpressions.size() ; i++){
				previousExpression = buildAndExpression(selectExpressions.get(i), previousExpression);
			}
			expression = previousExpression;

		} else if(selectExpressions.size() == 1){

			expression = selectExpressions.get(0);

		}
		return expression;
	}

	public static Expression buildAndExpression(Expression equalsTo, Expression previousExpression) {
		// TODO Auto-generated method stub
		AndExpression andExpression = new AndExpression(equalsTo, previousExpression);
		return andExpression;
	}

	public static Expression createEqualsToExpression(Column columnLeft, Column columnRight){
		EqualsTo expression = new EqualsTo();
		expression.setLeftExpression(columnLeft);
		expression.setRightExpression(columnRight);
		return expression;
	}

}

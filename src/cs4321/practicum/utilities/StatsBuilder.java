package cs4321.practicum.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.crypto.Data;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import cs4321.practicum.io.TupleWriterHumanReadable;


/**
 * Class to build Statistics Builder file
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class StatsBuilder {
	private BufferedReader br=null;
	private BufferedWriter bw=null;
	public StatsBuilder(){
		try {
			br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName()));
			bw=new BufferedWriter(new FileWriter(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to build statistics
	 * @throws IOException
	 */
	public void buildStatistics() throws IOException{
		String schemaReadLine=br.readLine();
		while(schemaReadLine!=null){
			String sourcePath=DatabaseGlobalCatalog.getInstance().getDataPath();
			String tableName;
			String[] tableAttr=schemaReadLine.split(" ");
			tableName=tableAttr[0];
			int tupleCount=0;
			ArrayList<String> attributeList = new ArrayList<String>();
			ArrayList<Integer> minList = new ArrayList<Integer>();
			ArrayList<Integer> maxList = new ArrayList<Integer>();
			sourcePath+=File.separator+tableName;
			for(int i=1;i<tableAttr.length;i++){
				attributeList.add(tableAttr[i]);
				minList.add(Integer.MAX_VALUE);
				maxList.add(Integer.MIN_VALUE);
			}
			TupleReaderBinaryFormat tupleReaderBinaryFormat = new TupleReaderBinaryFormat(sourcePath);
			List<Integer> tuple=tupleReaderBinaryFormat.getNextTuple();
			while (tuple != null && tuple.size() > 0){
				tupleCount++;
				if(tuple.size()!=attributeList.size()){
//					System.out.println(tableName+" : Attribute count doesn't match with Tuple's total number of values");
					continue;
				}
				for(int i=0;i<tuple.size();i++){
					if(minList.get(i)>tuple.get(i)){
						minList.set(i, tuple.get(i));
					}
					if(maxList.get(i)<tuple.get(i)){
						maxList.set(i, tuple.get(i));
					}
				}
				tuple = tupleReaderBinaryFormat.getNextTuple();
			}
			tupleReaderBinaryFormat.reset();
			tupleReaderBinaryFormat.close();
			statsWriter(tableName,tupleCount,attributeList,minList,maxList);
			schemaReadLine=br.readLine();
			if(schemaReadLine!=null){
				bw.newLine();
			}
		}
		br.close();
		bw.close();
	}
	
	/**
	 * Statistics Writer
	 * 
	 * @param relationName
	 * @param relationSize
	 * @param attributeSet
	 * @param minValue
	 * @param maxValue
	 */
	private void statsWriter(String relationName,Integer relationSize,List<String> attributeSet,List<Integer> minValue,List<Integer> maxValue) {
	   try {
		    String eachLine = relationName+" "+relationSize.toString();
		    for(Integer i=0;i<attributeSet.size();i++){
		    	eachLine+=" "+attributeSet.get(i)+","+minValue.get(i)+","+maxValue.get(i);
		    }
		    this.bw.write(eachLine);
	   } catch (IOException e) {
		   e.printStackTrace();
	   }
	}

}

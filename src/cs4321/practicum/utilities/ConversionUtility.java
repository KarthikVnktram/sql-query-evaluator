package cs4321.practicum.utilities;

import java.util.List;

import cs4321.practicum.io.TupleReaderBinaryFormat;
import cs4321.practicum.io.TupleReaderHumanReadable;
import cs4321.practicum.io.TupleWriterBinaryFormat;
import cs4321.practicum.io.TupleWriterHumanReadable;


public class ConversionUtility {
	
	public static void convertBinaryFileToHumanReadableFile(String sourcePath, String destinationPath){
		
		TupleReaderBinaryFormat tupleReaderBinaryFormat = new TupleReaderBinaryFormat(sourcePath);
				
		List<Integer> tuple = tupleReaderBinaryFormat.getNextTuple();
		
		if(tuple != null){
			TupleWriterHumanReadable tupleWriterHumanReadable = new TupleWriterHumanReadable(destinationPath);
			while (tuple != null && tuple.size() > 0){
				tupleWriterHumanReadable.write(tuple);
				
				tuple.clear();
				
				List<Integer> nextTuple = tupleReaderBinaryFormat.getNextTuple();
				if(nextTuple != null){
					tuple.addAll(nextTuple);
				}
			}
			tupleWriterHumanReadable.flush();	
			tupleWriterHumanReadable.close();
		}
		tupleReaderBinaryFormat.reset();
		tupleReaderBinaryFormat.close();
	}
	
	public static void convertHumanReadableFileToBinaryFile(String source, String destination){
		TupleReaderHumanReadable tupleReaderHumanReadable = new TupleReaderHumanReadable(source);
		TupleWriterBinaryFormat tupleWriterBinaryFormat = new TupleWriterBinaryFormat(destination);
		
		List<Integer> tuple = tupleReaderHumanReadable.getNextTuple();
		
		
		while (tuple != null && tuple.size() > 0){
			tupleWriterBinaryFormat.write(tuple);
			tuple.clear();
			List<Integer> tupleValues = tupleReaderHumanReadable.getNextTuple();
			if(tupleValues != null) {
				tuple.addAll(tupleValues); 
			}
		}

		tupleWriterBinaryFormat.flush();
		tupleWriterBinaryFormat.close();
		tupleReaderHumanReadable.close();
	}

}

package cs4321.practicum.utilities;

import java.io.IOException;

/**
 * Class to check Stats Builder
 *
 *  @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class StatsTester {

	public static void main(String[] args) {
		StatsBuilder statsBuilder=new StatsBuilder();
		try {
			statsBuilder.buildStatistics();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

package cs4321.practicum.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import cs4321.practicum.LogicalPlanUtilities.UnionFindElement;
import cs4321.practicum.LogicalPlanUtilities.UnionFindWhereClauseVisitor;
/**
 * Class to test Union Find Where Clause Visitor
 *
 *  @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class UnionFindWhereClauseVisitorTest {

	public static void main(String args[]){
		
		String queryFileName=DatabaseGlobalCatalog.getInstance().getInputFilePath()+File.separator+DatabaseGlobalCatalog.getInstance().getQueryFileName();
		CCJSqlParser parser;
		try {
			parser = new CCJSqlParser(new FileReader(queryFileName));
		
			Statement statement = parser.Statement();
			
			Select select=(Select)statement;
			PlainSelect plainSelect=(PlainSelect)select.getSelectBody();
			
			
			Expression whereExpression=plainSelect.getWhere();
			
			UnionFindWhereClauseVisitor unionFindWhereClauseVisitor = new UnionFindWhereClauseVisitor();
			whereExpression.accept(unionFindWhereClauseVisitor);
			UnionFind unionFind = unionFindWhereClauseVisitor.getUnionFind();
			List<UnionFindElement> unionFindElements = unionFind.getUnionFindElements();
			
			List<Expression> unusableComparisions = unionFindWhereClauseVisitor.getUnUsableComparisions();
			System.out.println("Union Find Elements are");
			for(UnionFindElement unionFindElement: unionFindElements){
				System.out.println("Attributes for Union Find Element "+unionFindElement.getAttributes().toString());
				System.out.println("Lower Bound "+unionFindElement.getLowerBound());
				System.out.println("Higher Bound "+unionFindElement.getUpperBound());
				System.out.println("Equality Constraint "+unionFindElement.getEqualityConstraint());
				System.out.println("Expression with this Union Find Element "+ unionFindElement.getUnionFindExpressions());
				System.out.println();
				System.out.println();
			}
			
			System.out.println("Unused Expressions are ");
			for(Expression expression: unusableComparisions){
				
				System.out.println(expression.toString());
				
			}
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}

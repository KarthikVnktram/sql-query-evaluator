package cs4321.practicum.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cs4321.practicum.PhysicalOperators.TupleComparator;
import cs4321.practicum.io.TupleReaderBinaryFormat;
import cs4321.practicum.io.TupleWriterHumanReadable;

/**
 * Class for running custom tests
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class CusTomTests {

	
	
	public static void  sortResults(){
		
		String inputfile="C:\\Users\\Karthik\\workspace\\DB_PRAC_BUG_FIX\\expected_output\\query";
		String outputfile="C:\\Users\\Karthik\\workspace\\DB_PRAC_BUG_FIX\\expected_output_sorted\\query";
		String inputfile_expected="C:\\Users\\Karthik\\workspace\\DB_PRAC_BUG_FIX\\expected\\query";
		String outputfile_expected="C:\\Users\\Karthik\\workspace\\DB_PRAC_BUG_FIX\\expected_sorted\\query";
		
		for(int i=1;i<=10;i++)
		{
			convertBinaryIntoHumanReadableSorted(inputfile+i,outputfile+i+"_humanreadable");
			convertBinaryIntoHumanReadableSorted(inputfile_expected+i,outputfile_expected+i+"_humanreadable");
		}
		
			}
	
	
	public static void convertBinaryIntoHumanReadableSorted(String fromPath, String targetPath){
		TupleReaderBinaryFormat binaryFormat = new TupleReaderBinaryFormat(fromPath);
		
		int index = 0 ;
		
		List<ArrayList<Integer>> allTuples = new ArrayList<ArrayList<Integer>>();
		List<Integer> tuple = binaryFormat.getNextTuple();
		if(tuple != null){
			TupleWriterHumanReadable writer = new TupleWriterHumanReadable(targetPath);
			while (tuple != null && tuple.size() > 0){
				allTuples.add((ArrayList<Integer>) tuple);
				index= tuple.size();
				tuple = binaryFormat.getNextTuple();
				
			}
			List<Integer> indices = new ArrayList<Integer>();
			
			for(int i=0; i < index ;i++){
				
				indices.add(i);
				
			}
			Collections.sort(allTuples,new TupleComparator(indices));
			for(List<Integer> indTuple: allTuples){
				writer.write(indTuple);
			}
			writer.flush();	
			writer.close();
		}
		binaryFormat.reset();
		binaryFormat.close();
	}
	

}

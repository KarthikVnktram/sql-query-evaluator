package cs4321.practicum.io;

import java.util.List;

import cs4321.TreeNodes.IndexNode;
import cs4321.TreeNodes.LeafNode;
/**
 * Abstract class for writing tuples
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public interface NodeWriter {
	
	public abstract int write(LeafNode leafNode);
	
	public abstract int write(IndexNode indexNode);
	
	public abstract void cleanUpTuples();
	
	public abstract void reset();
	
	public abstract void close();
	
	

}

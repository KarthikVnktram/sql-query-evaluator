package cs4321.practicum.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class TupleReaderBinaryFormat implements TupleReader{

	private FileInputStream fileInputStream;
	private ByteBuffer buffer;
	private FileChannel channel;
	
	private int attributesPerTupleIndex = 0;
	private int numberOfTupleInPageIndex= 4;
	
	private int bytePerAttribute = 4;
	
	private String inputFilePath;
	
	public String getInputFilePath() {
		return inputFilePath;
	}

	private int attributesPerTuple;
	private int numTuplesPerPage;


	private int indexOfNextTuple;

	private int pageId;
	private int tupleId;
	
	int sizeByte = 4;
	int firstRealDataIndex = 8;
	/**
	 * Constructor for Tuple Reader Binary Format
	 * 
	 * @param pathtoInputFile
	 */
	public TupleReaderBinaryFormat(String pathtoInputFile){
		buffer = ByteBuffer.allocateDirect(4096);
		try {
			inputFilePath=pathtoInputFile;
			fileInputStream=new FileInputStream(pathtoInputFile);
			channel = fileInputStream.getChannel();
			buffer.clear();
			channel.read(buffer);
			indexOfNextTuple = 8;
			attributesPerTuple = buffer.getInt(attributesPerTupleIndex);
			numTuplesPerPage = buffer.getInt(numberOfTupleInPageIndex);
			this.pageId = 0;
			this.tupleId=-1;
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Tried reset with Binary file. Buggy ad replaced by tuple reader human 
	 * @return
	 */
	
//	public void reset(int n){
//		buffer.clear();
//		try{
//			channel.close();
//			fistream.close();
//			fistream=new FileInputStream(this.inputFilePath);
//			channel = fis.getChannel();
//			long numberPages = 0; 
//			channel.position(4096*numberPages);
//			channel.read(buffer);
//		}
//		catch (IOException e){
//			e.printStackTrace();
//		}
//	}
	

	/**
	 * If the reader has more tuples to read
	 * 
	 * @return
	 */
	public boolean hasMoreTuples(){
		if(indexOfNextTuple < attributesPerTuple*numTuplesPerPage*bytePerAttribute+8 ) {
			return true;
		} else {
			return false;
		}
		
		
		
	}
	
	
	/**
	 * 
	 * get the next Tuple
	 * 
	 *
	 */
	@Override
	public List<Integer> getNextTuple() {
	
		if (this.hasMoreTuples() == false){
			buffer.clear();
			try {
				int nextInput = channel.read(buffer);
				
				
				if (nextInput == -1){
					return null;
				}
				else{
					
					pageId++;
					tupleId=0;
					this.numTuplesPerPage = buffer.getInt(numberOfTupleInPageIndex);
					this.indexOfNextTuple=8;
					List<Integer> tuple = new ArrayList<Integer>();
				
					for (int i = 0 ; i < bytePerAttribute*attributesPerTuple ; i +=bytePerAttribute){
						tuple.add(i/bytePerAttribute, buffer.getInt(indexOfNextTuple+i));
					}
					indexOfNextTuple+=bytePerAttribute*attributesPerTuple;
					return tuple;
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
		}
		else {
			List<Integer> tuple = new ArrayList<Integer>();
			for (int i =0; i < (bytePerAttribute*attributesPerTuple) ; i+=bytePerAttribute){
			
				//add data to tuples
				tuple.add((i+1)/bytePerAttribute, buffer.getInt(indexOfNextTuple +i) );
			}
			indexOfNextTuple+=bytePerAttribute*attributesPerTuple;
			
			return tuple;
		}
	}

	
	/**
	 * Resets resources
	 */
	@Override
	public void reset() {
		buffer.clear();
		try {
			channel.close();
			fileInputStream.close();
			
			fileInputStream=new FileInputStream(this.inputFilePath);
			
			channel = fileInputStream.getChannel();
			
			channel.read(buffer);
			
			numTuplesPerPage = buffer.getInt(numberOfTupleInPageIndex);
			
			indexOfNextTuple = 8;
			pageId=0;
			tupleId=-1;
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void close() {
		try {
			channel.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * function to point to a particular page and read data from it
	 * 
	 * @param page
	 * @param tupleOffset
	 */
	public void pointToPage(int page, int tupleOffset){
		
		if (this.pageId != page){
			buffer.clear();
			try {
				channel.position(4096*page);
				channel.read(buffer);
				this.numTuplesPerPage=buffer.getInt(4);
				indexOfNextTuple=firstRealDataIndex +tupleOffset*sizeByte*this.attributesPerTuple;
				this.pageId=page;
				this.tupleId = tupleOffset;
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		}else {
			indexOfNextTuple= firstRealDataIndex +tupleOffset* sizeByte *this.attributesPerTuple;
			this.tupleId = tupleOffset;
		}
	}
	
	
	public int getPageid() {
		return pageId;
	}

	public int getTupleid() {
		return tupleId;
	}

	public int getNumTuplesPerPage() {
		return numTuplesPerPage;
	}

	public void setNumTuplesPerPage(int numTuplesPerPage) {
		this.numTuplesPerPage = numTuplesPerPage;
	}


}

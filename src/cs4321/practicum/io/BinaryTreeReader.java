package cs4321.practicum.io;
/**
 * Class to read data from the index file created
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import cs4321.TreeNodes.IndexNode;
import cs4321.TreeNodes.LeafNode;
import cs4321.TreeNodes.Node;
import cs4321.TreeNodes.Rid;

public class BinaryTreeReader {
	
	private int orderOfTree;
	private String pathToIndexFile;
	
	
	public BinaryTreeReader(int order,String indexPath){
		this.orderOfTree=order;
		this.pathToIndexFile=indexPath;
		
	}
	
	/**
	 * Function to deserialize the node and return a java object containing the contents.
	 * This can be either a leaf or index node. Corresponding objects will be created and returned
	 * 
	 * @param pageNumber
	 * @return
	 */
	public Node readPageWithPageNumber(int pageNumber){
		Node node = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(pathToIndexFile);
			FileChannel channel = fileInputStream.getChannel();
			ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
			channel.position(4096*pageNumber);
			buffer.clear();
			channel.read(buffer);
			int isLeafNode = buffer.getInt(0);
			// 1 indicates a index node 
			//read the number of keys
			// read all the children
			// read all the child addresses. Create a Index Node and send back
			if(isLeafNode==1)
			{
				int numberOfKeys = buffer.getInt(4);
				ArrayList<Integer> keys = new ArrayList<Integer>();
				ArrayList<Integer> children= new ArrayList<Integer>();
				int currentReadPointer = 8;
				for (int keyIndex=0;keyIndex< numberOfKeys;keyIndex++)
				{
					keys.add(buffer.getInt(currentReadPointer));
					currentReadPointer+=4;
				}
				for (int currentChildIndex=0;currentChildIndex<numberOfKeys+1;currentChildIndex++)
				{
					int childAddress=(buffer.getInt(currentReadPointer));
					children.add(childAddress);
					currentReadPointer+=4;
				}
				
				node=new IndexNode(orderOfTree,keys,children);
				channel.close();
				fileInputStream.close();
			}
			
			// 0 indicates a leaf node 
			// read the number of keys
			// read all the keys
			// read all the RID values. Create a leaf Node and send back
			if (isLeafNode==0){
				int numberOfKeys = buffer.getInt(4);
				LinkedHashMap<Integer, ArrayList<Rid>> leafDataEntry =  new LinkedHashMap<Integer, ArrayList<Rid>>();
				int keyIndex = 0;
				int currentPointer = 8;
				while (keyIndex < numberOfKeys){
					ArrayList<Rid> currentEntries = new ArrayList<Rid>();
					int key = buffer.getInt(currentPointer);
					currentPointer = currentPointer + 4;
					int numberOfRids = buffer.getInt(currentPointer);
					currentPointer= currentPointer + 4;
					Rid rid = null;
					for (int ridIndex = 0 ; ridIndex < numberOfRids ; ridIndex++){
						int pageId = buffer.getInt(currentPointer);
						int tupleId = buffer.getInt(currentPointer+4);
						currentPointer= currentPointer + 8;
						rid = new Rid(pageId, tupleId);
						currentEntries.add(rid);
					}
					leafDataEntry.put(key, currentEntries);
					keyIndex++;
					
				}
				node = new LeafNode(this.orderOfTree,leafDataEntry);
				channel.close();
				fileInputStream.close();
				
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return node;
	}

	
}

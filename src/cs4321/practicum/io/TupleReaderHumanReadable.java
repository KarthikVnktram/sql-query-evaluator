package cs4321.practicum.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class for reading all tuples
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
 public class TupleReaderHumanReadable implements TupleReader{

	private Scanner fileScanner;
	private String dataPath;
	
	public TupleReaderHumanReadable(String filePath){
		try {
			dataPath = filePath;
			//initialize the file scanner
			fileScanner =  new Scanner(new File(this.dataPath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Integer> getNextTuple() {
		if (fileScanner.hasNextLine()) {
			String nextLine = fileScanner.nextLine();
			
			String[] nextTuples = nextLine.split("\\,");
			
			List<Integer> tuple = new ArrayList<Integer>();
			
			for (int i = 0; i<nextTuples.length;i++){
				tuple.add(Integer.parseInt(nextTuples[i]));
			}
			
			return tuple;
			
		}
		else {
			return null;
		}
	}

	@Override
	public void reset() {
		fileScanner.close();
		try {
			this.fileScanner = new Scanner(new File(this.dataPath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void close() {
		fileScanner.close();
		
	}
	
	public static String getTupleInFormatRequired(List<Integer> tuple){
		String concatenatedTupleString="";
		
		int n = tuple.size();
		for (int i = 0 ; i < n-1 ; i++){
			concatenatedTupleString += tuple.get(i) +",";
		}
		concatenatedTupleString += tuple.get(n-1);
		
		return concatenatedTupleString;
	}

	
	/**
	 * resets the file the file to the position indicated in index
	 * @param index
	 */
	public void reset(int index) {
		// TODO Auto-generated method stub
		this.close();
		try {
			this.fileScanner =  new Scanner(new File(this.dataPath));
			for(int i = 0;i < index;i++) {
				getNextTuple();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

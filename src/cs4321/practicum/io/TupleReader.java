package cs4321.practicum.io;

import java.util.List;


/**
 * An abstract class for reading all tuples
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public interface TupleReader {
	/**
	 * gets the next tuple
	 * @return
	 */
	public List<Integer> getNextTuple();
	
	/**
	 * resets the readers
	 */
	public void reset();
	
	/**
	 * closes the reader
	 */
	public void close();
	


}

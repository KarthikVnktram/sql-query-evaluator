package cs4321.practicum.io;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;


/**
 * A class to write binary files
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class TupleWriterBinaryFormat implements TupleWriter{

	private FileOutputStream fileOutputStream;
	private ByteBuffer buffer;
	private int numberOfTuples;
	
	private String path;
	private FileChannel channel;
	
	private int numAttribuesInTuples;
	private int nextTupleIndex;
	
	private int maxSizOfEachPage = 4096;

	
	public TupleWriterBinaryFormat(String path) {
		 this.path = path;
		 buffer = ByteBuffer.allocateDirect(maxSizOfEachPage);
		 buffer.clear();
		 nextTupleIndex = 8;
		 try {
			fileOutputStream = new FileOutputStream(this.path);
			channel = fileOutputStream.getChannel();
			numberOfTuples = 0;
			numAttribuesInTuples = 0;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean hasMoreTuples(){
		return (nextTupleIndex + 4*numAttribuesInTuples > maxSizOfEachPage);
	}
	
	public boolean endOfPageReached(){
		return (nextTupleIndex + 4*numAttribuesInTuples > maxSizOfEachPage);
	}
	 
	@Override
	public void write(List<Integer> tuple) {
		if (this.hasMoreTuples()){
			this.cleanUpTuples();
		}
		
		int n = tuple.size();
		this.numAttribuesInTuples = n;
		for (int i = 0 ; i<n ; i++){
			buffer.putInt(nextTupleIndex, tuple.get(i));
			nextTupleIndex+=4;
		}
		numberOfTuples++;
		
	}
	
	@Override
	public void cleanUpTuples() {
		if (numberOfTuples > 0){
			
			buffer.putInt(0, numAttribuesInTuples);
			buffer.putInt(4, numberOfTuples);
			try {
				channel.write(buffer);
				buffer.clear();
				
				numberOfTuples = 0;
				
				nextTupleIndex = 8;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}

	@Override
	public void reset() {
		buffer.clear();
		try {
			channel.close();
			fileOutputStream.close();

			fileOutputStream=new FileOutputStream(this.path);
			
			channel = fileOutputStream.getChannel();
			
			nextTupleIndex = 8;
			
			numberOfTuples = 0;
			numAttribuesInTuples = 0 ;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void flush() {
		if (numberOfTuples > 0){
			
			buffer.putInt(0, numAttribuesInTuples);
			buffer.putInt(4, numberOfTuples);
			try {
				channel.write(buffer);
				buffer.clear();
				nextTupleIndex = 8;
				numberOfTuples = 0;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}

	@Override
	public void close() {
		try {
			channel.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}

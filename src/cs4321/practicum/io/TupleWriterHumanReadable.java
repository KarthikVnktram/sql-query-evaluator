package cs4321.practicum.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
/**
 * File to read the files in human readable format
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class TupleWriterHumanReadable implements TupleWriter{

	private FileOutputStream fileOutputStream;

	private String outputfilePath;

	
	private OutputStreamWriter outputStreamWriter;
	
	public TupleWriterHumanReadable(String path) {
		this.outputfilePath = path;
		
	}
	
	
	@Override
	public void write(List<Integer> tuple) {
		if (this.fileOutputStream == null){
			this.open();
		}
		String tupleArray = TupleReaderHumanReadable.getTupleInFormatRequired(tuple);
		try {
			outputStreamWriter.append(tupleArray+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void write(String stringToPrint) {
		if (this.fileOutputStream == null){
			this.open();
		}
		
		try {
			outputStreamWriter.append(stringToPrint+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@Override
	public void cleanUpTuples() {
		
		
	}

	@Override
	public void reset() {
		this.close();
		this.open();
	}

	@Override
	public void close() {
		try {
			outputStreamWriter.close();
			fileOutputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void open(){
		try {
			fileOutputStream = new FileOutputStream(outputfilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	}

	public void flush() {
		
		
	}

}

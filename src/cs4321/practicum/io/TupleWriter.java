package cs4321.practicum.io;

import java.util.List;
/**
 * Abstract class for writing tuples
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public interface TupleWriter {
	
	public abstract void write(List<Integer> tuple);
	
	public abstract void cleanUpTuples();
	
	public abstract void reset();
	
	public abstract void close();
	
}

package cs4321.practicum.io;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import cs4321.TreeNodes.IndexNode;
import cs4321.TreeNodes.LeafNode;
import cs4321.TreeNodes.Rid;


/**
 * A class to write binary files
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class NodeWriterBinaryFormat implements NodeWriter{

	private FileOutputStream fileOutputStream;
	private ByteBuffer buffer;
		
	private String path;
	private FileChannel channel;

	private int maxSizOfEachPage = 4096;
	private int pageId = 0;
	
	
	public NodeWriterBinaryFormat(String path) {
		 this.path = path;
		 buffer = ByteBuffer.allocateDirect(maxSizOfEachPage);
		 buffer.clear();
		 try {
			fileOutputStream = new FileOutputStream(this.path);
			channel = fileOutputStream.getChannel();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * function to skip header file. Write some random data and come back to it later
	 * 
	 */
	public void skipFirstNode(){
		try {
			channel.position(0);
			buffer.clear();
			buffer.putInt(0,1);
			buffer.putInt(0,2);
			buffer.putInt(0,3);
			channel.write(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * function to write header page to the indexfile
	 * 
	 */
	public void writeHeaderFile(int addressOfRoot,int numberOfLeaves , int order){
		buffer.clear();
		try {
			channel.position(0);
			buffer.putInt(0,addressOfRoot);
			buffer.putInt(4,numberOfLeaves);
			buffer.putInt(8,order);
			channel.write(buffer);
			channel.close();
			fileOutputStream.close();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
 
	/**
	 * 
	 * function to write data to the leaf node
	 * 
	 */
	@Override
	public int write(LeafNode leafNode) {
		buffer.clear();
		int numberOfKeys = leafNode.getLeafDataEntry().size();
		buffer.putInt(0,0);
		buffer.putInt(4,numberOfKeys);
		int runningCounter = 0;
		int firstKeyAddress = 8;
		int factor = 2;
		for(int key: leafNode.getLeafDataEntry().keySet()){
			buffer.putInt(firstKeyAddress + 4 * runningCounter,key);
			ArrayList<Rid> rids = leafNode.getLeafDataEntry().get(key);
			int numberOfRids = rids.size();
			buffer.putInt(firstKeyAddress + 4 * runningCounter+4,numberOfRids);
			for(int ridNumber= 0;ridNumber < rids.size();ridNumber++) {
				buffer.putInt(firstKeyAddress + runningCounter * 4 + firstKeyAddress * ridNumber + 8,rids.get(ridNumber).getPageId());
				buffer.putInt(firstKeyAddress + runningCounter * 4 + firstKeyAddress * ridNumber + 12,rids.get(ridNumber).getTupleId());
			}
			runningCounter = 	runningCounter + factor* rids.size()+ factor;
		}
		try {
			channel.write(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pageId++;
		return pageId;
		
	}
	
	
	/**
	 * function to write indexNode into index file
	 * 
	 */
	@Override
	public int write(IndexNode indexNode) {
		buffer.clear();
		int numberOfKeys = indexNode.getNumberOfKeys();
		buffer.putInt(0,1);
		buffer.putInt(4,numberOfKeys);
		int actualKeyIndex = 8;
		for(int key : indexNode.getActualKeys()){
			buffer.putInt(actualKeyIndex,key);
			actualKeyIndex = actualKeyIndex +4;
		}
		for(int childAddress : indexNode.getAddressOfChildren()){
			buffer.putInt(actualKeyIndex,childAddress);
			actualKeyIndex = actualKeyIndex + 4;
		}
		try {
			channel.write(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pageId++;
		return pageId;
		
	}
	
	@Override
	public void cleanUpTuples() {
		
	}

	@Override
	public void reset() {
		
	}
	
	
	public void flush() {
	
		
		
	}

	@Override
	public void close() {
		try {
			channel.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}

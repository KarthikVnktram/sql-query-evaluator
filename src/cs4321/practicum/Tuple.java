package cs4321.practicum;

import java.util.ArrayList;
import java.util.List;

import cs4321.TreeNodes.Rid;
import net.sf.jsqlparser.schema.Table;
/**
 * Tuple Data Structure consisting of table for normal operation and List<Table> for joins and List<Integer> for storing the values of the tuples
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class Tuple {

	public Table table;
	public List<Integer> tuple;
	public List<Table> tables;
	
	//added a RID field in tuple. This is a combination of page id and tuple id of each tuple
	Rid rid = null;

	public Tuple(Table fromTable, List<Integer> tuple){
		this.table = fromTable;
		this.tuple = tuple;
	}

	public Tuple(List<Table> tables, List<Integer> joinedValues) {
		// TODO Auto-generated constructor stub
		this.tables= tables;
		tuple = joinedValues;
	}

	public List<Table> getTables(){
		List<Table> tupleTables = new ArrayList<Table>();
				
		if(this.table == null ){
			tupleTables.addAll(this.tables);
		} else {
			tupleTables.add(this.table);
		}
		return tupleTables;
		
	}
	public Rid getRid() {
		return rid;
	}

	public void setRid(Rid rid) {
		this.rid = rid;
	}
		
}

package cs4321.practicum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import cs4321.TreeNodes.IndexBuilder;
import cs4321.practicum.PhysicalOperators.ScanOperator;
import cs4321.practicum.utilities.ConversionUtility;

/**
 * Main Class of the project which accepts input folder and output folder and calls the SqlQueryInterpretor
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class SqlTranslator {
	
	public static void main(String[] args) {
		
		
		String interpreterFile = args[0];
		FileReader fileReader;
		try {			
			fileReader = new FileReader(interpreterFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String inputDirectory = bufferedReader.readLine();
			String outputDirectory = bufferedReader.readLine();
			String temporaryDirectory = bufferedReader.readLine();
			
			DatabaseGlobalCatalog databaseGlobalCatalog =DatabaseGlobalCatalog.getInstance();
			databaseGlobalCatalog.setInputFilePath(inputDirectory);
			databaseGlobalCatalog.setOutputFilePath(outputDirectory);
			databaseGlobalCatalog.setTempDirectory(temporaryDirectory);
			databaseGlobalCatalog.setSchemaPath(databaseGlobalCatalog.getInputFilePath()+File.separator+"db");
			databaseGlobalCatalog.setDataPath(databaseGlobalCatalog.getSchemaPath()+File.separator+"data");
			databaseGlobalCatalog.setIndexesPath(databaseGlobalCatalog.getSchemaPath()+File.separator+"indexes");
			databaseGlobalCatalog.setStatsPath(databaseGlobalCatalog.getSchemaPath());
			databaseGlobalCatalog.setBuildIndexes(true);
			databaseGlobalCatalog.setEvaluateQueries(true);
			
			
			SqlQueryInterpreter sqlQueryInterpreter = new SqlQueryInterpreter();
			sqlQueryInterpreter.translateQueries(true,true);		

			fileReader.close();
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

package cs4321.practicum.LogicalPlanUtilities;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cs4321.practicum.AbstractExpressionEvaluator;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 *  This class outputs 
 *         The UnionFind Elements and a list of unusable comparisions which would help in selection pushing     
 *           
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class UnionFindWhereClauseVisitor extends AbstractExpressionEvaluator {

	//List of unusable comparisions
	List<Expression> unUsableComparisions = new ArrayList<Expression>();
	
	//union find element
	UnionFind unionFind= null;
	
	public UnionFindWhereClauseVisitor() {
		unionFind = new UnionFind();
	}
	
	/**
	 * visits AndExpression and calls the left and right child
	 */
	@Override
	public void visit(AndExpression andExpression) {
		// TODO Auto-generated method stub
		andExpression.getLeftExpression().accept(this);
		andExpression.getRightExpression().accept(this);
		
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(EqualsTo equalsTo) {
		// TODO Auto-generated method stub
		buildExpressions(equalsTo);
	}
	

	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		buildExpressions(notEqualsTo);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThan greaterThan) {
		buildExpressions(greaterThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		buildExpressions(greaterThanEquals);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThan minorThan) {
		buildExpressions(minorThan);
	}
	
	/**
	 * Calls buidExpression to build the selection conditions map , join conditions map and neutral expressions
	 */
	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		buildExpressions(minorThanEquals);
	}	


	/**
	 * Method to create Union Find element and the list of of unusable comparision
	 * Logic: 
	 * 			Check if the expression is of the type   -->   Column Operator Column
	 * 				  if expression is of type EqualsTo    then
	 * 						merge the left and right expression
	 * 				  else 
	 * 					 add to unusable expressions
	 * 			if the expression is of type       -->    Column Operator Value   OR Value Operator Column
	 * 				  if the expression is of the type 
	 * 					 add to unusable expressions
	 * 				  if not set lower bounds, upper bounds and equality constraints appropriately
	 * 			
	 * @return
	 */
	private void buildExpressions(BinaryExpression expression) {
	
		if(expression.getLeftExpression() instanceof Column && expression.getRightExpression() instanceof Column) {
			 // logic to create and merge UnionFindElements
			// if expression is a not equals expression add to unUsableComparisions
			
			if( expression instanceof EqualsTo){
				//getUnionFindElement Creates a new one if not already present
				UnionFindElement unionFindElementLeft = unionFind.getUnionFindElement((Column)expression.getLeftExpression());
				unionFindElementLeft.addExpression(expression);
				UnionFindElement unionFindElementRight = unionFind.getUnionFindElement((Column)expression.getRightExpression());
				unionFind.mergeUnionFindElements(unionFindElementLeft, unionFindElementRight);
				
			} else { //if(expression instanceof NotEqualsTo){
				unUsableComparisions.add(expression);
			}
			
			
		} else if(expression.getLeftExpression() instanceof Column && expression.getRightExpression() instanceof LongValue) {
			// Logic to set the lower , upper bounds and equality constraints
			long value = ((LongValue)expression.getRightExpression()).getValue();
			
			if(expression instanceof NotEqualsTo){
				unUsableComparisions.add(expression);
			}else{
				UnionFindElement unionFindElement = unionFind.getUnionFindElement((Column)expression.getLeftExpression());
				unionFindElement.addExpression(expression);
				
				if(expression instanceof MinorThan){
					if(isUpperBoundSettingNeeded(value-1, unionFindElement.getUpperBound()) == true){
						unionFindElement.setUpperBound(value-1);
					}
				} else if(expression instanceof MinorThanEquals){
					if(isUpperBoundSettingNeeded(value, unionFindElement.getUpperBound()) == true){
						unionFindElement.setUpperBound(value);
					}
					
				} else if(expression instanceof GreaterThan){
					if(isLowerBoundSettingNeeded(value+1, unionFindElement.getLowerBound()) == true){
						unionFindElement.setLowerBound(value+1);
					}
				} else if(expression instanceof GreaterThanEquals){
					if(isLowerBoundSettingNeeded(value, unionFindElement.getLowerBound()) == true){
						unionFindElement.setLowerBound(value);
					}
				} else if(expression instanceof EqualsTo){
					unionFindElement.setUpperBound(value);
					unionFindElement.setLowerBound(value);
					unionFindElement.setEqualityConstraint(value);
				} 
			}
			
		} else if(expression.getLeftExpression() instanceof LongValue && expression.getRightExpression() instanceof Column){
			// Logic to set the lower , upper bounds and equality constraints
			
			long value = ((LongValue)expression.getLeftExpression()).getValue();
			if(expression instanceof NotEqualsTo){
				unUsableComparisions.add(expression);
			} else {
				UnionFindElement unionFindElement = unionFind.getUnionFindElement((Column)expression.getRightExpression());
				unionFindElement.addExpression(expression);
				if(expression instanceof MinorThan){
					if(isLowerBoundSettingNeeded(value+1, unionFindElement.getLowerBound()) == true){
						unionFindElement.setLowerBound(value+1);
					}
				} else if(expression instanceof MinorThanEquals){
					if(isLowerBoundSettingNeeded(value, unionFindElement.getLowerBound()) == true){
						unionFindElement.setLowerBound(value);
					}
				} else if(expression instanceof GreaterThan){
					if(isUpperBoundSettingNeeded(value-1, unionFindElement.getUpperBound()) == true){
						unionFindElement.setUpperBound(value-1);
					}
				} else if(expression instanceof GreaterThanEquals){
					if(isUpperBoundSettingNeeded(value-1, unionFindElement.getUpperBound()) == true){
						unionFindElement.setUpperBound(value);
					}
				} else if(expression instanceof EqualsTo){
					unionFindElement.setUpperBound(value);
					unionFindElement.setLowerBound(value);
					unionFindElement.setEqualityConstraint(value);
					
				} 
			}
		}
		
	}
	
	/**
	 * Method to check if the upper bound has to be reset
	 * @param newValue
	 * @param oldValue
	 * @return
	 */
	private boolean isUpperBoundSettingNeeded(long newValue,Long oldValue){
		if(oldValue == null || (newValue  < oldValue) ){
			return true;
		}
		return false;
	}
	
	/**
	 * Method to check if the lower bound has to be reset
	 * @param newValue
	 * @param oldValue
	 * @return
	 */
	private boolean isLowerBoundSettingNeeded(long newValue,Long oldValue){
		if(oldValue == null || (newValue  > oldValue) ){
			return true;
		}
		return false;
	}

	/**
	 * getter for unusable comparisions
	 * @return
	 */
	public List<Expression> getUnUsableComparisions() {
		return unUsableComparisions;
	}

	/**
	 * Method to getUnionFind Elements
	 * @return
	 */
	public UnionFind getUnionFind() {
		return unionFind;
	}

	
}

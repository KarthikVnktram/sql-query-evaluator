package cs4321.practicum.LogicalPlanUtilities;

import java.util.LinkedList;
import java.util.List;

import net.sf.jsqlparser.schema.Column;


/**
 * Class containing all the union find elements
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class UnionFind {
	//List of Union Find elements
	List<UnionFindElement> unionFindElements;
	
	public UnionFind(){
		this.unionFindElements = new LinkedList<UnionFindElement>();
	}
	
	/**
	 * adds a union find element to list
	 * @param unionFindElement
	 */
	public void addUnionFindElement(UnionFindElement unionFindElement){
		unionFindElements.add(unionFindElement);
	}
	
	/**
	 * removes the union find elements from the list
	 * @param unionFindElement
	 */
	public void removeUnionFindElement(UnionFindElement unionFindElement){
		UnionFindElement unionFindElementToRemove = null;
		for(UnionFindElement ufElement: unionFindElements){
			if(ufElement.attributes.equals(unionFindElement.attributes)){
				unionFindElementToRemove = ufElement;
				break;
			}
			
		}
		
		unionFindElements.remove(unionFindElementToRemove);
	}
	
	/**
	 * fetches the union find element matching the column
	 * @param attribute
	 * @return
	 */
	public UnionFindElement getUnionFindElement(Column attribute){
		
		for(UnionFindElement unionFindElement: unionFindElements){
			if(unionFindElement.hasAttribute(attribute)){
				return unionFindElement;
			}
		}
		
		UnionFindElement newUnionFindElement = new UnionFindElement(null, null, null);
		newUnionFindElement.addElement(attribute);
		this.addUnionFindElement(newUnionFindElement);
		return newUnionFindElement;
	
	}
	
	/**
	 * Merges Multiple Union Find elements together
	 * @param unionFindElement
	 * @param otherUnionFindElement
	 * @return
	 */
	public UnionFindElement mergeUnionFindElements(UnionFindElement unionFindElement,UnionFindElement otherUnionFindElement){
		unionFindElement.mergeElements(otherUnionFindElement);
		Long lowerBoundLeft = unionFindElement.getLowerBound();
		Long higherBoundLeft = unionFindElement.getUpperBound();
		Long lowerBoundRight = otherUnionFindElement.getLowerBound();
		Long higherBoundRight = otherUnionFindElement.getUpperBound();
		if(lowerBoundLeft == null || lowerBoundRight == null){
			if(lowerBoundLeft == null){
				unionFindElement.setLowerBound(lowerBoundRight);
			} else { //lowerBoundRight == null
				// do nothing
			}
		} else{
			if(lowerBoundRight > lowerBoundLeft ){
				unionFindElement.setLowerBound(lowerBoundRight);
			}
		}
		
		if(higherBoundLeft == null || higherBoundRight == null){
			if(higherBoundLeft == null){
				unionFindElement.setLowerBound(higherBoundRight);
			} else { //lowerBoundRight == null
				// do nothing
			}
		} else{
			if(higherBoundRight < higherBoundLeft ){
				unionFindElement.setLowerBound(higherBoundRight);
			}
		}
		
		this.unionFindElements.remove(otherUnionFindElement);
		return unionFindElement;
		
	}
	
	/**
	 * getter to get all union Find elements
	 * @return
	 */
	public List<UnionFindElement> getUnionFindElements() {
		return unionFindElements;
	}

	/**
	 * Sets all union find elements
	 * @param unionFindElements
	 */
	public void setUnionFindElements(List<UnionFindElement> unionFindElements) {
		this.unionFindElements = unionFindElements;
	}

	
}

package cs4321.practicum.LogicalPlanUtilities;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;

/**
 * Class representing Union Find Elements
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class UnionFindElement {
	
	//Attributes of UnionFindElement
	List<Column> attributes;


	Long upperBound ;
	
	Long lowerBound ;
	Long equalityConstraint ; // defining it as Long, as the equality Constraint can be null
	
	List<Expression> unionFindExpressions = new ArrayList<Expression>();

	public UnionFindElement(Long upperBound, Long lowerBound,Long equalityConstriaint){
	
		this.attributes = new ArrayList<Column>();
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.equalityConstraint = equalityConstriaint;
		
	}
	
	/**
	 * Adds attribute to the attributes list
	 * @param elementToAdd
	 */
	public void addElement(Column elementToAdd){
		this.attributes.add(elementToAdd);
	}
	
	
	/**
	 * Merges Multiple UnionFindElements together
	 * @param unionFindElement
	 */
	public void mergeElements(UnionFindElement unionFindElement){
		this.attributes.addAll(unionFindElement.attributes);
	}
	
	/**
	 * method to check if an attribute exists in a Union Find Element
	 * @param attribute
	 * @return
	 */
	public boolean hasAttribute(Column attribute){
		
		for(Column attributeToCompare: this.attributes){
			if(attributeToCompare.toString().equals(attribute.toString())){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * method to check if an attribute exists in a Union Find Element
	 * @param attribute
	 * @return
	 */
	public boolean hasAttributeColumnName(String columnName){
		
		for(Column attributeToCompare: this.attributes){
			if(attributeToCompare.toString().equals(columnName)){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * method to check if an attribute belonging to a particular table exists in a Union Find Element
	 * @param attribute
	 * @return
	 */
	public boolean hasAttributeBelongingToTable(String table){
		
		for(Column tableToCompare: this.attributes){
			if(getTableNameToCompare(tableToCompare.getTable()).equals(table)){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * method to return an  attribute belonging to a particular table
	 * @param attribute
	 * @return
	 */
	public Column getAttributeBelongingToTable(String table){
		
		for(Column tableToCompare: this.attributes){
			if(getTableNameToCompare(tableToCompare.getTable()).equals(table)){
				return tableToCompare;
			}
		}
		return null;
		
	}
	
	
	/**
	 * method to returns attributes belonging to a particular table
	 * @param attribute
	 * @return
	 */
	public List<Column> getAttributesBelongingToTable(String table){
		List<Column> columnsToCompare = new ArrayList<Column>();
		for(Column tableToCompare: this.attributes){
			if(getTableNameToCompare(tableToCompare.getTable()).equals(table)){
				columnsToCompare.add(tableToCompare);
			}
		}
		return columnsToCompare;
		
	}
	
	/**
	 * return the appropriate table name to compare
	 * @param table
	 * @return
	 */
	public String getTableNameToCompare(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	/**
	 * get Upper Bound
	 * @return
	 */
	public Long getUpperBound() {
		return upperBound;
	}

	/**
	 * set upper bound
	 * @param upperBound
	 */
	public void setUpperBound(Long upperBound) {
		this.upperBound = upperBound;
	}

	/**
	 * get lower bound
	 * @return
	 */
	public Long getLowerBound() {
		return lowerBound;
	}

	/**
	 * set lower bound
	 * @param lowerBound
	 */
	public void setLowerBound(Long lowerBound) {
		this.lowerBound = lowerBound;
	}

	/**
	 * get the equality constraint
	 * @return
	 */
	public Long getEqualityConstraint() {
		return equalityConstraint;
	}

	/**
	 * set equality constraint
	 * @param equalityConstraint
	 */
	public void setEqualityConstraint(Long equalityConstraint) {
		this.equalityConstraint = equalityConstraint;
	}
	
	/**
	 * gets all the atributes
	 * @return
	 */
	public List<Column> getAttributes() {
		return attributes;
	}
	
	/**
	 * adds the expressions to list
	 * @param expression
	 */
	public void addExpression(Expression expression){
		
		this.unionFindExpressions.add(expression);
		
	}
	
	
	public List<Expression> getUnionFindExpressions() {
		return unionFindExpressions;
	}


}

package cs4321.practicum.PlanBuilders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;

import cs4321.practicum.SqlQueryInterpreter;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import cs4321.TreeNodes.IndexBuilder;
import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.IndexExpressionVisitor;
import cs4321.practicum.IndexInfo;
import cs4321.practicum.SMJWhereClauseVisitor;
import cs4321.practicum.Tuple;
import cs4321.practicum.LogicalOperators.DuplicationEliminationOperatorLogical;
import cs4321.practicum.LogicalOperators.JoinOperatorLogical;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.LogicalOperators.LogicalPlanVisitor;
import cs4321.practicum.LogicalOperators.ProjectOperatorLogical;
import cs4321.practicum.LogicalOperators.ScanOperatorLogical;
import cs4321.practicum.LogicalOperators.SelectOperatorLogical;
import cs4321.practicum.LogicalOperators.SortOperatorLogical;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import cs4321.practicum.LogicalPlanUtilities.UnionFindElement;
import cs4321.practicum.PhysicalOperators.BNLJOperator;
import cs4321.practicum.PhysicalOperators.DuplicateEliminationOperator;
import cs4321.practicum.PhysicalOperators.ExternalSortOperator;
import cs4321.practicum.PhysicalOperators.IndexScanOperator;
import cs4321.practicum.PhysicalOperators.JoinOperator;
import cs4321.practicum.PhysicalOperators.Operator;
import cs4321.practicum.PhysicalOperators.ProjectOperator;
import cs4321.practicum.PhysicalOperators.SMJOperator;
import cs4321.practicum.PhysicalOperators.ScanOperator;
import cs4321.practicum.PhysicalOperators.SelectOperator;
import cs4321.practicum.PhysicalOperators.SortOperator;
import cs4321.practicum.utilities.DBUtils;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;

/**
 * Class for Building the Physical Operators from the created Logical Operator visitor tree. 
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class PhysicalQueryPlanBuilder implements LogicalPlanVisitor {

	private LogicalOperator logicalPlan;
	private Operator physicalTreeRoot;

	public PhysicalQueryPlanBuilder(LogicalOperator plan) {
		this.logicalPlan=plan;
		this.parsePlanBuilderConfig();
	}
	int sortType;

	int joinType;

	int bufferPagesForJoin;

	int bufferPagesForSort;

	boolean useIndexesForSelection = false;


	public void parsePlanBuilderConfig(){

		bufferPagesForJoin = 10000;
		sortType = 1;
		bufferPagesForSort = 10000;
		useIndexesForSelection = true;

		
	}

	public void buildPhysicalQueryTree(){
		logicalPlan.accept(this);
	}

	public Operator getPhysicalTreeroot(){
		return this.physicalTreeRoot;
	}

	@Override
	public void visit(ScanOperatorLogical scanOperatorLogical) {
		this.physicalTreeRoot = new ScanOperator(scanOperatorLogical.getFrom());

	}

	
	/**
	 * Find all the indexes present already
	 * 		Separate the expressions into index expressions and select expressions using index expression visitor
	 * 		Set the children correctly
	 * 
	 */
	@Override
	public void visit(SelectOperatorLogical selectOperatorLogical) {
		selectOperatorLogical.getChild().accept(this);
		Operator tmp = this.physicalTreeRoot;
		boolean isIndexOperatorNeeded = false;
		boolean isSelectOperatorNeeded = false;
		long lowKey = Long.MAX_VALUE;
		long highKey = Long.MIN_VALUE;
		Table fromTable = null;
		DatabaseGlobalCatalog databaseGlobalCatalog = DatabaseGlobalCatalog.getInstance();
		fromTable = ((ScanOperator)tmp).getFromTable();

		String keyToSearchIndex = null;
		//		if(fromTable.getAlias() != null){
		//			keyToSearchIndex =  fromTable.getAlias();
		//		}else {
		keyToSearchIndex = fromTable.getName();
		//		}
		int scanCost=(int) Math.ceil(getScanCost(keyToSearchIndex));
		String indexesFolderPath = databaseGlobalCatalog.getIndexesPath()  ;
		int indexCost=Integer.MAX_VALUE;
		IndexInfo bestIndex=null;
		List<Expression> bestSelectExpressionList = new ArrayList<Expression>();
		List<Expression> bestIndexExpressionList = new ArrayList<Expression>();
		List<Expression> backedUpSelectExpressionList =  new ArrayList<Expression>();
		if(useIndexesForSelection == true && tmp instanceof ScanOperator){
			// match and read only selected index condition
			// Handle the case of Multiple Aliases for the same table
			if( (databaseGlobalCatalog.indexTable.containsKey(keyToSearchIndex)) ) {
				//				System.out.println("Index match found for table - "+ keyToSearchIndex);
				//				String columnName = databaseGlobalCatalog.indexTable.get(keyToSearchIndex);
				List<IndexInfo> indexInfoObjs = databaseGlobalCatalog.indexTable.get(keyToSearchIndex);
				for(IndexInfo indexInfoObj: indexInfoObjs){
					IndexExpressionVisitor indexExpressionVisitor = new IndexExpressionVisitor(fromTable, indexInfoObj.getColumnName());
					selectOperatorLogical.getWhere().accept(indexExpressionVisitor);

					//set the new SelectoperatorLogical
					List<Expression> selectExpressions = indexExpressionVisitor.getSelectExpressions();
					List<Expression> indexExpressions = indexExpressionVisitor.getIndexExpressions();

					if(selectExpressions.size() > 0 || indexExpressions.size() > 0){
						isSelectOperatorNeeded = true;
						backedUpSelectExpressionList.clear();
						backedUpSelectExpressionList.addAll(selectExpressions);
						backedUpSelectExpressionList.addAll(indexExpressions);
						//						Expression selectExpressionConcatenated = buildConcatenatedAndExpressions(selectExpressions);
						//						selectOperatorLogical.setW11hereClause(selectExpressionConcatenated);   should be done later
					}
					if(indexExpressions.size() > 0){
						isIndexOperatorNeeded = true;
						lowKey = indexExpressionVisitor.getLowKey();
						highKey = indexExpressionVisitor.getHighKey();

						// You will have low and high key value here per attribute
						int curIndexCost=Integer.MAX_VALUE;
						try {
							curIndexCost = (int) Math.ceil(getIndexCost(keyToSearchIndex,indexInfoObj,lowKey,highKey));
						} catch (IOException e) {
							e.printStackTrace();
						}
						if(curIndexCost<indexCost){
							indexCost=curIndexCost;
							bestIndex=indexInfoObj;
							bestSelectExpressionList.clear();
							bestSelectExpressionList.addAll(selectExpressions);
							bestIndexExpressionList.clear();
							bestIndexExpressionList.addAll(indexExpressions);

						}

					} else {
//						bestSelectExpressionList.clear();
//						bestSelectExpressionList.addAll(selectExpressions);
					}


				}

			} else {
				isIndexOperatorNeeded = false;
				isSelectOperatorNeeded = true;
				bestSelectExpressionList.add(selectOperatorLogical.getWhere());
			}
		}
		if(scanCost<indexCost){
//			System.out.println();
//			System.out.println();
//			System.out.println("scan cost is : "+scanCost+" , AND index cost is GREATER than scan cost with best index as : "+keyToSearchIndex+"."+( bestIndex != null? bestIndex.getColumnName() : "")+" and cost: "+indexCost);
			isIndexOperatorNeeded = false;
			if(bestSelectExpressionList.size() > 0 || bestIndexExpressionList.size() > 0){
				bestSelectExpressionList.addAll(bestIndexExpressionList);
				
			} else {
				bestSelectExpressionList.addAll(backedUpSelectExpressionList);
			}
			Expression selectExpressionConcatenated = buildConcatenatedAndExpressions(bestSelectExpressionList);
			selectOperatorLogical.setWhereClause(selectExpressionConcatenated);

		}else{
//			System.out.println();
//			System.out.println();
			isIndexOperatorNeeded = true;
			if(bestSelectExpressionList.size() > 0){
				isSelectOperatorNeeded = true;
			} else {
				isSelectOperatorNeeded = false;
			}
//			System.out.println("scan cost is : "+scanCost+" , AND index cost is LESSER than scan cost with best index as : "+keyToSearchIndex+"."+(bestIndex != null? bestIndex.getColumnName() : "")+" and cost: "+indexCost);
			Expression selectExpressionConcatenated = buildConcatenatedAndExpressions(bestSelectExpressionList);
			selectOperatorLogical.setWhereClause(selectExpressionConcatenated);
		}
		Operator rootOperator=null;
		if(isIndexOperatorNeeded == true && isSelectOperatorNeeded == true){
//			System.out.println("Index read from location"+ indexesFolderPath+File.separator+keyToSearchIndex+"."+bestIndex.getColumnName());
//			System.out.println("Select Expression Contains" + selectOperatorLogical.getWhere());

			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(fromTable, bestIndex.getColumnName());
			IndexScanOperator indexScanOperatorOpr = new IndexScanOperator(fromTable,DatabaseGlobalCatalog.getInstance().getPathToTable(fromTable) , lowKey, highKey,
					indexesFolderPath+File.separator+keyToSearchIndex+"."+bestIndex.getColumnName(),bestIndex.isClustered(),index,keyToSearchIndex+"."+bestIndex.getColumnName());
			SelectOperator selectOperator = new SelectOperator(indexScanOperatorOpr, selectOperatorLogical.getWhere());
			rootOperator = selectOperator;

		} else if(isIndexOperatorNeeded == true){
//			System.out.println("Index read from location"+ indexesFolderPath+File.separator+keyToSearchIndex+"."+bestIndex.getColumnName());
//			System.out.println("No Select Needed");
			int index = DatabaseGlobalCatalog.getInstance().getIndexFromTable(fromTable, bestIndex.getColumnName());
			IndexScanOperator indexScanOperatorOpr = new IndexScanOperator(fromTable,DatabaseGlobalCatalog.getInstance().getPathToTable(fromTable) , lowKey, highKey,
					indexesFolderPath + File.separator+keyToSearchIndex+"."+bestIndex.getColumnName(),bestIndex.isClustered(),index,keyToSearchIndex+"."+bestIndex.getColumnName());
			rootOperator = indexScanOperatorOpr;
		} else if(isSelectOperatorNeeded == true){
//			System.out.println("No Index Match, just a normal select getting run");
//			System.out.println("Select Expression Contains" + selectOperatorLogical.getWhere());
			SelectOperator selectOperator = new SelectOperator(tmp, selectOperatorLogical.getWhere());
			rootOperator = selectOperator;
		} else {
//			System.out.println("No Index Match, just a normal select getting run");
//			System.out.println("Select Expression Contains" + selectOperatorLogical.getWhere());
			SelectOperator selectOperator = new SelectOperator(tmp, selectOperatorLogical.getWhere());
			rootOperator = selectOperator;
		}

		physicalTreeRoot = rootOperator;

	}

	private double getIndexCost(String keyToSearchIndex, IndexInfo indexInfoObj,long lowkey,long highkey) throws IOException {
		double indexCost=3.0d;//Tree traversal cost assumption
		double r=getReductionFactor(keyToSearchIndex,indexInfoObj,lowkey,highkey);
		int p=(int) Math.ceil(getScanCost(keyToSearchIndex));
		if(indexInfoObj.isClustered()==true){
			indexCost+=(p*r);
		}else{
			int t=DBUtils.getTupleCount(keyToSearchIndex);
			int l=DBUtils.getNumberOfLeavesInIndexFilePath(DatabaseGlobalCatalog.getInstance().getIndexesPath()+File.separator+keyToSearchIndex+"."+indexInfoObj.getColumnName());
			indexCost+=(r*(l+t));
		}
		return indexCost;
	}

	private int getLeavesCount(String keyToSearchIndex, IndexInfo indexInfoObj) {
		return 0;
	}

	/**
	 * Method to get the reduction factor for a particular indexInfo object
	 * 
	 * @param keyToSearchIndex
	 * @param indexInfoObj
	 * @param lowkey
	 * @param highkey
	 * @return
	 * @throws IOException
	 */
	private double getReductionFactor(String keyToSearchIndex, IndexInfo indexInfoObj, long lowkey, long highkey) throws IOException {
		double reductionFactor=0.0d;
		BufferedReader br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
		String line=br.readLine();
		String allFields[]=null;
		while(line!=null){
			allFields=line.split(" ");
			if(allFields[0].equals(keyToSearchIndex)){
				break;
			}
			line=br.readLine();
		}
		int i=2;
		String attributeField[]=null;
		while(i<allFields.length){
			attributeField=allFields[i].split(",");
			if(attributeField[0].equals(indexInfoObj.getColumnName())){
				break;
			}
			i++;
		}
		long min=Long.parseLong(attributeField[1]);
		long max=Long.parseLong(attributeField[2]);
		long numerator=1;
		if(highkey==Long.MAX_VALUE && lowkey==Long.MIN_VALUE){
			numerator=DBUtils.getTupleCount(keyToSearchIndex);
		}else{
			if (highkey==Long.MAX_VALUE){
				highkey=max;
			}
			else if(lowkey==Long.MIN_VALUE){
				lowkey=min;
			}
			numerator=highkey-lowkey+1;
		}
		reductionFactor=(double)numerator/(max-min+1);
		br.close();
		return reductionFactor;
	}

	/**
	 * returns the scan cost from the stats file
	 * @param tableName
	 * @return
	 */
	private double getScanCost(String tableName) {
		double scanCost=0.0d;
		BufferedReader br=null;
		String relationStats=null;
		try {
			br=new BufferedReader(new FileReader(DatabaseGlobalCatalog.getInstance().getStatsPath()+File.separator+DatabaseGlobalCatalog.getInstance().getStatsName()));
			relationStats=br.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(relationStats!=null){
			String relationName=relationStats.split(" ")[0];
			if(relationName.equals(tableName)){
				break;
			}
			try {
				relationStats=br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String tupleCount=relationStats.split(" ")[1];
		int attributeCount=relationStats.split(" ").length-2;
		int sizeOfEachAttribute=4;
		int allAttributesSize=attributeCount*sizeOfEachAttribute;
		scanCost=(allAttributesSize*Integer.parseInt(tupleCount))/4088.0d;
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return scanCost;
	}


	/**
	 * builds and concatenates the select expressions from a list to a single expression
	 * @param selectExpressions
	 * @return
	 */
	private Expression buildConcatenatedAndExpressions(List<Expression> selectExpressions) {
		// TODO Auto-generated method stub
		if(selectExpressions.size() == 0){
			return null;
		}
		Expression firstSelectExpression = selectExpressions.get(0);
		for(int i = 1 ; i < selectExpressions.size() ; i++){
			Expression subsequentExpression = selectExpressions.get(i);
			firstSelectExpression = buildAndExpression(firstSelectExpression, subsequentExpression);
		}
		return firstSelectExpression;
	}

	/**
	 * Builds the AND Expression
	 * 
	 * @param expression
	 * @param previousExpression
	 * @return
	 */
	private Expression buildAndExpression(Expression expression, Expression previousExpression) {
		// TODO Auto-generated method stub
		AndExpression andExpression = new AndExpression(expression, previousExpression);
		return andExpression;
	}

	
	/**
	 * 
	 * This method contains the logic to create the left Deep Tree from the join order
	 * 		For each table in sublist
	 * 		Get the Union Find Element AND unusedComparisions containing both the tables[or tables getting joined]
	 * 		Get the corresponding attributes
     *  	Build an equalsTo expression , put in a list. 
     *  	Final concatenate the list using AND operator
     *  	This becomes the join expression
	 *	Now Based on the join Selection Logic, a jointype is chosen
	 *	And the corresponding Operator is created
	 */
	@Override
	public void visit(JoinOperatorLogical joinOperatorLogical) {
		Operator[] operator = new Operator[2];

		//joinType=getJoinType(joinOperatorLogical.getUnionFindForJoin());
		JoinPhysicalPlanBuilder joinPhysicalPlanBuilder = new JoinPhysicalPlanBuilder(joinOperatorLogical);
		List<Table> joinTablesList = joinPhysicalPlanBuilder.generateJoinOrder(joinOperatorLogical.getUnionFindForJoin());

		List<Table> tablesJoined = new ArrayList<Table>();
		LogicalOperator opLeft = getOperatorForTable(joinTablesList.get(0),joinOperatorLogical.getChildrenOfJoin()); 
		opLeft.accept(this);
		operator[0] = this.physicalTreeRoot;
		LogicalOperator opRight = getOperatorForTable(joinTablesList.get(1),joinOperatorLogical.getChildrenOfJoin()); 
		opRight.accept(this);
		
		operator[1]=this.physicalTreeRoot;
	
		tablesJoined.add(joinTablesList.get(0));
		tablesJoined.add(joinTablesList.get(1));
		//Use this list to get the index expressions
		List<Expression> joinListExpressions = createAndBuildJoinExpression(tablesJoined.subList(0, tablesJoined.size()-1),tablesJoined.get(tablesJoined.size()-1),joinOperatorLogical.getUnionFindForJoin(),joinOperatorLogical.getUnusableComparisions());
		Expression joinExpression = DBUtils.buildExpressionFromList(joinListExpressions);
		
		List<Expression> joinListExpressionsJoinSelector=getJoinExpressionsListForJoinSelection(tablesJoined.subList(0, tablesJoined.size()-1),tablesJoined.get(tablesJoined.size()-1),joinOperatorLogical.getUnionFindForJoin(),joinOperatorLogical.getUnusableComparisions());
		//Expression joinExpressionJoinSelector=DBUtils.buildExpressionFromList(joinListExpressionsJoinSelector);
		List<String> tables=new ArrayList<String>();
		for(Table table:tablesJoined){
			if(table.getAlias()!=null){
				tables.add(table.getAlias());
			}else{
				tables.add(table.getName());
			}
		}
		List<Expression> unusableComparisions=joinOperatorLogical.getUnusableComparisions();
		List<Column> unusedColumns=new ArrayList<Column>();
		for(Expression unusedExpression: unusableComparisions){
			if( ((BinaryExpression)unusedExpression).getLeftExpression() instanceof Column && ((BinaryExpression)unusedExpression).getRightExpression() instanceof Column ){
				if(tables.contains(((Column) ( (BinaryExpression) unusedExpression).getLeftExpression()).getTable()) && tables.contains(((Column) ( (BinaryExpression) unusedExpression).getRightExpression()).getTable())){
					unusedColumns.add((Column) ((BinaryExpression)unusedExpression).getLeftExpression());
					unusedColumns.add((Column) ((BinaryExpression)unusedExpression).getRightExpression());
				}
			}
		}
		
		List<Column> equalsColumns=new ArrayList<Column>();
		if(joinListExpressionsJoinSelector != null){
			for(Expression equalExpression: joinListExpressionsJoinSelector){
				if( ((BinaryExpression)equalExpression).getLeftExpression() instanceof Column && ((BinaryExpression)equalExpression).getRightExpression() instanceof Column ){
					if(tables.contains(((Column) ( (BinaryExpression) equalExpression).getLeftExpression()).getTable().toString()) && tables.contains(((Column) ( (BinaryExpression) equalExpression).getRightExpression()).getTable().toString())){
						equalsColumns.add((Column) ((BinaryExpression)equalExpression).getLeftExpression());
						equalsColumns.add((Column) ((BinaryExpression)equalExpression).getRightExpression());
					}
				}
			}
		}
		
		
		joinType=getJoinType(tables,unusedColumns,equalsColumns);
		//joinType=getJoinType(joinOperatorLogical.getUnionFindForJoin(),tables,tables,unusedColumns);
//		System.out.println("Selected join: "+joinType+" for tables "+tables.get(0)+", "+tables.get(1));
		createJoinOperator(operator[0], operator[1], joinExpression, tablesJoined);
		for(int i = 2 ; i < joinTablesList.size() ; i++ ){
			operator[0]= this.physicalTreeRoot;
			
			LogicalOperator opIntermediate = getOperatorForTable(joinTablesList.get(i),joinOperatorLogical.getChildrenOfJoin()); 
			
			opIntermediate.accept(this);
			operator[1] = this.physicalTreeRoot;
			tablesJoined.add(joinTablesList.get(i));
			//Use this list to get the index expressions
			List<Expression> joinListExpr = createAndBuildJoinExpression(tablesJoined.subList(0, tablesJoined.size()-1),tablesJoined.get(tablesJoined.size()-1),joinOperatorLogical.getUnionFindForJoin(),joinOperatorLogical.getUnusableComparisions());
			joinExpression = DBUtils.buildExpressionFromList(joinListExpr);
			List<String> tablesJoinedForJoinSelection=tables;
			tables=new ArrayList<String>();
			if(joinTablesList.get(i).getAlias()!=null){
				tables.add(joinTablesList.get(i).getAlias());
				tablesJoinedForJoinSelection.add(joinTablesList.get(i).getAlias());
			}else{
				tables.add(joinTablesList.get(i).getName());
				tablesJoinedForJoinSelection.add(joinTablesList.get(i).getName());
			}
			
			
			
			unusedColumns=new ArrayList<Column>();
			for(Expression unusedExpression: unusableComparisions){
				if( ((BinaryExpression)unusedExpression).getLeftExpression() instanceof Column && ((BinaryExpression)unusedExpression).getRightExpression() instanceof Column ){
					if(tables.contains(((Column) ( (BinaryExpression) unusedExpression).getLeftExpression()).getTable().toString()) || tables.contains(((Column) ( (BinaryExpression) unusedExpression).getRightExpression()).getTable().toString())){
						unusedColumns.add((Column) ((BinaryExpression)unusedExpression).getLeftExpression());
						unusedColumns.add((Column) ((BinaryExpression)unusedExpression).getRightExpression());
					}
				}
			}
			joinListExpressionsJoinSelector=getJoinExpressionsListForJoinSelection(tablesJoined.subList(0, tablesJoined.size()-1),tablesJoined.get(tablesJoined.size()-1),joinOperatorLogical.getUnionFindForJoin(),joinOperatorLogical.getUnusableComparisions());
			equalsColumns=new ArrayList<Column>();
			for(Expression equalExpression: joinListExpressionsJoinSelector){
				if( ((BinaryExpression)equalExpression).getLeftExpression() instanceof Column && ((BinaryExpression)equalExpression).getRightExpression() instanceof Column ){
					if(tables.contains(((Column) ( (BinaryExpression) equalExpression).getLeftExpression()).getTable().toString()) || tables.contains(((Column) ( (BinaryExpression) equalExpression).getRightExpression()).getTable().toString())){
						equalsColumns.add((Column) ((BinaryExpression)equalExpression).getLeftExpression());
						equalsColumns.add((Column) ((BinaryExpression)equalExpression).getRightExpression());
					}
				}
			}

			joinType=getJoinType(tables,unusedColumns,equalsColumns);
			//joinType=getJoinType(joinOperatorLogical.getUnionFindForJoin(),tables,tablesJoinedForJoinSelection,unusedColumns);
//			System.out.println("Selected join: "+joinType+" for the next table "+tables);
			createJoinOperator(operator[0], operator[1], joinExpression, tablesJoined);
		}

		}


	/**
	 * 
	 * Method to decide whether a Sort Merge Join or BNLJ has to be chosen
	 * Logic for Selecting BNLJ/SMJ:
	Cases when SMJ will be used:
		(1)SMJ will be used only if and only if there is an equality condition between attributes
			e.g.: If Sailors,Reserves are getting joined and the join condition is Sailors.A=Reserves.B, then it goes to step (2).
			  If the join condition is anything other than '=' like Sailors.A!=Reserves.B , Sailors.A>=Reserves.B, etc, then SMJ won't be used.
		(2)Apart from this, we have made further refinement over SMJ selection by not allowing the cross product to go to SMJ as BNLJ with required 
		   buffers will be able to process these cross products faster comparatively.
		  
		Cases when BNLJ will be used:
			In all the cases where SMJ is not used, BNLJ will be used.
	 * 
	 * 
	 * @param tables
	 * @param unusedColumns
	 * @param equalsColumns
	 * @return
	 */
	private int getJoinType(List<String> tables, List<Column> unusedColumns, List<Column> equalsColumns) {
		final int SMJ=2;
		final int BNLJ=1;
		if(unusedColumns.size()>=1 || equalsColumns.size()==0){
			return BNLJ;
		}else{
			return SMJ;
		}
		/*boolean value=isIndexPresentInJoin(equalsColumns);
		if(value==true){
			return SMJ;
		}else{
			return BNLJ;
		}*/
		
	}

	
	/**
	 * Method to check if an index is present on any of the columns that are getting joined
	 * 
	 * @param equalsColumns
	 * @return
	 */
	private boolean isIndexPresentInJoin(List<Column> equalsColumns) {
		
		HashMap<String,List<IndexInfo>> indexTable=DatabaseGlobalCatalog.getInstance().indexTable;
		if(indexTable==null || indexTable.size()<=0){
			return false;
		}
		for(String key:indexTable.keySet()){
			List<IndexInfo> indexInfos=indexTable.get(key);
			for(IndexInfo indexInfo:indexInfos){
				if(indexInfo.isClustered()==true){
					for(String keyInner:SqlQueryInterpreter.allTables.keySet()){
						if(key.equals(SqlQueryInterpreter.allTables.get(keyInner))){
							//boolean value=getColumnNamesPartOfUnionFind(keyInner+"."+indexInfo.getColumnName(), unionFind,tables,tablesJoined);
							boolean value=check(keyInner+"."+indexInfo.getColumnName(),equalsColumns);
							if(value==true){
								return value;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * method to match index columns
	 * 
	 * @param index
	 * @param equalsColumns
	 * @return
	 */
	private boolean check(String index,List<Column> equalsColumns)
	{
		for(Column column: equalsColumns){
			if(column.toString().equals(index)){
				return true;
				}
			}
		return false;
	}
	
	/**
	 * Creates and builds join expression - Expressions that will be point of Join Physical Operator
	 * 
	 * @param subList
	 * @param table
	 * @param unionFindForJoin
	 * @param unusableComparisions
	 * @return
	 */
	private List<Expression> createAndBuildJoinExpression(List<Table> subList,
			Table table, UnionFind unionFindForJoin,List<Expression> unusableComparisions) {
		// TODO Auto-generated method stub 
		
		//for each table in sublist , get the UFE containing both the tables, get the attributes corresponding,
		// build an equalsTo expression , put in a list. Final concatenate the list with AND operator and  return
		if(unionFindForJoin == null){
			return null;
		}
		
		List<Expression> listOfEqualsExpressions = new ArrayList<Expression>();
		for(Table tableInList: subList){
			Expression equalToExpressionToAdd = getExpressionForJoiningTables(tableInList,table,unionFindForJoin);
			if(equalToExpressionToAdd !=null){
				listOfEqualsExpressions.add(equalToExpressionToAdd);
			}
			Expression unusableMatchedComparision= getUnusedExpressionMatchingTables(unusableComparisions,tableInList,table);
			if(unusableMatchedComparision != null){
				listOfEqualsExpressions.add(unusableMatchedComparision);
			}
		}
		return listOfEqualsExpressions;
		
	}
	
	
	/**
	 * Method to get Join Expressions List For Join Type Selection
	 * 
	 * @param subList
	 * @param table
	 * @param unionFindForJoin
	 * @param unusableComparisions
	 * @return
	 */
	private List<Expression> getJoinExpressionsListForJoinSelection(List<Table> subList,
			Table table, UnionFind unionFindForJoin,List<Expression> unusableComparisions) {
		
		//for each table in sublist , get the UFE containing both the tables, get the attributes corresponding,
		// build an equalsTo expression , put in a list. Final concatenate the list with AND operator and  return
		if(unionFindForJoin == null){
			return null;
		}
		
		List<Expression> listOfEqualsExpressions = new ArrayList<Expression>();
		for(Table tableInList: subList){
			List<Expression> equalToExpressionToAdd = getExpressionListForJoiningTables(tableInList,table,unionFindForJoin);
			if(equalToExpressionToAdd !=null && equalToExpressionToAdd.size() > 0){
				listOfEqualsExpressions.addAll(equalToExpressionToAdd);
			}
		}
		return listOfEqualsExpressions;
		
	}

	/**
	 * Method to obtain unused expressions that match the tables getting joined
	 * 
	 * @param unusableComparisions
	 * @param leftTable
	 * @param rightTable
	 * @return
	 */
	private Expression getUnusedExpressionMatchingTables(
			List<Expression> unusableComparisions, Table leftTable,
			Table rightTable) {
		// TODO Auto-generated method stub
		List<Expression> matchingExpressions = new ArrayList<Expression>();
 		for(Expression unusedExpression: unusableComparisions){
			if( ((BinaryExpression)unusedExpression).getLeftExpression() instanceof Column && ((BinaryExpression)unusedExpression).getRightExpression() instanceof Column ){
				Column leftColumn= (Column) ((BinaryExpression)unusedExpression).getLeftExpression();
				Column rightColumn =(Column) ((BinaryExpression)unusedExpression).getRightExpression();
				boolean leftTableMatchfound =false;
				if(getTableNameToCompare(leftTable).equals(getTableNameToCompare(leftColumn.getTable()))  || 
						getTableNameToCompare(leftTable).equals(getTableNameToCompare(rightColumn.getTable()))){
					leftTableMatchfound = true;
				}
				boolean rightTableMatchFound = false;
				if(getTableNameToCompare(rightTable).equals(getTableNameToCompare(leftColumn.getTable()))  || 
						getTableNameToCompare(rightTable).equals(getTableNameToCompare(rightColumn.getTable()))){
					rightTableMatchFound = true;
				}
				
				if(leftTableMatchfound == true && rightTableMatchFound == true){
					matchingExpressions.add(unusedExpression);
				}
				
			}
		}
		
		return DBUtils.buildExpressionFromList(matchingExpressions);
		
	}

	private Expression getExpressionForJoiningTables(Table tableInList,
			Table table, UnionFind unionFindForJoin) {
		// TODO Auto-generated method stub
		List<Expression> expressionsNeeded = new ArrayList<Expression>();
		for(UnionFindElement unionFindElement: unionFindForJoin.getUnionFindElements()){
			List<Column> leftTableAttribute = unionFindElement.getAttributesBelongingToTable(getTableNameToCompare(tableInList));
			List<Column> rightTableAttribute = unionFindElement.getAttributesBelongingToTable(getTableNameToCompare(table));
			for(Column leftColumn: leftTableAttribute){
				for(Column rightColumn: rightTableAttribute){
					Expression equalsTo = DBUtils.createEqualsToExpression(leftColumn, rightColumn);
					expressionsNeeded.add(equalsTo);	
				}
			}
		}
		
		return DBUtils.buildExpressionFromList(expressionsNeeded);
	}
	
	/**
	 * Method to return a list of expressions part of join 
	 * 
	 * @param tableInList
	 * @param table
	 * @param unionFindForJoin
	 * @return
	 */
	private List<Expression> getExpressionListForJoiningTables(Table tableInList,
			Table table, UnionFind unionFindForJoin) {
		// TODO Auto-generated method stub
		List<Expression> expressionsNeeded = new ArrayList<Expression>();
		for(UnionFindElement unionFindElement: unionFindForJoin.getUnionFindElements()){
			List<Column> leftTableAttribute = unionFindElement.getAttributesBelongingToTable(getTableNameToCompare(tableInList));
			List<Column> rightTableAttribute = unionFindElement.getAttributesBelongingToTable(getTableNameToCompare(table));
			for(Column leftColumn: leftTableAttribute){
				for(Column rightColumn: rightTableAttribute){
					Expression equalsTo = DBUtils.createEqualsToExpression(leftColumn, rightColumn);
					expressionsNeeded.add(equalsTo);	
				}
			}
		}
		
		return expressionsNeeded;
	}

	/**
	 * Method to get the operator sitting on top of base table
	 * 
	 * @param table
	 * @param childrenOfJoin
	 * @return
	 */
	public LogicalOperator getOperatorForTable(Table table,List<LogicalOperator> childrenOfJoin ){

		for(LogicalOperator operator : childrenOfJoin){
			if(operator instanceof SelectOperatorLogical){
				if( ((SelectOperatorLogical) operator).getChild() instanceof ScanOperatorLogical ){
					if(  getTableNameToCompare(((ScanOperatorLogical) (((SelectOperatorLogical) operator).getChild())).getFrom()).equals(getTableNameToCompare(table))){
						return operator;
					}
				}

			} else if(operator instanceof ScanOperatorLogical){
				if(  getTableNameToCompare(  ((ScanOperatorLogical)operator).getFrom() ).equals(getTableNameToCompare(table))){
					return operator;
				}
			}
		}
		return null;

	}

	public String getTableNameToCompare(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	
	/**
	 * Method to get Tables from Alias match
	 * 
	 * @param tableAttributesMap
	 * @param leftTable
	 * @return
	 */
	private List<Column> getTableFromAlias(
			Map<Table, List<Column>> tableAttributesMap, Table leftTable) {
		// TODO Auto-generated method stub
		
		for(Table table: tableAttributesMap.keySet()){

			String toCompare = "";
			if(leftTable.getAlias() != null){
				toCompare = leftTable.getAlias();
			} else {
				toCompare = leftTable.getName();
			}

			if(table.getName().equals(toCompare)){
				return tableAttributesMap.get(table);
			}

		}
		return null;
	}


	
	private List<Column> getTablesFromAlias(
			Map<Table, List<Column>> tableAttributesMap, List<Table> leftTables) {
		// TODO Auto-generated method stub
		List<Column> columns = new ArrayList<Column>();
		for(Table table: tableAttributesMap.keySet()){
			
			for(Table tableNew : leftTables){
				String toCompare = "";
				if(tableNew.getAlias() != null){
					toCompare = tableNew.getAlias();
				} else {
					toCompare = tableNew.getName();
				}
	
				if(table.getName().equals(toCompare)){
					columns.addAll(tableAttributesMap.get(table));
				}
			}	
		}
		return columns;
	}
	

	/**
	 * 
	 * Creates the Join Operator using the Left Child, Right Child, Join Expression and List of tables getting joined
	 * 
	 * @param operatorLeft
	 * @param operatorRight
	 * @param joinExpression
	 * @param joinedTables
	 */
	private void createJoinOperator(Operator operatorLeft, Operator operatorRight,Expression joinExpression, List<Table> joinedTables ){	
		Table leftTable  = null;
		Table rightTable = null;
		if (joinType == 0){
			physicalTreeRoot = new JoinOperator(operatorLeft, operatorRight, joinedTables, joinExpression);
		}
		else if (joinType == 1){
			physicalTreeRoot = new BNLJOperator(operatorLeft, operatorRight, joinExpression, bufferPagesForJoin,joinedTables);

		}
		else if (joinType == 2){


			Map<Table,List<Column>> tableAttributesMap=getOuterRelationAttributes(joinExpression);
			List<Table> leftTables = null;

			if(operatorLeft instanceof SelectOperator){
				leftTable = ((ScanOperator)((SelectOperator)operatorLeft).getChild()).getFromTable();
			}else if(operatorLeft instanceof ScanOperator){
				leftTable = ((ScanOperator)operatorLeft).getFromTable();
			} else if(operatorLeft instanceof SMJOperator){
				leftTables = new ArrayList<Table>();
				leftTables.addAll(((SMJOperator)operatorLeft).getJoinedTables());
				leftTable = getTableFromAliasForSMJ(tableAttributesMap,((SMJOperator)operatorLeft).getJoinedTables());
			}else if(operatorLeft instanceof BNLJOperator){
				leftTables = new ArrayList<Table>();
				leftTables.addAll(((BNLJOperator)operatorLeft).getTables());
				leftTable = getTableFromAliasForSMJ(tableAttributesMap,((BNLJOperator)operatorLeft).getTables());
			}else if(operatorLeft instanceof IndexScanOperator){
				leftTable = ((IndexScanOperator)operatorLeft).getFromTable();
			}

			if(operatorRight instanceof SelectOperator){
				rightTable = ((ScanOperator)((SelectOperator)operatorRight).getChild()).getFromTable();
			}else if(operatorRight instanceof ScanOperator){
				rightTable = ((ScanOperator)operatorRight).getFromTable();
			}else if(operatorLeft instanceof IndexScanOperator){
				rightTable = ((IndexScanOperator)operatorRight).getFromTable();
			}
			
			List<Column> leftAttributes = null;
			
			boolean multiTable = false;
			if(leftTables != null){
				leftAttributes = getTablesFromAlias(tableAttributesMap,leftTables);
				multiTable = true;
			} else{
				leftAttributes = getTableFromAlias(tableAttributesMap,leftTable);
			}
			List<Column> rightAttributes = getTableFromAlias(tableAttributesMap,rightTable);	

			if(sortType == 1){
				ExternalSortOperator leftExternalSortOperator = null;
				if(multiTable == true){
					leftExternalSortOperator = new ExternalSortOperator(leftTable ,leftAttributes , bufferPagesForSort,leftTables,operatorLeft);
				} else{
					leftExternalSortOperator = new ExternalSortOperator(operatorLeft,leftTable ,leftAttributes , bufferPagesForSort,leftTables);
				}
				ExternalSortOperator rightExternalSortOperator = new ExternalSortOperator(operatorRight,rightTable,rightAttributes , bufferPagesForSort,null);

				physicalTreeRoot = new SMJOperator(leftExternalSortOperator, rightExternalSortOperator, joinExpression, bufferPagesForJoin,joinedTables);

			} else {
				SortOperator leftSortOperator =null;
				if(leftTables != null) {
					leftSortOperator = new SortOperator(operatorLeft,leftTable,leftAttributes,((SMJOperator)operatorRight).getJoinedTables());	
				} else {
					leftSortOperator = new SortOperator(operatorLeft,leftTable,leftAttributes);
				}

				leftSortOperator.sorter();
				SortOperator rightSortOperator = new SortOperator(operatorRight,rightTable,rightAttributes,null );
				rightSortOperator.sorter();
				physicalTreeRoot = new SMJOperator(leftSortOperator, rightSortOperator, joinExpression, bufferPagesForJoin,joinedTables);
			}
		}
	}

	private Table getTableFromAliasForSMJ(
			Map<Table, List<Column>> tableAttributesMap, List<Table> leftTables) {
		// TODO Auto-generated method stub

		for(Table table: tableAttributesMap.keySet()){

			for(Table lefttable: leftTables){
				String tableToCompare = "";

				if(lefttable.getAlias() != null){
					tableToCompare = lefttable.getAlias();
				} else {
					tableToCompare = lefttable.getName();
				}


				if(table.getName().equals(tableToCompare)){
					return lefttable;
				}	
			}


		}
		return null;
	}


	@Override
	public void visit(ProjectOperatorLogical projectOperatorLogical) {
		projectOperatorLogical.getChild().accept(this);
		Operator tmp = this.physicalTreeRoot;
		if(projectOperatorLogical.getColumnNames() != null){
			physicalTreeRoot = new ProjectOperator(tmp,projectOperatorLogical.getColumnNames());
		} else {
			physicalTreeRoot = new ProjectOperator(projectOperatorLogical.getAllColumns(),tmp);
		}

	}

	@Override
	public void visit(SortOperatorLogical sortOperatorLogical) {
		sortOperatorLogical.getChild().accept(this);
		Operator temporaryRoot = this.physicalTreeRoot;
		if (sortType == 0){
			SortOperator sort;
			if(sortOperatorLogical.isOrderByPresent() == true){
				sort = new SortOperator(temporaryRoot, sortOperatorLogical.getOrderByElements());
			} else {

				sort = new SortOperator( sortOperatorLogical.getColumns(), temporaryRoot);
			}
			sort.sorter();
			physicalTreeRoot = sort;

		}
		else {
			//TODO 
			if(sortOperatorLogical.isOrderByPresent() == true){

				if(temporaryRoot instanceof ProjectOperator) {
					ExternalSortOperator leftExternalSortOperator = new ExternalSortOperator(temporaryRoot, ((ProjectOperator)temporaryRoot).getColumnFiltered(),bufferPagesForSort , sortOperatorLogical.getOrderByElements());
					physicalTreeRoot = leftExternalSortOperator;
				}
			} else {
				ExternalSortOperator leftExternalSortOperator = new ExternalSortOperator(temporaryRoot, sortOperatorLogical.getColumns(),bufferPagesForSort);
				physicalTreeRoot = leftExternalSortOperator;
			}
		}

	}

	@Override
	public void visit(DuplicationEliminationOperatorLogical duplicationEliminationOperatorLogical) {
		duplicationEliminationOperatorLogical.getChild().accept(this);
		Operator tmp = this.physicalTreeRoot;
		physicalTreeRoot = new DuplicateEliminationOperator(tmp);
	}

	public Map<Table,List<Column>> getOuterRelationAttributes(Expression expression){
		SMJWhereClauseVisitor smjWhereClauseVisitor=new SMJWhereClauseVisitor();
		expression.accept(smjWhereClauseVisitor);
		Map<Table,List<Column>> tableAttributesMap=smjWhereClauseVisitor.getTableAttributeMap();
		/*int totalAttributesCount;
		Table table[]=new Table[2];
		boolean compareFlag=false;
		int i=0;
		for(List<String> value:tableAttributesMap.values())
		{
			totalAttributesCount=value.size();
			break;
		}
		for(Table key:tableAttributesMap.keySet())
		{
			table[i++]=key;
		}
		for(i=0;i<totalAttributesCount;i++)
		{

			int result1=DatabaseGlobalCatalog.getInstance().getAttributesFromTableWithColumnName(leftTuple, table[0], tableAttributesMap.get(table[0]).get(i), true);
		}*/
		return tableAttributesMap;
	}
	
	
	/*private int getJoinType(UnionFind unionFind,List<String> tables,List<String> tablesJoined,List<Column> unusedColumns) {
//		if(SqlQueryInterpreter.orderByPresent==true){
//			boolean value=getColumnNamesPartOfUnionFind(SqlQueryInterpreter.firstOrderByElement, unionFind,tables,tablesJoined);
////			boolean valueSelectCheck=checkOrderByInSelect(SqlQueryInterpreter.firstOrderByElement);
//			boolean valueSelectCheck=false;
//			if(value || valueSelectCheck){
//				return 2; 
//			}
//		}
		if(unusedColumns.size()>=1){
			boolean result=isAttributeInUnused(unusedColumns,tables,tablesJoined);
			if(result==false){
				return 1;
			}
			return 1;
		}
		HashMap<String,List<IndexInfo>> indexTable=DatabaseGlobalCatalog.getInstance().indexTable;
		if(indexTable==null || indexTable.size()<=0){
			return 1;
		}
		for(String key:indexTable.keySet()){
			List<IndexInfo> indexInfos=indexTable.get(key);
			for(IndexInfo indexInfo:indexInfos){
				if(indexInfo.isClustered()==true){
					for(String keyInner:SqlQueryInterpreter.allTables.keySet()){
						if(key.equals(SqlQueryInterpreter.allTables.get(keyInner))){
							boolean value=getColumnNamesPartOfUnionFind(keyInner+"."+indexInfo.getColumnName(), unionFind,tables,tablesJoined);
							if(value==true){
								return 2;
							}
						}
					}
				}
			}
		}
		return 1;
	}*/
	

	private boolean isAttributeInUnused(List<Column> unusedColumns, List<String> tables,List<String> tablesJoined) {
		try
		{
			for(int i=0;i<unusedColumns.size();i++){
				if(tables.contains(unusedColumns.get(i).getTable().toString())){
					unusedColumns.remove(unusedColumns.get(i));
					i--;
				}
			}
		}catch(Exception e){
			return false;
		}
		for(Column column:unusedColumns){
			if(tablesJoined.contains(column.getTable().toString())){
				return true;
			}
		}
		return false;
	}

	private boolean getColumnNamesPartOfUnionFind(String columnName, UnionFind unionFind,List<String> tables,List<String> tablesJoinedSoFar){

		if(unionFind==null){
			return false;
		}
		List<UnionFindElement> beforeUnionFindElements=unionFind.getUnionFindElements();
		List<UnionFindElement> unionFindElements=new ArrayList<UnionFindElement>();
		for(UnionFindElement unionFindElement : beforeUnionFindElements){
			List<Column> columns=unionFindElement.getAttributes();
			boolean flag=false;
			for(Column column:columns){
				if(tablesJoinedSoFar.contains(column.getTable().toString())==false){
					flag=true;
					break;
				}
			}
			if(flag==true){
				continue;
			}
			for(Column column: columns){
				if(tables.contains(column.getTable().toString())==true){
					unionFindElements.add(unionFindElement);
					break;
				}
			}
		}
		for(UnionFindElement unionFindElement : unionFindElements){
			List<Column> columns=unionFindElement.getAttributes();
			if(columns.size()<=1){
				continue;
			}
			for(Column column: columns){
					if(column.getTable().toString().equals(columnName.split("\\.")[0])==false){
						if(unionFindElement.hasAttributeColumnName(columnName)){
							return true;
						}
					}
			}
		}
		return false;
	}

	private boolean checkOrderByInSelect(String firstOrderByElement) {
		List<String> selectItems=SqlQueryInterpreter.selectItems;
		for(String column:selectItems){
			if(column.equals("*") || column.equals(firstOrderByElement)){
				return true;
			}
		}
		return false;
	}
	
	
}

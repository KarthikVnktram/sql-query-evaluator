package cs4321.practicum.PlanBuilders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.text.TabExpander;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import cs4321.practicum.LogicalOperators.JoinOperatorLogical;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.LogicalOperators.ScanOperatorLogical;
import cs4321.practicum.LogicalOperators.SelectOperatorLogical;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import cs4321.practicum.LogicalPlanUtilities.UnionFindElement;
import cs4321.practicum.LogicalPlanUtilities.UnionFindWhereClauseVisitor;
import cs4321.practicum.PhysicalOperators.IndexScanOperator;
import cs4321.practicum.PhysicalOperators.Operator;
import cs4321.practicum.utilities.DBUtils;

public class JoinPhysicalPlanBuilder {
	
	JoinOperatorLogical joinOperatorLogical = null;
	
	HashMap<String, Long> relationToCostMap =  new HashMap<String, Long>();
	HashMap<String, Long> relationToSizeMap  =  new HashMap<String, Long>();
	HashMap<String,List<Table>> realtionToJoinOrderMap = new HashMap<String, List<Table>>();
	HashMap<Integer, List<Table>> relationToBestCostList = new HashMap<Integer, List<Table>>();
	
	
	public JoinPhysicalPlanBuilder(JoinOperatorLogical joinOperatorLogical){
	
		this.joinOperatorLogical = joinOperatorLogical;
		
	}
	
	/**
	 *     Implementation to get the join order of multiple tables. By using the Dynamic Programming Algorithm
	 *     For each subset of level (subsets of size 1,2,3, upto n)
	 *     	   For each permutation of each subset	
	 *     		  create a hashMap of relation to cost, relation to size 
	 *     		  use the size and cost of previously calculated relations
	 *     		  find the best total cost per subset[the permutation with the lowest cost in a subset] and store the same
	 *      return the best cost
	 * 		
	 * 
	 * 		The Key created in HashMap is of type Table1_Table2      
	 * 		The Elements are always sorted in the order in which the tables appear in join clause
	 * 
	 * @param unionFind
	 * @return
	 */
	public List<Table> generateJoinOrder(UnionFind unionFind){
		
		for(int i = 1 ; i <= joinOperatorLogical.getTables().size();i++){
			List<List<Table>> subsetList = getAllSubsetsOfGivenSize(joinOperatorLogical.getTables(), i);
			long bestCost = Long.MAX_VALUE;
			// for subsets of size 1 and 2. Just 
			for(List<Table> subset: subsetList){
				if(i == 1){
					String joinKey = getKeyFromTablesList(subset);
					relationToCostMap.put(joinKey, (long) 0);
					relationToSizeMap.put(joinKey, (long) calculateSizeForBaseTables(subset.get(0), unionFind));
					realtionToJoinOrderMap.put(joinKey, subset);
					continue;
				}
				
				if(i == 2) {
					String joinKey = getKeyFromTablesList(subset);
					relationToCostMap.put(joinKey, (long) 0);
					List<Table> firstTableList = new ArrayList<Table>();
					List<Table> secondTableList = new ArrayList<Table>();
					firstTableList.add(subset.get(0));
					secondTableList.add(subset.get(1));
					long firstTableSize =  getSize(firstTableList);//DBUtils.getTupleCount(subset.get(0).getName());
					long secondTableSize = getSize(secondTableList);//DBUtils.getTupleCount(subset.get(1).getName());
					List<Table> joinOrder = new ArrayList<Table>();
					if(firstTableSize < secondTableSize){
						joinOrder.add(subset.get(0));
						joinOrder.add(subset.get(1));
					} else {
						joinOrder.add(subset.get(1));
						joinOrder.add(subset.get(0));
					}
					relationToBestCostList.put(2, joinOrder);
					relationToSizeMap.put(getKeyFromTablesList(subset), calculateSize(subset,unionFind));
					realtionToJoinOrderMap.put(joinKey, joinOrder);
					continue;
				}
				long previousMinCost = Long.MAX_VALUE;
				List<Table> bestJoinOrder = new ArrayList<Table>();
				//for 3 or more
				for(List<Table> tableList: getAllPermutations(subset)){
					
					List<Table> subList = tableList.subList(0, tableList.size()-1);
					long cost = calculateCost(subList);
					long size= getSize(subList);
					long totalCost = cost + size;
					
					if(totalCost < previousMinCost){
						previousMinCost = totalCost;
						bestJoinOrder = new ArrayList<Table>();
						bestJoinOrder.addAll(realtionToJoinOrderMap.get(getKeyFromTablesList(subList)));
						bestJoinOrder.add(tableList.get(tableList.size()-1));
						
					}
					//keep track of lowest cost and store it and write outside
				}
				
				relationToSizeMap.put(getKeyFromTablesList(subset), calculateSize(subset,unionFind));
				relationToCostMap.put(getKeyFromTablesList(subset), previousMinCost);
				if(previousMinCost <bestCost ){
					relationToBestCostList.put(i, bestJoinOrder);
				}
				//this is not correct
				realtionToJoinOrderMap.put(getKeyFromTablesList(subset), bestJoinOrder);
			}
				
			
		}
		
		return relationToBestCostList.get(joinOperatorLogical.getTables().size());
	}
	
	/**
	 * Get the previously calculated cost
	 * @param subList
	 * @return
	 */
	public long calculateCost(List<Table> subList){
		
		return relationToCostMap.get(getKeyFromTablesList(subList));
		
	}
	
	/**
	 * get the previously calculated size
	 * @param subList
	 * @return
	 */
	public long getSize(List<Table> subList){
		return relationToSizeMap.get(getKeyFromTablesList(subList));
	}
	
	
	/**
	 * Calculate Size of a particular join order
	 * @param tablesList
	 * @param unionFind
	 * @return
	 */
	public long calculateSize(List<Table> tablesList,UnionFind unionFind){
		List<Table> subList = tablesList.subList(0, tablesList.size()-1);
		boolean isLeftAJoinRelation = false;
		if(subList.size() > 1){
			isLeftAJoinRelation = true;
		}
		
		
		
		Table tableGettingJoined = tablesList.get(tablesList.size()-1);
		long numerator = getSize(subList) * DBUtils.getTupleCount(tableGettingJoined.getName());
		long denominator =1;
		if(unionFind ==null){
			return numerator;
		}
		// if no join conditions match return the productOfSizeOfAllTables
		List<UnionFindElement> unionFindElements = getAllUnionFindElementsPartOfTable(getTableNameToCompare(tableGettingJoined), unionFind);
		if(unionFindElements.size() == 0){
			return numerator;
		} 		
		// if conditions match, then calculatevValues and find max in pairs. Use as denominator and proceed
		for(UnionFindElement unionFindElement : unionFindElements){
			Column leftTableColumn = null;
			Column rightTableColumn = null;
			Table leftTableWithMatch = null;
					
			for(Column column : unionFindElement.getAttributes()){
				if(getTableNameToCompare(column.getTable()).equals(getTableNameToCompare(tableGettingJoined))){
					rightTableColumn = column;
				}
				
				for(Table table :subList){
					if(getTableNameToCompare(column.getTable()).equals(getTableNameToCompare(table))){
						leftTableColumn = column;
						leftTableWithMatch = table;
					}
				}
			}
			
			boolean isSelectPresentRight = isSelectPresentOnTable(tableGettingJoined);
			boolean isBaseTableRight = true;
			if(isSelectPresentRight == true){
				isBaseTableRight = false;
			}
			boolean isSelectPresentLeft = false;
			boolean isBaseTableLeft = false;
			
			if(isLeftAJoinRelation == true){
				isBaseTableLeft = false;
				isSelectPresentLeft = false;
			} else {
				isSelectPresentLeft = isSelectPresentOnTable(tableGettingJoined);
				isBaseTableLeft = true;
				if(isSelectPresentLeft == true){
					isBaseTableLeft = false;
				}
			}
			if(leftTableColumn != null && rightTableColumn != null) {
				denominator = denominator * Math.max(calculateVvalues(tableGettingJoined, rightTableColumn.toString(), isBaseTableRight, isSelectPresentRight, unionFind,null) ,
					calculateVvalues(leftTableWithMatch, leftTableColumn.toString(), isBaseTableLeft, isSelectPresentLeft, unionFind,null));
			}
		}
		
		
		return numerator/denominator;
		
	}
	
	
	
	public long calculateSizeForBaseTables(Table table,UnionFind unionFind){
		
		long numerator =  DBUtils.getTupleCount(table.getName());
		if(unionFind == null){
			return numerator;
		} 
		// if no join conditions match return the productOfSizeOfAllTables
		List<UnionFindElement> unionFindElements = getAllUnionFindElementsPartOfTable(getTableNameToCompare(table), unionFind);
		if(unionFindElements.size() == 0){
			return numerator;
		} 	
		Long prevValues=null;
		// if conditions match, then calculatevValues and find max in pairs. Use as denominator and proceed
		for(UnionFindElement unionFindElement : unionFindElements){
			Column tableColumn = null;
					
			for(Column column : unionFindElement.getAttributes()){
				if(getTableNameToCompare(column.getTable()).equals(getTableNameToCompare(table))){
					tableColumn = column;
					break;
				}
			}
			
			boolean isSelectPresent = isSelectPresentOnTable(table);
			boolean isBaseTable = true;
			if(isSelectPresent == true){
				isBaseTable = false;
			}
			
			if(tableColumn != null ) {
				numerator = calculateVvaluesBaseTable(table, tableColumn.toString(), isBaseTable, isSelectPresent, unionFind, prevValues);
			}
			prevValues = numerator;
			
		}
		
		
		return numerator;
		
	}
	
	/**
	 * Method to check if there is select present on multiple tables
	 * @param table
	 * @return
	 */
	public boolean isSelectPresentOnTable(Table table){
		
		for(LogicalOperator operator : joinOperatorLogical.getChildrenOfJoin()){
			if(operator instanceof SelectOperatorLogical){
				if(((SelectOperatorLogical) operator).getChild() instanceof IndexScanOperator ){
					if(  getTableNameToCompare(((IndexScanOperator) (((SelectOperatorLogical) operator).getChild())).getFromTable()).equals(getTableNameToCompare(table))){
						 return true;
					 }
					
					
				} else if( ((SelectOperatorLogical) operator).getChild() instanceof ScanOperatorLogical ){
					 if(  getTableNameToCompare(((ScanOperatorLogical) (((SelectOperatorLogical) operator).getChild())).getFrom()).equals(getTableNameToCompare(table))){
						 return true;
					 }
				}
				
			}
		}
		return false;
		
	}
	
	public String getTableNameToCompare(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	/**
	 * Method to calculate V-Values
	 * 
	 *  There are 3 cases getting handled here 
	 *  	1. if the column is on a base table directly
	 *  		return max-min+1 reading from stats file
	 *  	2. If the column is on a Select operator
	 *  		get the total number of tuples. keep updating all the reduction factors
	 *  	3. If the column is on a join condition 
	 * 			calculate the min values of the columns on which equality condition is present
	 * 
	 * @param table
	 * @param columnName
	 * @param isBaseTable
	 * @param isSelectCondition
	 * @param unionFind
	 * @param previousRelSize
	 * @return
	 */
	public long calculateVvalues(Table table, String columnName,boolean isBaseTable, boolean isSelectCondition, UnionFind unionFind,Long previousRelSize){
		try {
			if(isBaseTable == true){
				return (long) DBUtils.getNumberOfUniqueValuesPerAttribute(table.getName(), columnName);
			} else if(isSelectCondition == true){
				
				long initialVvalue = (long) DBUtils.getNumberOfUniqueValuesPerAttribute(table.getName(), columnName);
			
				
				//get all the attributes in the select condition which are part of union find. get the high key and low key ranges
				long totalTuplesToBeScanned = DBUtils.getTupleCount(table.getName());
				if(previousRelSize != null &&previousRelSize < totalTuplesToBeScanned){
				totalTuplesToBeScanned = previousRelSize;
				}
				UnionFindElement unionFindElement= getMatchingUnionFindElement(columnName,unionFind);
				 List<Column> columns = unionFindElement.getAttributes();
				 Long lowKeyVal = unionFindElement.getLowerBound(); 
				 if(lowKeyVal == null){
					 lowKeyVal = Long.MIN_VALUE;
				 }
				 Long highKeyVal = unionFindElement.getUpperBound();
					
				 
				 if(highKeyVal == null){
					 highKeyVal = Long.MAX_VALUE;
				 }
				 
				 for(Column column: columns){
					 if(getTableNameToCompare(column.getTable()).equals(getTableNameToCompare(table)) == false){
						 continue;
					 }
					totalTuplesToBeScanned = (long) (totalTuplesToBeScanned * DBUtils.getReductionFactor(table.getName(), column.getColumnName(), lowKeyVal, highKeyVal,totalTuplesToBeScanned) * 1.0);
					break;
				 }
				if(totalTuplesToBeScanned < 1){
					initialVvalue = 1;
				} else if(totalTuplesToBeScanned < initialVvalue ){
					initialVvalue = totalTuplesToBeScanned;
				}
				return initialVvalue;
			} else {
				//this is for the join condition
				List<Column> columns = getColumnNamesPartOfUnionFind(columnName,unionFind);
				boolean isColumnNamePartOfJoinCondition = false;
				boolean readFromBaseTable = false;
				//add another case to check if the expression doesnot appear in join condition but might have a = or range value
				if(columns.size() > 0){
					isColumnNamePartOfJoinCondition = true;
					
				} else if(isColumnNamePartOfUnusedExpression(columnName,unionFind)){
					readFromBaseTable = false;
				} else{
					isColumnNamePartOfJoinCondition= false;
					readFromBaseTable =true;
				}
				if(isColumnNamePartOfJoinCondition == true){
					long minVvalue = Long.MAX_VALUE;
					for(Column column: columns){
						//calculate the v value
						long vValue  = (long) DBUtils.getNumberOfUniqueValuesPerAttribute(column.getTable().getName(), column.getColumnName());
						if(vValue < minVvalue){
							minVvalue = vValue;
						}
					}
					return minVvalue;
					
				} else if(readFromBaseTable){
					return (long) DBUtils.getNumberOfUniqueValuesPerAttribute(table.getName(), columnName);
				}
				
				
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	public long calculateVvaluesBaseTable(Table table, String columnName,boolean isBaseTable, boolean isSelectCondition, UnionFind unionFind,Long previousRelSize){
		try {
			if(isBaseTable == true){
				return (long) DBUtils.getTupleCount(table.getName());
			} else if(isSelectCondition == true){
				
				long initialVvalue = (long) DBUtils.getTupleCount(table.getName());
			
				
				//get all the attributes in the select condition which are part of union find. get the high key and low key ranges
				long totalTuplesToBeScanned = DBUtils.getTupleCount(table.getName());
				if(previousRelSize != null &&previousRelSize < totalTuplesToBeScanned){
				totalTuplesToBeScanned = previousRelSize;
				}
				UnionFindElement unionFindElement= getMatchingUnionFindElement(columnName,unionFind);
				 List<Column> columns = unionFindElement.getAttributes();
				 Long lowKeyVal = unionFindElement.getLowerBound(); 
				 if(lowKeyVal == null){
					 lowKeyVal = (long) 0;
				 }
				 Long highKeyVal = unionFindElement.getUpperBound();
					
				 
				 if(highKeyVal == null){
					 highKeyVal = totalTuplesToBeScanned;
				 }
				 
				 for(Column column: columns){
					 if(getTableNameToCompare(column.getTable()).equals(getTableNameToCompare(table)) == false){
						 continue;
					 }
					totalTuplesToBeScanned = (long) (totalTuplesToBeScanned * DBUtils.getReductionFactorBaseTable(table.getName(), column.getColumnName(), lowKeyVal, highKeyVal,totalTuplesToBeScanned) * 1.0);

					break;
				 }
				if(totalTuplesToBeScanned < 1){
					initialVvalue = 1;
				} else if(totalTuplesToBeScanned < initialVvalue ){
					initialVvalue = totalTuplesToBeScanned;
				}
				return initialVvalue;
			} 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	private boolean isColumnNamePartOfUnusedExpression(String columnName,
			UnionFind unionFind) {
				
		// TODO Auto-generated method stub
		for(UnionFindElement unionFindElement : unionFind.getUnionFindElements()){
			if(unionFindElement.hasAttributeColumnName(columnName)){
				return false;
			}
		}
		//If it is not part of any normal expression , its part of unused expression
		return true;
	}


	
	private List<Column> getColumnNamesPartOfUnionFind(String columnName, UnionFind unionFind){
		List<Column> columns = new ArrayList<Column>();
		for(UnionFindElement unionFindElement : unionFind.getUnionFindElements()){
			if(unionFindElement.hasAttributeColumnName(columnName)){
				return unionFindElement.getAttributes();
			}
		}
		return columns;
	}
	
	
	//Write logic to return all the columns
		private List<UnionFindElement> getAllUnionFindElementsPartOfTable(String tableName, UnionFind unionFind){
			List<UnionFindElement> unionFindElements = new ArrayList<UnionFindElement>();
			for(UnionFindElement unionFindElement : unionFind.getUnionFindElements()){
				if(unionFindElement.hasAttributeBelongingToTable(tableName)){
					unionFindElements.add(unionFindElement);
				}
			}
			return unionFindElements;
		}
	
	
	
	
	//Write logic to return matching UFO     NULL CHECK HERE
		private UnionFindElement getMatchingUnionFindElement(String columnName, UnionFind unionFind){
		
			for(UnionFindElement unionFindElement : unionFind.getUnionFindElements()){
				if(unionFindElement.hasAttributeColumnName(columnName)){
					return unionFindElement;
				}
			}
			return null;
		}
	
	public List<List<Table>> getAllSubsetsOfGivenSize(List<Table> originalElements , int k){
		List<List<Table>> resultantSubsets = new ArrayList<List<Table>>();
	    getSubsets(originalElements, k, 0, new ArrayList<Table>(), resultantSubsets);
	    return resultantSubsets;
		
	}


	private void getSubsets(List<Table> originalElements, int k, int index,
			ArrayList<Table> currentSet, List<List<Table>> result) {
		// TODO Auto-generated method stub
		
		if (currentSet.size() == k) {
	        result.add(new ArrayList<>(currentSet));
	        return;
	    }
	    
	    if (index == originalElements.size()) return;
	    Table x = originalElements.get(index);
	    currentSet.add(x);
	   
	    getSubsets(originalElements, k, index+1, currentSet, result);
	    currentSet.remove(x);
	  
	    getSubsets(originalElements, k, index+1, currentSet, result);
		
		
	}
	
	public String getTableNameToUse(Table table){
		
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
		
	}
	
	public String getKeyFromTablesList(List<Table> tablesList){
		String key="";
		List<Table> tables = getSortedListOfTables(tablesList);
		for( int i = 0 ; i < tables.size() ; i ++ ) {
			if(i != tables.size() -1 ){
				key = key + getTableNameToUse(tables.get(i))+ "_";
			} else {
				key = key + getTableNameToUse(tables.get(i));
			}
				
		}
		return key;
	}
	

	public List<Table> getSortedListOfTables(List<Table> tables){
		List<Table> orderedTables = new ArrayList<Table>();
		for(Table joinTable: joinOperatorLogical.getTables()){
			
			for(Table table: tables){
				if(isTableMatching(joinTable, table)){
					orderedTables.add(table);
					break;
				}
				
			}
			
			
		}
		
		return orderedTables;
		
	}
	
	private boolean isTableMatching(Table leftTable,Table rightTable){
		if(leftTable.getAlias()!= null) {
			if(leftTable.getAlias().equals(rightTable.getAlias())){
				 return true;
			}
		}else {
			if(leftTable.getName().equals(rightTable.getName())){
				 return true;
			}
		}
		return false;
	}
	/**
	 * Method to get all the permutations of tables
	 * @param tables
	 * @return
	 */
	public ArrayList<ArrayList<Table>> getAllPermutations(List<Table> tables) {
		ArrayList<ArrayList<Table>> result = new ArrayList<ArrayList<Table>>();
	 
		
		result.add(new ArrayList<Table>());
	 
		for (int i = 0; i < tables.size(); i++) {
			
			ArrayList<ArrayList<Table>> current = new ArrayList<ArrayList<Table>>();
	 
			for (ArrayList<Table> l : result) {

				for (int j = 0; j < l.size()+1; j++) {

					l.add(j, tables.get(i));
	 
					ArrayList<Table> temp = new ArrayList<Table>(l);
					current.add(temp);

					l.remove(j);
				}
			}
	 
			result = new ArrayList<ArrayList<Table>>(current);
		}
	 
		return result;
	}
	
}

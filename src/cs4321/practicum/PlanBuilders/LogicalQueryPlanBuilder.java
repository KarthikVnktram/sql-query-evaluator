package cs4321.practicum.PlanBuilders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cs4321.practicum.DatabaseGlobalCatalog;
import cs4321.practicum.WhereClauseVisitor;
import cs4321.practicum.LogicalOperators.DuplicationEliminationOperatorLogical;
import cs4321.practicum.LogicalOperators.JoinOperatorLogical;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.LogicalOperators.ProjectOperatorLogical;
import cs4321.practicum.LogicalOperators.ScanOperatorLogical;
import cs4321.practicum.LogicalOperators.SelectOperatorLogical;
import cs4321.practicum.LogicalOperators.SortOperatorLogical;
import cs4321.practicum.LogicalPlanUtilities.UnionFind;
import cs4321.practicum.LogicalPlanUtilities.UnionFindElement;
import cs4321.practicum.LogicalPlanUtilities.UnionFindWhereClauseVisitor;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

/**
 * This Class Builds the Query Execution Plan for each of the Sql Queries
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class LogicalQueryPlanBuilder {


	/**
	 * This Method Builds the Query Execution Plan for each of the Sql Queries
	 * Please refer to readme file for detailed explanation on how join is handled
	 * @param statement - Sql query Statement
	 * @return
	 */
	public LogicalOperator buildQuery(Statement statement){
		 Select select=(Select)statement;
			PlainSelect plainSelect=(PlainSelect)select.getSelectBody();
			
			
			Expression whereExpression=plainSelect.getWhere();
//			List<OrderByElement> emptyOrderByList = new ArrayList<OrderByElement>();
			boolean isOrderByPresent = true;
			if(plainSelect.getJoins() != null){
				
				// first build join perfectly
				//then add the common code for project and others
				List<Table> joinTables = new ArrayList<Table>();
				joinTables.add((Table) plainSelect.getFromItem());
				List<Join> joins = plainSelect.getJoins();
				for(Join join :joins){
					joinTables.add(((Table)join.getRightItem()));
				}
				
				List<UnionFindElement> unionFindElements = new ArrayList<UnionFindElement>();
				List<Expression> unusableComparisions = new ArrayList<Expression>();
				UnionFindWhereClauseVisitor unionFindWhereClauseVisitor = new UnionFindWhereClauseVisitor();
				ArrayList<LogicalOperator> childrenOfJoin = new ArrayList<LogicalOperator>();
				UnionFind unionFind = null;
				if(whereExpression != null){
					whereExpression.accept(unionFindWhereClauseVisitor);
					unionFind = unionFindWhereClauseVisitor.getUnionFind();
					if(unionFind != null){
						unionFindElements.addAll(unionFind.getUnionFindElements());
						
						unusableComparisions.addAll(unionFindWhereClauseVisitor.getUnUsableComparisions());
					}
				}	
				JoinOperatorLogical joinOperator = null;
				
				/**
				 * Go Through all the Tables in the join condition
				 * Go Through all the union find elements 
				 * 			for each union find element
				 * 			    if equality condition found, 
				 * 					 create an EqualsTo operator with the bound value add the select expressions
				 * 				else
				 * 					create an Lower Bound operator with the bound value add the select expressions		
				 *                 
				 *                  create an Upper Bound operator with the bound value add the select expressions
				 *          get all the Columns within the same table, part of same Union Find element -	case to handle R.A= R.B types
				 * 
				 * 			Add the remaining elements of the unusable comparision as part of join and keep it
				 */
				for(Table table: joinTables){
					List<Expression> selectExpressions = new ArrayList<Expression>();
					for(UnionFindElement unionFindElement : unionFindElements){
						List<Column> columns = unionFindElement.getAttributes();
						for(Column column: columns){
							if(getTableNameToMatch(column.getTable()).equals(getTableNameToMatch(table))){
								if(unionFindElement.getEqualityConstraint() != null){
									
									Expression expression = createEqualsToExpression(column, unionFindElement.getLowerBound());
									
									selectExpressions.add(expression);
									
									
								} else {
									
									if(unionFindElement.getLowerBound() != null){
										Expression expression = createGreaterThanEqualsExpression(column, unionFindElement.getLowerBound());
										selectExpressions.add(expression);
									} 
									
									if(unionFindElement.getUpperBound() != null){
										Expression expression = createLesserThanEqualsExpression(column, unionFindElement.getUpperBound());
										selectExpressions.add(expression);
									}
								
									
								}
							}
						}
						
						//case to handle R.A= R.B types
						
						List<Column> columnsPending = getAllAtrributesWithSameTableName(columns,table);
						if(columnsPending.size() >1 ){
							Column baseColumn = columnsPending.get(0);
							for(int i=1 ; i < columnsPending.size();i++){
								Column columngettingAnded = columnsPending.get(i);
								Expression expressionToAdd = createEqualsToExpressionAllColumns(baseColumn, columngettingAnded);
								selectExpressions.add(expressionToAdd);
							}
						}
					}
					
					List<Expression> expressionToRemove= new ArrayList<Expression>();
					for(Expression expression : unusableComparisions){
						
						Expression leftExpression = ((BinaryExpression)expression).getLeftExpression();
						Expression rightExpression = ((BinaryExpression)expression).getRightExpression();
						if(leftExpression instanceof Column){
							if(getTableNameToMatch(((Column)leftExpression).getTable()).equals(getTableNameToMatch(table)) &&
									getTableNameToMatch(((Column)rightExpression).getTable()).equals(getTableNameToMatch(table))){
								selectExpressions.add(expression);
								expressionToRemove.add(expression);
							}
						} 
								
					}
					
					unusableComparisions.removeAll(expressionToRemove);
					
					Expression selectExpresn = buildExpressionFromList(selectExpressions);
					LogicalOperator logicalOperator;
					if(selectExpressions.size() >=1){
						ScanOperatorLogical scanOperatorLogical = new ScanOperatorLogical(table);
						logicalOperator = new SelectOperatorLogical(scanOperatorLogical, selectExpresn);
						
					} else {
						//create a Scan operator
						logicalOperator = new ScanOperatorLogical(table);
					}
					childrenOfJoin.add(logicalOperator);
					
				}
				Expression joinExpression = buildExpressionFromList(unusableComparisions);
				
				
				joinOperator = new JoinOperatorLogical(childrenOfJoin, joinTables, joinExpression,unionFind );
				
				joinOperator.setUnusableComparisions((ArrayList<Expression>) unusableComparisions);
				ProjectOperatorLogical projectOperator = null;
				//Now that join tree is built , build the next flow
				if((plainSelect.getSelectItems().get(0) instanceof AllColumns)){
					projectOperator = new ProjectOperatorLogical(getAllColumns(joinTables),joinOperator);
				} else{
					projectOperator =  new ProjectOperatorLogical(joinOperator, plainSelect.getSelectItems());
				}
				
				if(plainSelect.getOrderByElements() != null){
					SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator,plainSelect.getOrderByElements());
//					sortOperator.sorter();
					if(plainSelect.getDistinct() != null ){
						DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
						//duplicateEliminationOperator.performDistinctOperation();
						return duplicateEliminationOperator;
					} else {
						return sortOperator;
					}
				
				} else if (plainSelect.getDistinct() != null){
					isOrderByPresent = false;
					SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator, getColumnsToPerformDistinctOn(projectOperator,plainSelect),isOrderByPresent);
//					sortOperator.sorter();
					DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
					//duplicateEliminationOperator.performDistinctOperation();
					return duplicateEliminationOperator;
				} else {
					return projectOperator;
				}
			
			} else {

				Table tableName=(Table)plainSelect.getFromItem();
				ScanOperatorLogical scanOperator=new ScanOperatorLogical(tableName);
				
				ProjectOperatorLogical projectOperator = null;
				//System.out.println(whereExpression);
				if(whereExpression != null){

					SelectOperatorLogical selectOperator=new SelectOperatorLogical(scanOperator,whereExpression);
					

					if((plainSelect.getSelectItems().get(0) instanceof AllColumns)){
						//#tag
//						projectOperator =  new ProjectOperator(selectOperator, plainSelect.getSelectItems()); 
						List<Table> selectTable =  new ArrayList<Table>();
						selectTable.add((Table)plainSelect.getFromItem());
					
						projectOperator = new ProjectOperatorLogical( getAllColumns(selectTable),selectOperator );
						if(plainSelect.getOrderByElements() != null){
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator,plainSelect.getOrderByElements());
//							sortOperator.sorter();
							
							if(plainSelect.getDistinct() != null ){
								DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
								//duplicateEliminationOperator.performDistinctOperation();
								return duplicateEliminationOperator;
							} else {
								return sortOperator;
							}
						
						} else if(plainSelect.getDistinct() != null){
							isOrderByPresent = false;
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator, getColumnsToPerformDistinctOn(projectOperator,plainSelect),isOrderByPresent);
//							sortOperator.sorter();
							DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
							//duplicateEliminationOperator.performDistinctOperation();
							return duplicateEliminationOperator;
						}else {
							return selectOperator;
						}
					} else{
						projectOperator =  new ProjectOperatorLogical(selectOperator, plainSelect.getSelectItems());
						if(plainSelect.getOrderByElements() != null){
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator,plainSelect.getOrderByElements());
//							sortOperator.sorter();
							
							if(plainSelect.getDistinct() != null ){
								DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
								//duplicateEliminationOperator.performDistinctOperation();
								return duplicateEliminationOperator;
							} else {
								return sortOperator;
							}
						
						} else if(plainSelect.getDistinct() != null){
							isOrderByPresent = false;
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator, getColumnsToPerformDistinctOn(projectOperator,plainSelect),isOrderByPresent);
//							sortOperator.sorter();
							DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
							//duplicateEliminationOperator.performDistinctOperation();
							return duplicateEliminationOperator;
						}else {
							return projectOperator;
						}
					}
										
					
					


				} else {


					if((plainSelect.getSelectItems().get(0) instanceof AllColumns)){
						List<Table> selectTable =  new ArrayList<Table>();
						selectTable.add((Table)plainSelect.getFromItem());
					
						projectOperator = new ProjectOperatorLogical( getAllColumns(selectTable),scanOperator );
						
						if(plainSelect.getOrderByElements() != null){
						
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator,plainSelect.getOrderByElements());
//							sortOperator.sorter();
	
							if(plainSelect.getDistinct() != null ){
								DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
								//duplicateEliminationOperator.performDistinctOperation();
								return duplicateEliminationOperator;
							} else {
								return sortOperator;
							}
						
						} else if(plainSelect.getDistinct() != null){ 
							isOrderByPresent = false;
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator, getColumnsToPerformDistinctOn(projectOperator,plainSelect),isOrderByPresent);
//							sortOperator.sorter();
							DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
							//duplicateEliminationOperator.performDistinctOperation();
							return duplicateEliminationOperator;
						
						}else{
							return scanOperator;
						}
					} else{
						projectOperator =  new ProjectOperatorLogical(scanOperator, plainSelect.getSelectItems());
						if(plainSelect.getOrderByElements() != null){
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator,plainSelect.getOrderByElements());
//							sortOperator.sorter();
							if(plainSelect.getDistinct() != null ){
								DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
//								//duplicateEliminationOperator.performDistinctOperation();
								return duplicateEliminationOperator;
							} else {
								return sortOperator;
							}
						
						} else if (plainSelect.getDistinct() != null){
							isOrderByPresent = false;
							SortOperatorLogical sortOperator=new SortOperatorLogical(projectOperator, getColumnsToPerformDistinctOn(projectOperator,plainSelect),isOrderByPresent);
//							sortOperator.sorter();
							DuplicationEliminationOperatorLogical duplicateEliminationOperator=new DuplicationEliminationOperatorLogical(sortOperator) ;
//							//duplicateEliminationOperator.performDistinctOperation();
							return duplicateEliminationOperator;
							
						} else{
							return projectOperator;
						}
						
					}
					

				}
			}

	 }


		private List<Column> getAllAtrributesWithSameTableName(
			List<Column> columns, Table table) {
		// TODO Auto-generated method stub
			List<Column> columnsOfSameTable = new ArrayList<Column>();
			for(Column column: columns){
				if(getTableNameToMatch(column.getTable()).equals(getTableNameToMatch(table))){
					columnsOfSameTable.add(column);
					
				}
			}
		return columnsOfSameTable;
	}


		/**
	 * this method checks if there are any join conditions that are part 
	 * of the where clause for the list of tables being joined and 
	 * returns the ANDed expression
	 * 
	 * @param tablesPartOfJoin -tables on which join is being performed
	 * @param joinExpression  - all the joinExpressions part of the sql query 
	 * @return
	 */
	private Expression getJoinConditions(List<Table> tablesPartOfJoin,
			Map<List<Table>, Expression> joinExpression) {
		// TODO Auto-generated method stub
		boolean isFirstTableFound = false;
		Expression expr =null;
		List<List<Table>> tableListToRemove = new ArrayList<List<Table>>();
		Set<List<Table>> joinTables = joinExpression.keySet();
		for( List<Table> tableList: joinTables){
			isFirstTableFound = false;
			for(Table tablesPartOfExpression : tableList){
				for(Table tablepartOfJoin: tablesPartOfJoin){
						if(isTableMatching(tablepartOfJoin, tablesPartOfExpression)){
							if(isFirstTableFound  == true ){
								if(expr == null){
									expr = joinExpression.get(tableList);
								} else {
									expr = new AndExpression(expr,joinExpression.get(tableList));
								}
								tableListToRemove.add(tableList);
								
							} else{
								isFirstTableFound = true;
							}
						
						}
				}
			}
		}
		for(List<Table> tables : tableListToRemove){
			joinExpression.remove(tables);
		}
		return expr;
	}



	/**
	 * Method to check if a table is part of Select Expression. If it is , 
	 * then Select operation can be applied on the table before before Join
	 * 
	 * @param table
	 * @param selectExpression
	 * @return
	 */
	private Table isTablePartOfSelectExpression(Table table, Map<Table, Expression> selectExpression) {
		// TODO Auto-generated method stub
		for(Table selectTable : selectExpression.keySet()){
			if(isTableMatching(table, selectTable)){
				return selectTable;
			}
		}
		return null;

	}
	
	/**
	 * Returns the list of all the columns part of the tables in the list
	 * 
	 * @param tables - List of tables 
	 * @return
	 */
	private static List<Column> getAllColumns(List<Table> tables){
		List<Column> columns = new ArrayList<Column>();
		String schema_path=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getSchemaName();
		try {


			for(Table table : tables) {
				BufferedReader br =new BufferedReader(new FileReader(schema_path));
				String temp=null;
				boolean tableFound = false;
				while((temp=br.readLine())!=null) {
					String splittedLine[]=temp.split(" ");
					if(splittedLine[0].equals(table.getName()))
					{
						for(int i=1;i<splittedLine.length;i++) {

							columns.add(new Column(table, splittedLine[i]));
							tableFound = true;
						}
					}
					if(tableFound == true){
						break;
					}
				}
				br.close();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return columns; 
	}


	/**
	 * Method checks if the two tables given as input match
	 * 
	 * @param leftTable
	 * @param rightTable
	 * @return
	 */
	private boolean isTableMatching(Table leftTable,Table rightTable){
		if(leftTable.getAlias()!= null) {
			if(leftTable.getAlias().equals(rightTable.getName())){
				return true;
			}
		}else {
			if(leftTable.getName().equals(rightTable.getName())){
				return true;
			}
		}
		return false;
	}
	
	private Expression buildAndExpression(Expression equalsTo, Expression previousExpression) {
		// TODO Auto-generated method stub
		AndExpression andExpression = new AndExpression(equalsTo, previousExpression);
		return andExpression;
	}

	/**
	 * Method to return the columns to perform distinct operation on
	 * @param projectOperator
	 * @param plainSelect
	 * @return
	 */
	private List<Column> getColumnsToPerformDistinctOn(
			ProjectOperatorLogical projectOperator, PlainSelect plainSelect) {
		// TODO Auto-generated method stub
		if (plainSelect.getSelectItems().get(0) instanceof AllColumns){
			return projectOperator.getAllColumns();
		} else {
			List<Column> columns= new ArrayList<Column>();
			List<SelectItem> selectItems = projectOperator.getColumnNames();
			for(SelectItem columnName: selectItems){
				if(columnName instanceof SelectExpressionItem){
					Column column = (Column)((SelectExpressionItem) columnName).getExpression();
					columns.add(column);
				}
			}
			return columns;
		}
	}
	
	private String getTableNameToMatch(Table table){
		if(table.getAlias() != null){
			return table.getAlias();
		} else {
			return table.getName();
		}
	}
	
	private Expression createEqualsToExpression(Column column, long longVal){
		EqualsTo expression = new EqualsTo();
		LongValue longValue = new LongValue(longVal);
		expression.setLeftExpression(column);
		expression.setRightExpression(longValue);
		return expression;
	}
	
	private Expression createEqualsToExpressionAllColumns(Column leftColumn, Column rightColumn){
		EqualsTo expression = new EqualsTo();
		expression.setLeftExpression(leftColumn);
		expression.setRightExpression(rightColumn);
		return expression;
	}
	
	private Expression createGreaterThanEqualsExpression(Column column, long longVal){
		GreaterThanEquals expression = new GreaterThanEquals();
		LongValue longValue = new LongValue(longVal);
		expression.setLeftExpression(column);
		expression.setRightExpression(longValue);
		return expression;
	}

	private Expression createLesserThanEqualsExpression(Column column, long longVal){
		MinorThanEquals expression = new MinorThanEquals();
		LongValue longValue = new LongValue(longVal);
		expression.setLeftExpression(column);
		expression.setRightExpression(longValue);
		return expression;
	}
	
	private Expression buildExpressionFromList(List<Expression> selectExpressions){
		Expression expression = null;
		if(selectExpressions.size() > 1){
			Expression previousExpression = buildAndExpression(selectExpressions.get(0), selectExpressions.get(1));
			for(int i =2 ; i < selectExpressions.size() ; i++){
				previousExpression = buildAndExpression(selectExpressions.get(i), previousExpression);
			}
			expression = previousExpression;
			
		} else if(selectExpressions.size() == 1){
			
			expression = selectExpressions.get(0);
			
		}
		return expression;
	}
}

package cs4321.practicum;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cs4321.TreeNodes.IndexBuilder;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.PhysicalOperators.Operator;
import cs4321.practicum.PlanBuilders.LogicalQueryPlanBuilder;
import cs4321.practicum.PlanBuilders.PhysicalQueryPlanBuilder;
import cs4321.practicum.utilities.DBUtils;
import cs4321.practicum.utilities.StatsBuilder;
import cs4321.practicum.utilities.TreePlan;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

/**
 * This Class takes in all the Sql Queries and calls the query builder
 * 
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class SqlQueryInterpreter {

	public static boolean orderByPresent=false;
	public static String firstOrderByElement=new String(); 
	public static HashMap<String,String> allTables=new HashMap<String,String>();
	public static List<String> selectItems=new ArrayList<String>();
	/**
	 * This method
	 * 
	 * Calls each of the SQL statement in order 
	 * Calls the query plan builder and 
	 * Places the output in the required folder
	 * @param isEvaluationNeeded 
	 * @param isIndexBuildingNeeded 
	 * 
	 */
	public void translateQueries(boolean isIndexBuildingNeeded, boolean isEvaluationNeeded){ 

		String queryFileName=DatabaseGlobalCatalog.getInstance().getInputFilePath()+File.separator+DatabaseGlobalCatalog.getInstance().queryFileName;
		int fileNumber=1;
		Statement statement=null;

			try {
			CCJSqlParser parser = new CCJSqlParser(new FileReader(queryFileName));
			checkAndBuildIndexes(statement,isIndexBuildingNeeded);
			StatsBuilder statsBuilder=new StatsBuilder();
			try {
				statsBuilder.buildStatistics();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
				while((statement=parser.Statement())!=null)
				{
					try {
//						checkAndBuildIndexes(statement,isIndexBuildingNeeded);

						Select select=(Select)statement;
						PlainSelect plainSelect=(PlainSelect)select.getSelectBody();
						for(int i=0;i<plainSelect.getSelectItems().size();i++){
							selectItems.add(plainSelect.getSelectItems().get(i).toString());
						}
						if(plainSelect.getOrderByElements()==null){
							orderByPresent=false;
						}else{
							orderByPresent=true;
							firstOrderByElement=plainSelect.getOrderByElements().get(0).toString();
						}
					
						if(((Table)(plainSelect.getFromItem())).getAlias()!=null){
							allTables.put(((Table)(plainSelect.getFromItem())).getAlias(),((Table)(plainSelect.getFromItem())).getName());
						}else{
							allTables.put(((Table)(plainSelect.getFromItem())).getName(),((Table)(plainSelect.getFromItem())).getName());
						}
						List<Join> joins = plainSelect.getJoins();
						if(joins != null){
							for(Join join :joins){
								if(((Table)(join.getRightItem())).getAlias()!=null){
									allTables.put(((Table)(join.getRightItem())).getAlias(),((Table)(join.getRightItem())).getName());
								}else{
									allTables.put(((Table)(join.getRightItem())).getName(),((Table)(join.getRightItem())).getName());
								}
							}
						}
						long initialTime = System.currentTimeMillis();
						LogicalQueryPlanBuilder logicalQueryPlanBuilder = new LogicalQueryPlanBuilder();
						LogicalOperator logicalOperator = logicalQueryPlanBuilder.buildQuery(statement);
						TreePlan.LogicalTreePlan(logicalOperator,fileNumber);
						DBUtils.cleanUpTempDir(new File(DatabaseGlobalCatalog.getInstance().tempDirectory));					

						PhysicalQueryPlanBuilder phys = new PhysicalQueryPlanBuilder(logicalOperator);
						phys.parsePlanBuilderConfig();
						phys.buildPhysicalQueryTree();
						Operator physicalOperatorRoot = phys.getPhysicalTreeroot();
						TreePlan.PhysicalTreePlan(physicalOperatorRoot,fileNumber);
						
						physicalOperatorRoot.dump(fileNumber);
						
						long totalTime= System.currentTimeMillis() - initialTime;
						System.out.println("Query "+fileNumber+" Finished in "+ totalTime+" ms\n");
						fileNumber++;
					}catch(Exception expression){
						System.out.println("Statement "+statement+" failed");
//						throw expression;
					}
					orderByPresent=false;
					firstOrderByElement=new String();
					allTables=new HashMap<String,String>();
					selectItems=new ArrayList<String>();
				}
			} catch (ParseException | FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

	}
	
	private static void checkAndBuildIndexes(Statement statement,boolean isIndexBuildNeeded) {
		// TODO Auto-generated method stub
		List<Table> allTables = new ArrayList<Table>();
		
		if(statement != null) {
			Select select=(Select)statement;
			PlainSelect plainSelect=(PlainSelect)select.getSelectBody();
			
			allTables.add((Table) plainSelect.getFromItem());
			List<Join> joins = plainSelect.getJoins();
			if(joins != null){
				for(Join join :joins){
					allTables.add(((Table)join.getRightItem()));
				}
			}
		}
		String indexFilePath=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getIndexInfoFileName();
		try {
			BufferedReader br =new BufferedReader(new FileReader(indexFilePath));
			String temp1=null;
			//@Author: Jigar
			ArrayList<String> allIndexInfo=new ArrayList<String>();
			int i=0;
			while((temp1=br.readLine())!=null){
				allIndexInfo.add(temp1);
			}
//			System.out.println("Index order before : " +allIndexInfo);
			getOrderedByCluster(allIndexInfo);
//			System.out.println("Index order after : " +allIndexInfo);
			//@Author: Jigar
			
			// match and read only selected index condition
		    // Handle the case of Multiple Aliases for the same table
			DatabaseGlobalCatalog databaseGlobalCatalog=   DatabaseGlobalCatalog.getInstance();
			for(String temp:allIndexInfo){
				String[] indexInfo = temp.split(" ");
				String tableName = indexInfo[0];
				String columnName =indexInfo[1];
				String clustered = indexInfo[2];
				String orderOfIndex = indexInfo[3];
				
				Table matchingTable = getTableMatchingTableName(tableName,allTables);
				
				String nameToIndexOn = null;
				
				if(matchingTable != null) {
					if(matchingTable.getAlias() != null){
						nameToIndexOn = matchingTable.getAlias();
					} else {
						nameToIndexOn = matchingTable.getName();
					}
				} else {
					matchingTable = new Table(null,tableName);
					nameToIndexOn = tableName;
				}
				
				boolean isClustered = false;
				if(Integer.parseInt(clustered) == 1){
					isClustered = true;
				}
				int order = Integer.parseInt(orderOfIndex);
				IndexInfo indexInfoObj = new IndexInfo(columnName, isClustered, order);
				if(databaseGlobalCatalog.indexTable.get(tableName) != null){
					List<IndexInfo> indexInfoObjsList = databaseGlobalCatalog.indexTable.get(tableName);
					indexInfoObjsList.add(indexInfoObj);
				} else {
					List<IndexInfo> indexInfoObjsList = new ArrayList<IndexInfo>();
					indexInfoObjsList.add(indexInfoObj);
					databaseGlobalCatalog.indexTable.put(tableName, indexInfoObjsList);
					
				}
				
				if(isIndexBuildNeeded){
//					System.out.println(" Index Creation happening for table "+ nameToIndexOn + " on Column Name "+columnName);
					IndexBuilder indexBuilder = new IndexBuilder(matchingTable, columnName, order,isClustered);
					indexBuilder.buildIndexes();
				}
			}
			br.close();
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//@Author: Jigar
	private static void getOrderedByCluster(ArrayList<String> allIndexInfo) {
		for(int i=0;i<allIndexInfo.size()-1;i++){
			for(int j=i+1;j<allIndexInfo.size();j++){
				String first=allIndexInfo.get(i);
				String second=allIndexInfo.get(j);
				String firstArray[]=first.split(" ");
				String secondArray[]=second.split(" ");
				if(firstArray[0].equals(secondArray[0]) && Integer.parseInt(firstArray[2])<Integer.parseInt(secondArray[2])){
					allIndexInfo.set(i, second);
					allIndexInfo.set(j,first);
				}
			}
		}
	}

	private static Table getTableMatchingTableName(String tableName, List<Table> allTables) {
		// TODO Auto-generated method stub
		for(Table table : allTables){
			if(table.getName() != null && table.getName().equals(tableName)){
				return table;
			}
		}
		
		return null;
	}





}


package cs4321.practicum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import cs4321.TreeNodes.IndexBuilder;
import cs4321.practicum.LogicalOperators.LogicalOperator;
import cs4321.practicum.PhysicalOperators.Operator;
import cs4321.practicum.PlanBuilders.LogicalQueryPlanBuilder;
import cs4321.practicum.PlanBuilders.PhysicalQueryPlanBuilder;
import cs4321.practicum.utilities.ConversionUtility;
import cs4321.practicum.utilities.DBUtils;
import cs4321.practicum.utilities.CusTomTests;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;


/**
 * A class to run custom tests
 * @author Karthik Venkataramaiah (kv233)
 * @author Jigar Bhati (jmb776)
 * @author Dhwanish Shah (ds2246)
 *
 */
public class CustomTest {

	public static void main(String[] args) {
		try {
			String queryFileName=DatabaseGlobalCatalog.getInstance().getInputFilePath()+File.separator+DatabaseGlobalCatalog.getInstance().queryFileName;
			
			Statement statement = null;
			int fileNumber=1;
			
			CCJSqlParser parser = new CCJSqlParser(new FileReader(queryFileName));
			ConversionUtility.convertHumanReadableFileToBinaryFile("C:\\Users\\Karthik\\workspace\\DB_PRAC4\\input\\db\\data\\Boats_humanreadable_new", "C:\\Users\\Karthik\\workspace\\DB_PRAC4\\input\\db\\data\\Boats");
			
			
			String indexFilePath=DatabaseGlobalCatalog.getInstance().getSchemaPath()+File.separator+DatabaseGlobalCatalog.getInstance().getInputFilePath();
			BufferedReader br =new BufferedReader(new FileReader(indexFilePath));
			String temp=null;
			
			while((temp=br.readLine())!=null) {
				String[] indexInfo = temp.split("");
				String tableName = indexInfo[0];
				String columnName =indexInfo[1];
				String clustered = indexInfo[2];
				String orderOfIndex = indexInfo[3];
				// take these lines out later
				Table table = new Table();
				table.setAlias("B");
				table.setName("Boats");
				boolean isClustered = false;
				IndexBuilder indexBuilder = new IndexBuilder(table, "F", 2,isClustered);
				
			}
			
			br.close();
			while((statement=parser.Statement())!=null)
			{
				LogicalQueryPlanBuilder builder = new LogicalQueryPlanBuilder();
				LogicalOperator logicalOperator = builder.buildQuery(statement);
				DBUtils.cleanUpTempDir(new File(DatabaseGlobalCatalog.getInstance().tempDirectory));
	
				long tmp = System.currentTimeMillis();
				
				PhysicalQueryPlanBuilder phys = new PhysicalQueryPlanBuilder(logicalOperator);
				phys.parsePlanBuilderConfig();
				phys.buildPhysicalQueryTree();
				Operator physicalOperatorRoot = phys.getPhysicalTreeroot();
				
				physicalOperatorRoot.dump(fileNumber);
				
				long totalTime= System.currentTimeMillis() - tmp;
//				System.out.println("Query "+fileNumber+" Finished in "+ totalTime+" ms\n");
				
				fileNumber++;
			}
			CusTomTests.sortResults();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}

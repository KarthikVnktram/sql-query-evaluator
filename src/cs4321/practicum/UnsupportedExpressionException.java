package cs4321.practicum;

/**
 * To Throw Exceptions
 * @author Jigar Bhati (jmb776)
 * @author Karthik Venkataramaiah (kv233)
 * @author Dhwanish Shah (ds2246)
 */

public class UnsupportedExpressionException extends RuntimeException {

	public UnsupportedExpressionException(String exceptionMessage){
		super(exceptionMessage);
	}
}
